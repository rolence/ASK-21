/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Injectable} from '@angular/core';
import {Headers, Http, RequestMethod, RequestOptions, RequestOptionsArgs, Response, ResponseContentType} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {Store} from '@ngrx/store';
import * as fromRoot from '../../store';
import {AUTHORIZATION_HEADER, TENANT_HEADER, USER_HEADER} from '../http/http.service';
import {State} from '../../store/security/authentication.reducer';

@Injectable()
export class DownloadService {

  constructor(private http: Http, private store: Store<fromRoot.State>) {
  }

  public downloadFile(url: string, fileName?: string, method: RequestMethod = RequestMethod.Get, body?: any): Observable<Blob> {
    return this.store.select(fromRoot.getAuthenticationState)
      .take(1)
      .map(authenticationState => this.mapHeader(authenticationState, 'application/json'))
      .switchMap((headers: Headers) =>
        this.http.request(url, {
          method,
          body,
          responseType: ResponseContentType.Blob,
          headers
        })
        .do((response: Response) => {
          const blob = response.blob();
          fileName = fileName || this.extractFileName(response.headers);
          this.downloadBlob(blob, fileName);
        })
        .map((response: Response) => response.blob())
      );
  }

  private downloadBlob(blob: Blob, fileName?: string): void {
    const link = document.createElement('a');
    link.href = URL.createObjectURL(blob);
    link.download = fileName;
    link.click();
    URL.revokeObjectURL(link.href);
  }

  public getImage(url: string): Observable<{} | Blob> {
    return this.store.select(fromRoot.getAuthenticationState)
      .take(1)
      .map(authenticationState => this.mapHeader(authenticationState, 'application/json'))
      .switchMap((headers: Headers) =>
        this.http.get(url, {
          responseType: ResponseContentType.Blob,
          headers
        })
        .map((response: Response) => response.blob())
        .catch(() => Observable.empty()));
  }

  public getText(url: string, options?: RequestOptionsArgs): Observable<string | {}> {
    return this.store.select(fromRoot.getAuthenticationState)
      .take(1)
      .map(authenticationState => this.mapHeader(authenticationState, 'text/plain'))
      .map((headers: Headers) => {
        const requestOptions: RequestOptions = new RequestOptions({
          headers
        });

        return requestOptions.merge(options);
      })
      .switchMap((requestOptions: RequestOptions) =>
        this.http.get(url, requestOptions)
          .do((response: Response) => {
            const text = response.text();
            // Create artificial link to trigger download by the browser
            const blob = new Blob([text]);
            const fileName = this.extractFileName(response.headers);
            this.downloadBlob(blob, fileName);
          })
          .map((response: Response) => response.text())
          .catch(() => Observable.empty()));
  }

  private extractFileName(headers: Headers): string {
    const contentHeader = headers.get('content-disposition');

    let fileName = 'file';

    if (contentHeader) {
      const headerValues: string[] = contentHeader.split(';');

      if (headerValues.length > 1) {
        const keyValue: string[] = headerValues[1].trim().split('=');
        if (keyValue.length > 1) {
          fileName = keyValue[1].replace(/"/g, '');
        }
      }
    }

    return fileName;
  }

  private mapHeader(authenticationState: State, accept: string): Headers {
    const headers = new Headers();

    headers.set('Accept', accept);

    headers.set(TENANT_HEADER, authenticationState.tenant);
    headers.set(USER_HEADER, authenticationState.username);
    headers.set(AUTHORIZATION_HEADER, authenticationState.authentication.accessToken);

    return headers;
  }

}
