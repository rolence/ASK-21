/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '../http/http.service';
import {Observable} from 'rxjs/Observable';
import {CustomerNumberFormat} from './domain/customer-number-format.model';
import {CustomerDoubletValidation} from './domain/customer-doublet-validation.model';
import {CustomerVerification} from './domain/customer-verification.model';

@Injectable()
export class SettingsService {

  constructor(@Inject('customerBaseUrl') private baseUrl: string, private http: HttpClient) {
  }

  setCustomerNumberFormat(customerNumberFormat: CustomerNumberFormat): Observable<void> {
    return this.http.put(`${this.baseUrl}/settings/format`, customerNumberFormat);
  }

  getCustomerNumberFormat(silent?: boolean): Observable<CustomerNumberFormat> {
    return this.http.get(`${this.baseUrl}/settings/format`, null, silent);
  }

  setCustomerDoubletValidation(customerDoubletValidation: CustomerDoubletValidation): Observable<void> {
    return this.http.put(`${this.baseUrl}/settings/doublet`, customerDoubletValidation);
  }

  getCustomerDoubletValidation(silent?: boolean): Observable<CustomerDoubletValidation> {
    return this.http.get(`${this.baseUrl}/settings/doublet`, null, silent);
  }

  verifyCustomer(customerVerfification: CustomerVerification): Observable<boolean> {
    return this.http.post(`${this.baseUrl}/settings/doublet/verify`, customerVerfification);
  }
}
