/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {LoanType} from '../loan-type.model';
import {Term} from './term.model';
import {DebtToIncome} from './debt-to-income.model';
import {Guarantor} from './guarantor.model';
import {Document} from './document-meta-data.model';
import {AppliedCharge} from './applied-charge.model';
import {AttachedCollateral} from './attached-collateral.model';

export type State = 'PENDING' | 'UNDERWRITING' | 'APPROVED' | 'DECLINED' |
  'DISBURSED' | 'DELINQUENT' | 'REPAID' | 'RESTRUCTURE' | 'END_OF_TERM';

export type DisbursementMethod = 'CASH' | 'TRANSFER' | 'CHEQUE';

export interface LoanAgreement {
  loanType: LoanType;
  productShortName: string;
  loanAccount?: string;
  hasPendingAssessments: boolean;
  currentUserCanApprove?: boolean;
  eclStage?: number;
  amountOnHold?: string;
  appliedCharges: AppliedCharge[];
  purpose?: string;
  attachedCollateral?: AttachedCollateral;
  disbursementMethod?: DisbursementMethod;
  lossAllowance?: string;
  term: Term;
  debtToIncome?: DebtToIncome;
  guarantor?: Guarantor;
  relatedDocuments?: Document[];
  state: State;
  disbursedOn?: string;
  disbursedAmount?: string;
  inRestructuring?: boolean;
  accruedInterest?: string;
  delinquentSince?: string;
  arrearsAccount?: string;
  arrearsAmount?: string;
  branchSortCode?: string;
  createdBy?: string;
  createdOn?: string;
  lastModifiedBy?: string;
  lastModifiedOn?: string;
}
