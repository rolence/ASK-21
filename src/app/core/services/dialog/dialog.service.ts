/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Injectable} from '@angular/core';
import {TdDialogService} from '@covalent/core';
import {TranslateService} from '@ngx-translate/core';
import {Observable} from 'rxjs/Observable';
import {MatDialogConfig, MatDialogRef} from '@angular/material';
import {ComponentType} from '@angular/cdk/portal';

export interface DialogConfig {
  message: string;
  title: string;
  acceptButton: string;
}

@Injectable()
export class DialogService {

  constructor(private tdDialogService: TdDialogService, private translateService: TranslateService) {}

  openConfirm(config: DialogConfig): Observable<any> {
    return this.translateService.get([config.title, config.message, config.acceptButton])
      .mergeMap(result =>
        this.tdDialogService.openConfirm({
          message: result[config.message],
          title: result[config.title],
          acceptButton: result[config.acceptButton]
        }).afterClosed()
      );
  }

  open<T>(component: ComponentType<T>, config?: MatDialogConfig): MatDialogRef<T> {
    return this.tdDialogService.open(component, config);
  }
}
