/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '../http/http.service';
import {CustomerListEntry} from '../customer/domain/customer-list-entry.model';
import {DepositAccountListEntry} from '../depositAccount/domain/definition/deposit-product-list-entry.model';
import {RequestOptionsArgs, URLSearchParams} from '@angular/http';
import {Type} from '../depositAccount/domain/type.model';
import {State as DepositAccountState} from '../depositAccount/domain/instance/state.model';
import {State as LoanAgreementState} from '../loan/domain/agreement/loan-agreement.model';
import {buildDateRangeParam, buildSearchParams} from '../domain/paging/search-param.builder';
import {DownloadService} from '../download/download.service';
import {TermAccountListEntry} from '../depositAccount/domain/definition/term-account-list-entry';
import {LoanAgreementListEntry} from '../loan/domain/agreement/loan-agreement-list-entry.model';
import {StandingOrderEntry} from './domain/standing-order-entry.model';
import {IncomeStatement} from '../accounting/domain/income-statement.model';
import {FinancialCondition} from '../accounting/domain/financial-condition.model';
import {TrialBalance} from '../accounting/domain/trial-balance.model';


@Injectable()
export class ReportingService {

  constructor(@Inject('reportingBaseUrl') private baseUrl: string, private http: HttpClient, private downloadService: DownloadService) {
  }

  getCustomerListEntries(): Observable<CustomerListEntry[]> {
    return this.http.get(`${this.baseUrl}/customers/listing`);
  }

  exportCustomerList(): Observable<string | {}> {
    return this.downloadService.getText(`${this.baseUrl}/customers/listing/export`);
  }

  fetchDepositAccounts(state: DepositAccountState, startDate: string, endDate: string, type: Type,
                       employee?: string): Observable<DepositAccountListEntry[]> {
    const params: URLSearchParams = buildSearchParams();
    const dateRange = buildDateRangeParam(startDate, endDate);
    params.append('dateRange', dateRange);
    params.append('state', state);
    params.append('type', type);
    if (employee === '') {
      employee = undefined;
    }
    params.append('employee', employee);

    const requestOptions: RequestOptionsArgs = {
      params
    };

    return this.http.get(`${this.baseUrl}/deposits/listing`, requestOptions);
  }

  exportDepositAccountList(state: DepositAccountState, startDate: string,
                           endDate: string, type: Type, employee?: string): Observable<string | {}> {
    const params: URLSearchParams = buildSearchParams();
    const dateRange = buildDateRangeParam(startDate, endDate);

    params.append('dateRange', dateRange);
    params.append('state', state);
    params.append('type', type);

    if (employee === '') {
      employee = undefined;
    }
    params.append('employee', employee);

    const requestOptions: RequestOptionsArgs = {
      params
    };
    return this.downloadService.getText(`${this.baseUrl}/deposits/listing/export`, requestOptions);
  }

  fetchTermDepositAccounts(): Observable<TermAccountListEntry[]> {
    return this.http.get(`${this.baseUrl}/deposits/terms`);
  }

  exportTermAccountList(): Observable<string | {}> {
    return this.downloadService.getText(`${this.baseUrl}/deposits/terms/export`);
  }
  fetchLoanAccounts(state: LoanAgreementState): Observable<LoanAgreementListEntry[]> {
    const params: URLSearchParams = buildSearchParams();
    params.append('state', state);
    const requestOptions: RequestOptionsArgs = {
      params
    };

    return this.http.get(`${this.baseUrl}/loans/listing`, requestOptions);
  }

  exportLoanAccountList(state: LoanAgreementState): Observable<string | {}> {
    const params: URLSearchParams = buildSearchParams();
    params.append('state', state);

    const requestOptions: RequestOptionsArgs = {
      params
    };
    return this.downloadService.getText(`${this.baseUrl}/loans/listing/export`, requestOptions);
  }

  standingOrderListing(): Observable<StandingOrderEntry[]> {
    return this.http.get(`${this.baseUrl}/deposits/standingorders`);
  }

  exportStandingOrderListing(): Observable<string | {}> {
    return this.downloadService.getText(`${this.baseUrl}/deposits/standingorders/export`);
  }

  public getIncomeStatement(): Observable<IncomeStatement> {
    return this.http.get(`${this.baseUrl}/finance/statement`);
  }

  public downloadIncomeStatement(): void {
    this.downloadService.getText(`${this.baseUrl}/finance/statement/export`)
      .subscribe();
  }

  public getFinancialCondition(): Observable<FinancialCondition> {
    return this.http.get(`${this.baseUrl}/finance/condition`);
  }

  public downloadFinancialCondition(): void {
    this.downloadService.getText(`${this.baseUrl}/finance/condition/export`)
      .subscribe();
  }

  public getTrialBalance(includeEmptyEntries?: boolean): Observable<TrialBalance> {
    const params: URLSearchParams = new URLSearchParams();
    params.append('includeEmptyEntries', includeEmptyEntries ? 'true' : 'false');

    const requestOptions: RequestOptionsArgs = {
      params
    };
    return this.http.get(`${this.baseUrl}/finance/balance`, requestOptions);
  }

  public downloadTrialBalance(includeEmptyEntries?: boolean): void {
    const params: URLSearchParams = new URLSearchParams();
    params.append('includeEmptyEntries', includeEmptyEntries ? 'true' : 'false');

    const requestOptions: RequestOptionsArgs = {
      params
    };
    this.downloadService.getText(`${this.baseUrl}/finance/balance/export`, requestOptions)
      .subscribe();
  }

}
