/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {fakeAsync, TestBed, tick} from '@angular/core/testing';
import {provideMockActions} from '@ngrx/effects/testing';
import {OfficeSearchApiEffects} from './service.effects';
import {Observable} from 'rxjs/Observable';
import {OfficeService} from '../../../services/office/office.service';
import {SearchAction, SearchCompleteAction} from '../office.actions';
import {OfficePage} from '../../../services/office/domain/office-page.model';
import {ReplaySubject} from 'rxjs/ReplaySubject';
import {emptySearchResult} from '../../util/search.reducer';

describe('Office Search Api Effects', () => {

  let actions: ReplaySubject<any>;

  beforeEach(() => {

    TestBed.configureTestingModule({
      providers: [
        OfficeSearchApiEffects,
        {
          provide: OfficeService,
          useValue: jasmine.createSpyObj('officeService', ['listOffices'])
        },
        provideMockActions(() => actions)
      ]
    });

  });

  describe('search$', () => {

    function setup(params?: {searchOfficesReturnValue: any}) {
      const officeService = TestBed.get(OfficeService);
      if (params) {
        officeService.listOffices.and.returnValue(params.searchOfficesReturnValue);
      }

      return {
        officeEffects: TestBed.get(OfficeSearchApiEffects)
      };
    }

    it('should return a new SearchCompleteAction with EmployeePage', fakeAsync(() => {
      const officePage: OfficePage = {
        offices: [
          { identifier: '', name: '' }
        ],
        totalElements: 1,
        totalPages: 1
      };

      const { officeEffects } = setup({ searchOfficesReturnValue: Observable.of(officePage) });

      const expectedResult = new SearchCompleteAction({
        elements: officePage.offices,
        totalPages: officePage.totalPages,
        totalElements: officePage.totalElements
      });

      actions = new ReplaySubject(1);

      actions.next(new SearchAction({}));

      let result = null;
      officeEffects.search$.subscribe(_result => result = _result);

      tick(299);
      expect(result).toBe(null);
      tick(300);
      expect(result).toEqual(expectedResult);
    }));

    it('should return a new SearchCompleteAction, with an empty array, if office service throws', fakeAsync(() => {
      const {officeEffects} = setup({searchOfficesReturnValue: Observable.throw(new Error())});

      const expectedResult = new SearchCompleteAction(emptySearchResult());

      actions = new ReplaySubject(1);

      actions.next(new SearchAction({}));

      let result = null;
      officeEffects.search$.subscribe(_result => result = _result);

      tick(299);
      expect(result).toBe(null);
      tick(300);
      expect(result).toEqual(expectedResult);
    }));
  });
});
