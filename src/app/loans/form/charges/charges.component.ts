/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, EventEmitter, Input, Output} from '@angular/core';
import {AbstractControl, FormArray, FormGroup} from '@angular/forms';
import {Action} from '../../../core/services/loan/domain/definition/action.model';
import {chargeTypes} from '../../domain/chargetype-types.model';

@Component({
  selector: 'aten-loan-product-charges-form',
  templateUrl: './charges.component.html'
})
export class LoanProductChargesFormComponent {

  chargeTypeOptions = chargeTypes;

  @Input() actions: Action[];
  @Input() formGroup: FormGroup;

  @Output() onAddCharge = new EventEmitter<void>();
  @Output() onRemoveCharge = new EventEmitter<number>();

  addCharge(): void {
    this.onAddCharge.emit();
  }

  removeCharge(index: number): void {
    this.onRemoveCharge.emit(index);
  }

  get charges(): AbstractControl[] {
    const charges: FormArray = this.formGroup.get('charges') as FormArray;
    return charges.controls;
  }

}
