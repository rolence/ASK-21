/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {FormBuilder, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {Injectable} from '@angular/core';
import {FimsValidators, isEmptyInputValue} from '../../../core/validator/validators';
import {AccountingService} from '../../../core/services/accounting/accounting.service';
import {accountExists} from '../../../core/validator/account-exists.validator';
import {ECLStage} from '../../../core/services/loan/domain/definition/ECL-stage.model';
import {ExpectedCreditLoss} from '../../../core/services/loan/domain/definition/expected-credit-loss.model';

@Injectable()
export class LossAllowanceFormService {

  private _lossAllowanceFormGroup: FormGroup = new FormGroup({});

  static pastDueDaysNoConflict(formGroup: FormGroup): ValidationErrors | null {
    const secondStage = formGroup.get('secondStage') as FormGroup;
    const thirdStage = formGroup.get('thirdStage') as FormGroup;

    if (secondStage.disabled || thirdStage.disabled) {
      return null;
    }

    const pastDueDaysSecondStage = secondStage.get('pastDueDays').value as string;
    const pastDueDaysThirdStage = thirdStage.get('pastDueDays').value as string;

    if (isEmptyInputValue(pastDueDaysSecondStage) || isEmptyInputValue(pastDueDaysThirdStage)) {
      return null;
    }

    if (parseFloat(pastDueDaysSecondStage) >= parseFloat(pastDueDaysThirdStage)) {
      return {
        dueDays: {
          valid: false
        }
      };
    }
    return null;
  }

  constructor(private formBuilder: FormBuilder, private accountingService: AccountingService) {}

  initLossAllowanceForm(): FormGroup {
    this._lossAllowanceFormGroup = this.buildLossAllowanceForm();

    this._lossAllowanceFormGroup.get('enabled').valueChanges
      .startWith(false)
      .subscribe(enabled => this.toggleEnabled(enabled));

    return this._lossAllowanceFormGroup;
  }

  private toggleEnabled(enabled: boolean): void {
    const allowanceControl = this._lossAllowanceFormGroup.get('loanLossAllowanceAccount');
    const cashAccountControl = this._lossAllowanceFormGroup.get('cashAccount');
    const initialStageControl = this._lossAllowanceFormGroup.get('initialStage');
    const firstStageControl = this._lossAllowanceFormGroup.get('firstStage');
    const secondStageControl = this._lossAllowanceFormGroup.get('secondStage');
    const thirdStageControl = this._lossAllowanceFormGroup.get('thirdStage');

    if (enabled) {
      allowanceControl.enable();
      cashAccountControl.enable();
      initialStageControl.enable();
      firstStageControl.enable();
      secondStageControl.enable();
      thirdStageControl.enable();
    } else {
      allowanceControl.disable();
      cashAccountControl.disable();
      initialStageControl.disable();
      firstStageControl.disable();
      secondStageControl.disable();
      thirdStageControl.disable();
    }
  }

  private buildLossAllowanceForm(): FormGroup {
    return this.formBuilder.group({
      enabled: [false],
      loanLossAllowanceAccount: ['', [Validators.required], accountExists(this.accountingService)],
      cashAccount: ['', [Validators.required], accountExists(this.accountingService)],
      initialStage: this.initStage(0),
      firstStage: this.initStage(1),
      secondStage: this.initStage(2),
      thirdStage: this.initStage(3),
    }, { validator: LossAllowanceFormService.pastDueDaysNoConflict });
  }

  private initStage(stageNumber: number, stage?: ECLStage): FormGroup {
    return this.formBuilder.group({
      stage: [stage ? stage.stage : stageNumber, [Validators.required]],
      percentage: [stage ? stage.percentage : '0.00', [Validators.required, FimsValidators.minValue(0), FimsValidators.maxValue(100)]],
      pastDueDays: [stage ? stage.pastDueDays : 0, [Validators.required, FimsValidators.minValue(0)]],
      curePeriod: [stage ? stage.curePeriod : 0, [Validators.required, FimsValidators.minValue(0)]]
    });
  }

  getCreditLoss(): ExpectedCreditLoss | undefined {
    if (this._lossAllowanceFormGroup.get('enabled').value) {
      return this._lossAllowanceFormGroup.getRawValue();
    }
  }

  setCreditLoss(creditLoss: ExpectedCreditLoss): void {
    if (creditLoss) {
      this._lossAllowanceFormGroup.reset({
        enabled: true,
        loanLossAllowanceAccount: creditLoss.loanLossAllowanceAccount,
        cashAccount: creditLoss.cashAccount,
        initialStage: creditLoss.initialStage,
        firstStage: creditLoss.firstStage,
        secondStage: creditLoss.secondStage,
        thirdStage: creditLoss.thirdStage
      });
    }
  }

  get valid(): boolean {
    if (this._lossAllowanceFormGroup.get('enabled').value) {
      return this._lossAllowanceFormGroup.valid;
    }

    return true;
  }
}
