/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {Injectable} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AccountingService} from '../../../core/services/accounting/accounting.service';
import {Charge} from '../../../core/services/loan/domain/definition/charge.model';
import {FimsValidators} from '../../../core/validator/validators';
import {accountExists} from '../../../core/validator/account-exists.validator';

@Injectable()
export class ChargesFormService {

  private _chargesFormGroup: FormGroup = new FormGroup({});

  constructor(private formBuilder: FormBuilder, private accountingService: AccountingService) {}

  initChargesForm(): FormGroup {
    this._chargesFormGroup = this.buildChargesForm();

    return this._chargesFormGroup;
  }

  private buildChargesForm(): FormGroup {
    return this.formBuilder.group({
      charges: this.initCharges([])
    });
  }

  private initCharges(charges: Charge[]): FormArray {
    const formControls: FormGroup[] = [];
    charges.forEach(charge => formControls.push(this.initCharge(charge)));
    return this.formBuilder.array(formControls);
  }

  private toggleAdHoc(formGroup: FormGroup, adHoc: boolean): void {
    const thresholdControl = formGroup.get('threshold');
    const proportionalControl = formGroup.get('proportional');
    const amountControl = formGroup.get('amount');
    const chargeTypeControl = formGroup.get('chargeType');
    if (adHoc) {
      thresholdControl.disable();
      proportionalControl.disable();
      amountControl.disable();
      chargeTypeControl.disable();
    } else {
      thresholdControl.enable();
      proportionalControl.enable();
      amountControl.enable();
      chargeTypeControl.enable();
    }
  }

  private initCharge(charge?: Charge): FormGroup {
    const formGroup = this.formBuilder.group({
      chargeType: [charge ? charge.chargeType : 'ONCE', [Validators.required]],
      description: [charge ? charge.description : '', [Validators.required, Validators.maxLength(256)]],
      adHoc: [],
      action: [charge ? charge.action : '', Validators.required],
      revenueAccount: [charge ? charge.revenueAccount : '', [Validators.required], accountExists(this.accountingService)],
      threshold: [charge ? charge.threshold : undefined, [FimsValidators.greaterThanValue(0)]],
      proportional: [charge ? charge.proportional : false ],
      amount: [charge ? charge.amount : '0.00', [ Validators.required, FimsValidators.greaterThanValue(0)] ]
    });

    const adHocControl = formGroup.get('adHoc');

    adHocControl.valueChanges
      .subscribe(adHoc => this.toggleAdHoc(formGroup, adHoc));

    adHocControl.setValue(charge ? charge.adHoc : false);

    return formGroup;
  }

  addCharge(charge?: Charge): void {
    const charges: FormArray = this.chargesFormGroup.get('charges') as FormArray;
    charges.push(this.initCharge(charge));
  }

  removeCharge(index: number): void {
    const charges: FormArray = this.chargesFormGroup.get('charges') as FormArray;
    charges.removeAt(index);
  }

  get chargesFormGroup(): FormGroup {
    return this._chargesFormGroup;
  }

  getCharges(): Charge[] {
    const charges = this.chargesFormGroup.get('charges').value;

    return charges.map(charge => {
      if (charge.adHoc) {
        return {
          chargeType: charge.chargeType,
          description: charge.description,
          adHoc: charge.adHoc,
          action: charge.action,
          revenueAccount: charge.revenueAccount
        };
      }

      return charge;
    });
  }

  setCharges(charges: Charge[]): void {
    charges.forEach(charge => this.addCharge(charge));
  }
}
