/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {Injectable} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ApprovalLimit} from '../../../core/services/loan/domain/definition/approval-limit.model';
import {FimsValidators} from '../../../core/validator/validators';

@Injectable()
export class LimitsFormService {

  private _limitsFormGroup: FormGroup = new FormGroup({});

  constructor(private formBuilder: FormBuilder) {}

  initLimitsForm(): FormGroup {
    this._limitsFormGroup = this.buildLimitsForm();

    return this._limitsFormGroup;
  }

  private buildLimitsForm(): FormGroup {
    return this.formBuilder.group({
      limits: this.initLimits([])
    });
  }

  private initLimits(limits: ApprovalLimit[]): FormArray {
    const formControls: FormGroup[] = [];
    limits.forEach(limit => formControls.push(this.initLimit(limit)));
    return this.formBuilder.array(formControls);
  }

  private initLimit(limit?: ApprovalLimit): FormGroup {
    return this.formBuilder.group({
      role: [limit ? limit.role : undefined, Validators.required],
      limit: [limit ? limit.limit : undefined, FimsValidators.greaterThanValue(0)]
    });
  }

  addLimit(limit?: ApprovalLimit): void {
    const limits: FormArray = this.limitsFormGroup.get('limits') as FormArray;
    limits.push(this.initLimit(limit));
  }

  removeLimit(index: number): void {
    const limits: FormArray = this.limitsFormGroup.get('limits') as FormArray;
    limits.removeAt(index);
  }

  get limitsFormGroup(): FormGroup {
    return this._limitsFormGroup;
  }

  getLimits(): ApprovalLimit[] {
    const limits: FormArray = this.limitsFormGroup.get('limits') as FormArray;
    return limits.getRawValue();
  }

  setLimits(limits: ApprovalLimit[]): void {
    limits.forEach(limit => this.addLimit(limit));
  }
}
