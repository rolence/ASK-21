/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Component} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import * as fromLoanProducts from './store';
import {ActivatedRoute, Router} from '@angular/router';
import {DeleteProductAction, DisableProductAction, EnableProductAction} from './store/loan-product.actions';
import {Store} from '@ngrx/store';
import {TableData} from '../shared/data-table/data-table.component';
import {LoanDefinition} from '../core/services/loan/domain/definition/loan-definition.model';
import {DialogService} from '../core/services/dialog/dialog.service';
import {dataTypes} from '../shared/custom-fields/domain/datatype-types.model';
import {chargeTypes} from './domain/chargetype-types.model';
import {ChargeType} from '../core/services/loan/domain/definition/charge-type.model';

@Component({
  templateUrl: './loan-product.detail.component.html'
})
export class LoanProductDetailComponent {

  fieldData$: Observable<TableData>;

  columns: any[] = [
    { name: 'identifier', label: 'Identifier' },
    { name: 'dataType', label: 'Data type', format: value => dataTypes.find(type => type.type === value).label },
    { name: 'label', label: 'Label' },
    { name: 'hint', label: 'Hint' },
    { name: 'description', label: 'Description' },
    { name: 'mandatory', label: 'Mandatory' }
  ];

  loanDefinition$: Observable<LoanDefinition>;

  constructor(private router: Router, private route: ActivatedRoute,
              private store: Store<fromLoanProducts.State>, private dialogService: DialogService) {
    this.loanDefinition$ = this.store.select(fromLoanProducts.getSelectedProduct)
      .filter(product => !!product);

    this.fieldData$ = this.loanDefinition$
      .filter(definition => !!definition.collateral)
      .map(definition => ({
        data: definition.collateral.fields,
        totalElements: definition.collateral.fields.length,
        totalPages: 1
      }));
  }

  enableProduct(definition: LoanDefinition): void {
    this.store.dispatch(new EnableProductAction({
      definition
    }));
  }

  disableProduct(definition: LoanDefinition): void {
    this.store.dispatch(new DisableProductAction({
      definition
    }));
  }

  deleteProduct(definition: LoanDefinition): void {
    this.confirmDeletion()
      .filter(accept => accept)
      .subscribe(() => this.store.dispatch(new DeleteProductAction({
        definition,
        activatedRoute: this.route
      })));
  }

  private confirmDeletion(): Observable<boolean> {
    return this.dialogService.openConfirm({
      message: 'Do you want to delete this loan product?',
      title: 'Confirm deletion',
      acceptButton: 'DELETE LOAN PRODUCT',
    });
  }

  editProduct(): void {
    this.router.navigate(['edit'], { relativeTo: this.route });
  }

  getChargeTypeOption(chargeType: ChargeType) {
    return chargeTypes.find(value => value.type === chargeType).label;
  }

}
