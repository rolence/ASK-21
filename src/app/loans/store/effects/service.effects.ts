/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Actions, Effect} from '@ngrx/effects';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';
import {Action} from '@ngrx/store';
import {Injectable} from '@angular/core';
import * as actions from '../loan-product.actions';
import {ConsumerLoanService} from '../../../core/services/loan/consumer-loan.service';

@Injectable()
export class LoanProductApiEffects {

  @Effect()
  search$: Observable<Action> = this.actions$
    .ofType(actions.SEARCH)
    .mergeMap(() =>
      this.loanService.fetchAllProducts()
        .map(products => new actions.SearchCompleteAction(products))
        .catch(() => of(new actions.SearchCompleteAction([])))
    );

  @Effect()
  createProduct$: Observable<Action> = this.actions$
    .ofType(actions.CREATE)
    .map((action: actions.CreateProductAction) => action.payload)
    .mergeMap(payload =>
      this.loanService.createProduct(payload.definition)
        .map(() => new actions.CreateProductSuccessAction({
          resource: payload.definition,
          activatedRoute: payload.activatedRoute
        }))
        .catch((error) => of(new actions.CreateProductFailAction(error)))
    );

  @Effect()
  updateProduct$: Observable<Action> = this.actions$
    .ofType(actions.UPDATE)
    .map((action: actions.UpdateProductAction) => action.payload)
    .mergeMap(payload =>
      this.loanService.updateProduct(payload.shortName, payload.changeSet)
        .map(() => new actions.UpdateProductSuccessAction({
          resource: {
            ...payload.changeSet,
            shortName: payload.shortName
          },
          activatedRoute: payload.activatedRoute
        }))
        .catch((error) => of(new actions.UpdateProductFailAction(error)))
    );

  @Effect()
  deleteProduct$: Observable<Action> = this.actions$
    .ofType(actions.DELETE)
    .map((action: actions.DeleteProductAction) => action.payload)
    .mergeMap(payload =>
      this.loanService.deleteProduct(payload.definition.shortName)
        .map(() => new actions.DeleteProductSuccessAction({
          resource: payload.definition,
          activatedRoute: payload.activatedRoute
        }))
        .catch((error) => of(new actions.DeleteProductFailAction(error)))
    );

  @Effect()
  enableProduct$: Observable<Action> = this.actions$
    .ofType(actions.ENABLE)
    .map((action: actions.EnableProductAction) => action.payload)
    .mergeMap(payload =>
      this.loanService.process(payload.definition.shortName, {
        type: 'ENABLE'
      }).map(() => new actions.EnableProductSuccessAction(payload))
        .catch((error) => of(new actions.EnableProductFailAction(error)))
    );

  @Effect()
  disableProduct$: Observable<Action> = this.actions$
    .ofType(actions.DISABLE)
    .map((action: actions.DisableProductAction) => action.payload)
    .mergeMap(payload =>
      this.loanService.process(payload.definition.shortName, {
        type: 'DISABLE'
      }).map(() => new actions.DisableProductSuccessAction(payload))
        .catch((error) => of(new actions.DisableProductFailAction(error)))
    );

  constructor(private actions$: Actions, private loanService: ConsumerLoanService) { }
}
