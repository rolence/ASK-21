/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {createResourceReducer, getResourceLoadedAt, getResourceSelected, ResourceState} from '../../core/store/util/resource.reducer';
import {ActionReducerMap, createFeatureSelector} from '@ngrx/store';
import {createSelector} from 'reselect';
import * as fromProducts from '../store/loan-products.reducer';
import * as fromRoot from '../../core/store';
import * as fromSearch from './search-loan-products.reducer';
import {getEntities} from './search-loan-products.reducer';
import {InjectionToken} from '@angular/core';

export interface LoansState {
  productsSearch: fromSearch.State;
  products: ResourceState;
}

export interface State extends fromRoot.State {
  loans: LoansState;
}

export const reducers: ActionReducerMap<LoansState> = {
  productsSearch: fromSearch.reducer,
  products: createResourceReducer('Loan Product', fromProducts.reducer, 'shortName'),
};

export const reducerToken = new InjectionToken<ActionReducerMap<LoansState>>('Loan reducers');

export const reducerProvider = [
  { provide: reducerToken, useValue: reducers }
];

export const selectLoansState = createFeatureSelector<LoansState>('loans');

/**
 * Product selectors
 */
export const getProductSearchState = createSelector(selectLoansState, (state: LoansState) => state.productsSearch);
export const getSearchProducts = createSelector(getProductSearchState, getEntities);

export const getProductsState = createSelector(selectLoansState, (state: LoansState) => state.products);

export const getProductsLoadedAt = createSelector(getProductsState, getResourceLoadedAt);
export const getSelectedProduct = createSelector(getProductsState, getResourceSelected);
