/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {ActivatedRoute, Router} from '@angular/router';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';
import * as fromEmployees from '../store';
import {DELETE, SelectAction} from '../store/employee.actions';
import {Store} from '@ngrx/store';
import {Employee} from '../../core/services/office/domain/employee.model';
import {User} from '../../core/services/identity/domain/user.model';
import {DialogService} from '../../core/services/dialog/dialog.service';

@Component({
  templateUrl: './employee.detail.component.html'
})
export class EmployeeDetailComponent {

  employee$: Observable<Employee>;
  user$: Observable<User>;

  constructor(private route: ActivatedRoute, private router: Router, private dialogService: DialogService,
              private store: Store<fromEmployees.State>) {
    this.employee$ = this.store.select(fromEmployees.getSelectedEmployee);
    this.user$ = this.route.data.map(( data: { user: User }) => data.user);
  }

  searchEmployee(term): void {
    if (!term) {
      return;
    }
    this.goToOverviewPage(term);
  }

  confirmDeletion(): Observable<boolean> {
    return this.dialogService.openConfirm({
      message: 'Do you want to delete this employee?',
      title: 'Confirm deletion',
      acceptButton: 'DELETE EMPLOYEE',
    });
  }

  deleteEmployee(): void {
    this.confirmDeletion()
      .filter(accept => accept)
      .mergeMap(() => this.employee$.take(1))
      .subscribe(employee => {
        this.store.dispatch({ type: DELETE, payload: {
          employee,
          activatedRoute: this.route
        } });
      });
  }

  goToOverviewPage(term?: string): void {
    this.router.navigate(['../../'], { queryParams: { term: term }, relativeTo: this.route });
  }

  goToEditPage(): void {
    this.router.navigate(['edit'], { relativeTo: this.route });
  }
}
