/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {async, ComponentFixture, inject, TestBed} from '@angular/core/testing';
import {TranslateModule} from '@ngx-translate/core';
import {EmployeeFormComponent, EmployeeSaveEvent} from '../form.component';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {CreateEmployeeFormComponent} from './create.form.component';
import {mapEmployee, mapUser} from '../form.mapper';
import {CREATE} from '../../store/employee.actions';
import {Store} from '@ngrx/store';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import * as fromEmployees from '../../store';
import {SharedModule} from '../../../shared/shared.module';
import {IdentityService} from '../../../core/services/identity/identity.service';
import {OfficeService} from '../../../core/services/office/office.service';

const eventMock: EmployeeSaveEvent = {
  detailForm: {
    identifier: 'test',
    firstName: 'test',
    middleName: 'test',
    lastName: 'test',
    password: 'test',
    role: 'test',
    assignedOffice: 'test'
  },
  contactForm: {
    email: 'test',
    mobile: 'test',
    phone: 'test'
  }
};

let router: Router;

describe('Test employee form component', () => {

  let fixture: ComponentFixture<CreateEmployeeFormComponent>;

  let testComponent: CreateEmployeeFormComponent;

  beforeEach(() => {
    router = jasmine.createSpyObj('Router', ['navigate']);

    TestBed.configureTestingModule({
      declarations: [
        EmployeeFormComponent,
        CreateEmployeeFormComponent,
      ],
      imports: [
        TranslateModule.forRoot(),
        SharedModule.forRoot(),
        NoopAnimationsModule
      ],
      providers: [
        { provide: Router, useValue: router},
        { provide: ActivatedRoute, useValue: {} },
        {
          provide: Store, useClass: class {
          dispatch = jasmine.createSpy('dispatch');
          select = jasmine.createSpy('select').and.returnValue(Observable.empty());
        }},
        {
          provide: Store, useClass: class {
            dispatch = jasmine.createSpy('dispatch');
            select = jasmine.createSpy('select').and.returnValue(Observable.empty());
          }
        },
        {
          provide: IdentityService, useClass: class {
            listRoles = jasmine.createSpy('listRoles').and.returnValue(Observable.of([]));
          }
        },
        {
          provide: OfficeService, useClass: class {
            listOffices = jasmine.createSpy('listOffices').and.returnValue(Observable.of({
              offices: [],
              totalElements: 0,
              totalPages: 0
            }));
          }
        }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    });

    fixture = TestBed.createComponent(CreateEmployeeFormComponent);
    testComponent = fixture.componentInstance;
  });

  it('should test if employee is created', async(inject([Store], (store: Store<fromEmployees.State>) => {
    fixture.detectChanges();

    testComponent.onSave(eventMock);

    fixture.whenStable().then(() => {
      const employee = mapEmployee(eventMock);
      const user = mapUser(eventMock);

      expect(store.dispatch).toHaveBeenCalledWith({ type: CREATE, payload: {
        employee: employee,
        user: user,
        activatedRoute: {}
      }});
    });
  })));

  xit('should test if error is set on 409', async(() => {
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      expect(testComponent.formComponent.detailForm.get('identifier').errors).toBeDefined();
      expect(testComponent.formComponent.detailForm.get('identifier').errors['unique']).toBeTruthy();
    });
  }));
});
