/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {ITdDataTableColumn} from '@covalent/core';
import {TermProductDefinition} from '../../../core/services/depositAccount/domain/definition/term-product-definition.model';
import {TableData} from '../../../shared/data-table/data-table.component';

@Component({
  selector: 'aten-deposit-terms-detail',
  templateUrl: './terms-detail.component.html'
})
export class DepositTermsDetailComponent implements OnChanges {

  @Input() termProductDefinition: TermProductDefinition;

  tableData: TableData;

  columns: ITdDataTableColumn[] = [
    { name: 'identifier', label: 'Identifier' },
    { name: 'length', label: 'Length' },
    { name: 'period', label: 'Period' },
    { name: 'interest', label: 'Interest', format: value => value.amount },
  ];

  ngOnChanges(changes: SimpleChanges): void {
    const terms = this.termProductDefinition.terms;
    if (changes.termProductDefinition && terms && terms.length > 0) {
      this.tableData = {
        data: terms,
        totalElements: terms.length,
        totalPages: 1
      };
    }
  }
}
