/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {Charge} from '../../../core/services/depositAccount/domain/definition/charge.model';
import {TableData} from '../../../shared/data-table/data-table.component';

@Component({
  selector: 'aten-deposit-charges-detail',
  templateUrl: './charges-detail.component.html'
})
export class DepositChargesDetailComponent implements OnChanges {

  @Input() charges: Charge[];

  tableData: TableData;

  columns: any[] = [
    { name: 'name', label: 'Name' },
    { name: 'description', label: 'Description' },
    { name: 'actionIdentifier', label: 'Applied on' },
    { name: 'proportional', label: 'Proportional?' },
    { name: 'incomeAccount', label: 'Income account' },
    { name: 'amount', label: 'Amount', numeric: true, format: value => value ? value.toFixed(2) : undefined }
  ];

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.charges && this.charges && this.charges.length > 0) {
      this.tableData = {
        data: this.charges,
        totalElements: this.charges.length,
        totalPages: 1
      };
    }
  }
}
