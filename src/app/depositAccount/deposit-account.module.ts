/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {StoreModule} from '@ngrx/store';
import {reducerProvider, reducerToken} from './store/index';
import {EffectsModule} from '@ngrx/effects';
import {DatePipe} from '@angular/common';
import {NgModule} from '@angular/core';
import {DepositAccountRoutingModule} from './deposit-account-routing.module';
import {DepositProductComponent} from './deposit-account.component';
import {DepositProductChargesFormComponent} from './form/charges/charges.component';
import {DepositShareDetailComponent} from './shares/share.detail.component';
import {DepositShareIndexComponent} from './shares/share.index.component';
import {DepositProductDividendsComponent} from './shares/dividends/dividends.component';
import {DepositProductDividendApiEffects} from './store/dividends/effects/service.effects';
import {DepositProductDividendNotificationEffects} from './store/dividends/effects/notification.effects';
import {DepositProductDividendRouteEffects} from './store/dividends/effects/route.effects';
import {DividendFormComponent} from './shares/dividends/form/form.component';
import {CreateDividendFormComponent} from './shares/dividends/form/create.component';
import {TermProductExistsGuard} from './terms/term-product-exists.guard';
import {CheckingProductExistsGuard} from './checkings/checking-product-exists.guard';
import {SavingsProductExistsGuard} from './savings/savings-product-exists.guard';
import {ShareProductExistsGuard} from './shares/share-product-exists.guard';
import {DepositSavingDetailComponent} from './savings/saving.detail.component';
import {DepositTermDetailComponent} from './terms/term.detail.component';
import {DepositCheckingDetailComponent} from './checkings/checking.detail.component';
import {DepositCheckingProductApiEffects} from './store/checkings/effects/service.effects';
import {DepositShareProductApiEffects} from './store/shares/effects/service.effects';
import {DepositSavingProductApiEffects} from './store/savings/effects/service.effects';
import {DepositTermProductApiEffects} from './store/terms/effects/service.effects';
import {DepositTypeSelectionComponent} from './form/type-selection.component';
import {DepositSearchProductApiEffects} from './store/search/effects/service.effects';
import {DepositSavingsProductFormComponent} from './savings/form/form.component';
import {DepositCheckingsProductFormComponent} from './checkings/form/form.component';
import {DepositTermProductFormComponent} from './terms/form/form.component';
import {DepositShareProductFormComponent} from './shares/form/form.component';
import {DepositDetailFormComponent} from './form/detail/detail-form.component';
import {DepositInterestFormComponent} from './form/interest/interest-form.component';
import {DepositAdministrationFeeFormComponent} from './form/administrationFee/administration-fee-form.component';
import {DepositValueAddedTaxFormComponent} from './form/valueAddedTax/value-added-tax-form.component';
import {DepositInstallmentFormComponent} from './form/installment/installment-form.component';
import {DepositProductTermsFormComponent} from './form/terms/terms.component';
import {DepositSavingProductRouteEffects} from './store/savings/effects/route.effects';
import {DepositSavingProductNotificationEffects} from './store/savings/effects/notification.effects';
import {DepositShareProductRouteEffects} from './store/shares/effects/route.effects';
import {DepositShareProductNotificationEffects} from './store/shares/effects/notification.effects';
import {DepositCheckingProductNotificationEffects} from './store/checkings/effects/notification.effects';
import {DepositCheckingProductRouteEffects} from './store/checkings/effects/route.effects';
import {DepositTermProductRouteEffects} from './store/terms/effects/route.effects';
import {DepositTermProductNotificationEffects} from './store/terms/effects/notification.effects';
import {DepositChargesDetailComponent} from './detail/charges/charges-detail.component';
import {DepositMessageDetailComponent} from './detail/message/message-detail.component';
import {DepositAdministrationFeeDetailComponent} from './detail/administration-fee/administration-fee-detail.component';
import {DepositInstallmentDetailComponent} from './detail/installment/installment-detail.component';
import {DepositValueAddedTaxDetailComponent} from './detail/valueAddedTax/value-added-tax.component';
import {DepositLastChangesDetailComponent} from './detail/lastChanges/last-changes-detail.component';
import {DepositCommonDetailComponent} from './detail/common-detail/common-detail.component';
import {DepositInterestDetailComponent} from './detail/interest/interest-detail.component';
import {DepositTermIndexComponent} from './terms/term.index.component';
import {DepositCheckingIndexComponent} from './checkings/checking.index.component';
import {DepositSavingIndexComponent} from './savings/saving.index.component';
import {DepositTermsDetailComponent} from './detail/terms/terms-detail.component';
import {CreateDepositCheckingsProductFormComponent} from './checkings/form/create.form.component';
import {EditDepositCheckingsProductFormComponent} from './checkings/form/edit.form.component';
import {EditDepositSavingsProductFormComponent} from './savings/form/edit.form.component';
import {CreateDepositSavingsProductFormComponent} from './savings/form/create.form.component';
import {EditDepositSharesProductFormComponent} from './shares/form/edit.form.component';
import {CreateDepositSharesProductFormComponent} from './shares/form/create.form.component';
import {CreateDepositTermsProductFormComponent} from './terms/form/create.form.component';
import {EditDepositTermsProductFormComponent} from './terms/form/edit.form.component';
import {AdministrationFeeFormService} from './form/services/administration-fee-form.service';
import {DetailsFormService} from './form/services/details-form.service';
import {InstallmentFormService} from './form/services/installment-form.service';
import {InterestFormService} from './form/services/interest-form.service';
import {TermsFormService} from './form/services/terms-form.service';
import {ValueAddedTaxFormService} from './form/services/value-added-tax-form.service';
import {DepositProductsIndexComponent} from './deposit-accounts.index.component';
import {DepositACHTransactionListComponent} from './ach/ach-transaction.list.component';
import {DryRunComponent} from './standing-orders/dry-run/dry-run.component';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  imports: [
    DepositAccountRoutingModule,
    SharedModule,

    StoreModule.forFeature('deposit', reducerToken),

    EffectsModule.forFeature([
      DepositSearchProductApiEffects,

      DepositTermProductApiEffects,
      DepositTermProductRouteEffects,
      DepositTermProductNotificationEffects,

      DepositCheckingProductApiEffects,
      DepositCheckingProductRouteEffects,
      DepositCheckingProductNotificationEffects,

      DepositSavingProductApiEffects,
      DepositSavingProductRouteEffects,
      DepositSavingProductNotificationEffects,

      DepositShareProductApiEffects,
      DepositShareProductRouteEffects,
      DepositShareProductNotificationEffects,

      DepositProductDividendApiEffects,
      DepositProductDividendRouteEffects,
      DepositProductDividendNotificationEffects
    ])
  ],
  declarations: [
    DepositProductsIndexComponent,
    DepositProductComponent,
    DepositProductChargesFormComponent,
    DepositShareIndexComponent,
    DepositProductDividendsComponent,
    CreateDividendFormComponent,
    DividendFormComponent,
    DepositTermDetailComponent,
    DepositCheckingDetailComponent,
    DepositSavingDetailComponent,
    DepositShareDetailComponent,
    DepositTypeSelectionComponent,
    DepositTermProductFormComponent,
    DepositCheckingsProductFormComponent,
    DepositSavingsProductFormComponent,
    DepositShareProductFormComponent,
    DepositDetailFormComponent,
    DepositInterestFormComponent,
    DepositAdministrationFeeFormComponent,
    DepositValueAddedTaxFormComponent,
    DepositInstallmentFormComponent,
    DepositProductTermsFormComponent,
    DepositChargesDetailComponent,
    DepositTermsDetailComponent,
    DepositMessageDetailComponent,
    DepositInterestDetailComponent,
    DepositAdministrationFeeDetailComponent,
    DepositInstallmentDetailComponent,
    DepositValueAddedTaxDetailComponent,
    DepositLastChangesDetailComponent,
    DepositCommonDetailComponent,
    DepositTermIndexComponent,
    DepositShareIndexComponent,
    DepositCheckingIndexComponent,
    DepositSavingIndexComponent,

    // Checking
    CreateDepositCheckingsProductFormComponent,
    EditDepositCheckingsProductFormComponent,

    // Saving
    CreateDepositSavingsProductFormComponent,
    EditDepositSavingsProductFormComponent,

    // Share
    CreateDepositSharesProductFormComponent,
    EditDepositSharesProductFormComponent,

    // Term
    CreateDepositTermsProductFormComponent,
    EditDepositTermsProductFormComponent,
    DividendFormComponent,

    DepositACHTransactionListComponent,
    DryRunComponent
  ],
  providers: [
    TermProductExistsGuard,
    CheckingProductExistsGuard,
    SavingsProductExistsGuard,
    ShareProductExistsGuard,
    AdministrationFeeFormService,
    DetailsFormService,
    InstallmentFormService,
    InterestFormService,
    TermsFormService,
    ValueAddedTaxFormService,
    DatePipe,
    reducerProvider
  ]
})
export class DepositAccountModule {}
