/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {Component} from '@angular/core';
import * as fromDepositAccounts from '../../store/index';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {UpdateProductDefinitionAction} from '../../store/terms/term.actions';
import {Store} from '@ngrx/store';
import {TermProductDefinition} from '../../../core/services/depositAccount/domain/definition/term-product-definition.model';

@Component({
  templateUrl: './edit.form.component.html'
})
export class EditDepositTermsProductFormComponent {

  term$: Observable<TermProductDefinition>;

  constructor(private router: Router, private route: ActivatedRoute, private store: Store<fromDepositAccounts.State>) {
    this.term$ = store.select(fromDepositAccounts.getSelectedTermProduct)
      .filter(product => !!product);
  }

  save(term: TermProductDefinition): void {
    this.store.dispatch(new UpdateProductDefinitionAction({
      term,
      activatedRoute: this.route
    }));
  }

  cancel(): void {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

}
