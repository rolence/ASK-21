/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {ActivatedRoute} from '@angular/router';
import {SelectAction} from '../store/terms/term.actions';
import * as fromDepositAccounts from '../store';
import {Store} from '@ngrx/store';
import {TermProductDefinition} from '../../core/services/depositAccount/domain/definition/term-product-definition.model';
import {BreadCrumbService} from '../../shared/bread-crumbs/bread-crumb.service';

@Component({
  templateUrl: './term.index.component.html'
})
export class DepositTermIndexComponent implements OnInit, OnDestroy {

  private actionsSubscription: Subscription;
  private productSubscription: Subscription;

  constructor(private route: ActivatedRoute, private store: Store<fromDepositAccounts.State>,
              private breadCrumbService: BreadCrumbService) {}

  ngOnInit(): void {
    this.actionsSubscription = this.route.params
      .map(params => new SelectAction(params['id']))
      .subscribe(this.store);

    this.productSubscription = this.store.select(fromDepositAccounts.getSelectedTermProduct)
      .filter(product => !!product)
      .subscribe((term: TermProductDefinition) => this.breadCrumbService.setCustomBreadCrumb(this.route.snapshot, term.name));
  }

  ngOnDestroy(): void {
    this.actionsSubscription.unsubscribe();
    this.productSubscription.unsubscribe();
  }
}
