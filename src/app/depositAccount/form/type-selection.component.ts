/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {typeOptionList} from '../domain/type-option-list.model';
import {Component, EventEmitter, Input, Output} from '@angular/core';
import {MatRadioChange} from '@angular/material';
import {Type} from '../../core/services/depositAccount/domain/type.model';

@Component({
  selector: 'aten-deposit-type-selection',
  templateUrl: './type-selection.component.html'
})
export class DepositTypeSelectionComponent {

  @Input() type: Type;

  @Output() change = new EventEmitter<Type>();

  typeOptions = typeOptionList;

  selectionChange(change: MatRadioChange): void {
    this.change.emit(change.value);
  }
}
