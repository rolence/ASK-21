/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {Component, EventEmitter, Input, Output} from '@angular/core';
import {AbstractControl, FormArray, FormGroup} from '@angular/forms';
import {periodOptionList} from '../../domain/period-option-list.model';

@Component({
  selector: 'aten-deposit-product-terms-form',
  templateUrl: './terms.component.html'
})
export class DepositProductTermsFormComponent {

  @Input() formGroup: FormGroup;

  @Output() onAddTerm = new EventEmitter<void>();
  @Output() onRemoveTerm = new EventEmitter<number>();

  periodOptions = periodOptionList;

  addTerm(): void {
    this.onAddTerm.emit();
  }

  removeTerm(index: number): void {
    this.onRemoveTerm.emit(index);
  }

  get terms(): AbstractControl[] {
    const terms: FormArray = this.formGroup.get('terms') as FormArray;
    return terms.controls;
  }

  getFormGroup(index: number): FormGroup {
    const terms = this.formGroup.get('terms') as FormArray;
    return terms.at(index) as FormGroup;
  }
}
