/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {Injectable} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Currency} from '../../../core/services/depositAccount/domain/definition/currency.model';
import {Currency as CurrencyServiceModel} from '../../../core/services/currency/domain/currency.model';
import {Charge} from '../../../core/services/depositAccount/domain/definition/charge.model';
import {DepositAccountService} from '../../../core/services/depositAccount/deposit-account.service';
import {AccountingService} from '../../../core/services/accounting/accounting.service';
import {CurrencyService} from '../../../core/services/currency/currency.service';
import {FimsValidators} from '../../../core/validator/validators';
import {ledgerExists} from '../../../core/validator/ledger-exists.validator';
import {accountExists} from '../../../core/validator/account-exists.validator';
import {Observable} from 'rxjs/Observable';
import {Action} from '../../../core/services/depositAccount/domain/definition/action.model';

export interface DepositProductDetails {
  shortCode: string;
  name: string;
  description: string;
  currency: Currency;
  minimumBalance: string;
  equityLedger: string;
  payableAccount?: string;
  charges: Charge[];
}


@Injectable()
export class DetailsFormService {

  private _currencies: CurrencyServiceModel[] = [];

  private _detailFormGroup: FormGroup = new FormGroup({});

  private _chargesFormGroup: FormGroup = new FormGroup({});

  constructor(private formBuilder: FormBuilder, private depositService: DepositAccountService,
              private accountingService: AccountingService, private currencyService: CurrencyService) {
  }

  initDetailForm(): FormGroup {
    this._detailFormGroup = this.buildDetailForm();

    return this._detailFormGroup;
  }

  initChargesForm(): FormGroup {
    this._chargesFormGroup = this.buildChargesForm();

    return this._chargesFormGroup;
  }

  private buildDetailForm(): FormGroup {
    return this.formBuilder.group({
      shortCode: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(32), FimsValidators.urlSafe]],
      name: ['', [Validators.required, Validators.maxLength(256)]],
      description: ['', Validators.maxLength(4096)],
      currencyCode: ['', [Validators.required]],
      minimumBalance: ['', [Validators.required, FimsValidators.minValue(0)]],
      equityLedger: ['', [Validators.required], ledgerExists(this.accountingService)],
      payableAccount: ['', [], accountExists(this.accountingService)],
    });
  }

  private buildChargesForm(): FormGroup {
    return this.formBuilder.group({
      charges: this.initCharges([])
    });
  }

  private initCharges(charges: Charge[]): FormArray {
    const formControls: FormGroup[] = [];
    charges.forEach(charge => formControls.push(this.initCharge(charge)));
    return this.formBuilder.array(formControls);
  }

  private initCharge(charge?: Charge): FormGroup {
    const amount = charge ? charge.amount : 0;

    return this.formBuilder.group({
      actionIdentifier: [charge ? charge.actionIdentifier : '', Validators.required],
      incomeAccount: [charge ? charge.incomeAccount : '', [Validators.required], accountExists(this.accountingService)],
      name: [charge ? charge.name : '', [Validators.required, Validators.maxLength(256)]],
      description: [charge ? charge.description : '', Validators.maxLength(2048)],
      proportional: [charge ? charge.proportional : false ],
      amount: [amount.toFixed(2), [ FimsValidators.minValue(0)] ]
    });
  }

  addCharge(charge?: Charge): void {
    const charges: FormArray = this.chargesFormGroup.get('charges') as FormArray;
    charges.push(this.initCharge(charge));
  }

  removeCharge(index: number): void {
    const charges: FormArray = this.chargesFormGroup.get('charges') as FormArray;
    charges.removeAt(index);
  }

  private getCharges(): Charge[] {
    const charges = this.chargesFormGroup.get('charges').value;

    return charges.map(charge => Object.assign({}, charge, {
      amount: parseFloat(charge.amount)
    }));
  }

  getDepositProductDetails(): DepositProductDetails {
    const foundCurrency = this._currencies.find(currency => currency.code === this.detailFormGroup.get('currencyCode').value);

    const charges: Charge[] = this.getCharges();

    const details: DepositProductDetails = {
      shortCode: this.detailFormGroup.get('shortCode').value,
      name: this.detailFormGroup.get('name').value,
      description: this.detailFormGroup.get('description').value,
      minimumBalance: this.detailFormGroup.get('minimumBalance').value,
      currency: {
        code: foundCurrency.code,
        name: foundCurrency.name,
        sign: foundCurrency.sign,
        scale: foundCurrency.digits
      },
      charges,
      equityLedger: this.detailFormGroup.get('equityLedger').value,
      payableAccount: this.detailFormGroup.get('payableAccount').value,
    };

    return details;
  }

  setDepositProductDetails(details: DepositProductDetails): void {
    this.detailFormGroup.reset({
      shortCode: details.shortCode,
      name: details.name,
      description: details.description,
      minimumBalance: details.minimumBalance,
      currencyCode: details.currency ? details.currency.code : '',
      equityLedger: details.equityLedger,
      payableAccount: details.payableAccount
    });

    if (details.charges) {
      details.charges.forEach(charge => this.addCharge(charge));
    }
  }

  fetchAndInitCurrencies(): Observable<CurrencyServiceModel[]> {
    return this.currencyService.fetchCurrencies()
      .do(currencies => this._currencies = currencies);
  }

  fetchActions(): Observable<Action[]> {
    return this.depositService.fetchActions();
  }

  get detailFormGroup(): FormGroup {
    return this._detailFormGroup;
  }

  get chargesFormGroup(): FormGroup {
    return this._chargesFormGroup;
  }

  get valid(): boolean {
    return this.detailFormGroup.valid && this.chargesFormGroup.valid;
  }

}
