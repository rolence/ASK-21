/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Injectable} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FimsValidators} from '../../../core/validator/validators';
import {Installment} from '../../../core/services/depositAccount/domain/definition/installment.model';

@Injectable()
export class InstallmentFormService {

  private _installmentFormGroup: FormGroup = new FormGroup({});

  constructor(private formBuilder: FormBuilder) {
  }

  init(): FormGroup {
    this._installmentFormGroup = this.buildInstallmentFormGroup();
    return this._installmentFormGroup;
  }

  private buildInstallmentFormGroup(): FormGroup {
    return this.formBuilder.group({
      enabled: [false],
      minimumBalance: [undefined, [FimsValidators.minValue(0)]],
      payable: ['MONTHS', [Validators.required]],
      payrollAllocation: [false, [Validators.required]],
    });
  }

  getInstallment(): Installment {
    if (this.installmentFormGroup.get('enabled').value) {
      const minimumBalance = this.installmentFormGroup.get('minimumBalance').value;

      return {
        minimumBalance: minimumBalance ? minimumBalance : undefined,
        payable: this.installmentFormGroup.get('payable').value,
        payrollAllocation: this.installmentFormGroup.get('payrollAllocation').value
      };
    }
  }

  setInstallment(installment?: Installment, inUse: boolean = false): void {
    if (installment) {
      this.installmentFormGroup.reset({
        enabled: true,
        minimumBalance: { value: installment.minimumBalance, disabled: inUse },
        payable: { value: installment.payable, disabled: inUse },
        payrollAllocation: { value: installment.payrollAllocation, disabled: inUse }
      });
    } else {
      this.installmentFormGroup.reset({
        enabled: { value: false, disabled: inUse },
        minimumBalance: '0.00',
        payable: 'MONTHS',
        payrollAllocation: false
      });
    }
  }

  get installmentFormGroup(): FormGroup {
    return this._installmentFormGroup;
  }

}
