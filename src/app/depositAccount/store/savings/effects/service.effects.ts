/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import {Observable} from 'rxjs/Observable';
import {Action} from '@ngrx/store';
import {of} from 'rxjs/observable/of';
import * as savingActions from '../saving.actions';
import {DepositAccountService} from '../../../../core/services/depositAccount/deposit-account.service';

@Injectable()
export class DepositSavingProductApiEffects {

  @Effect()
  createProduct$: Observable<Action> = this.actions$
    .ofType(savingActions.CREATE)
    .map((action: savingActions.CreateProductDefinitionAction) => action.payload)
    .mergeMap(payload =>
      this.depositService.createSavingsProduct(payload.saving)
        .map(() => new savingActions.CreateProductDefinitionSuccessAction({
          resource: payload.saving,
          activatedRoute: payload.activatedRoute
        }))
        .catch((error) => of(new savingActions.CreateProductDefinitionFailAction(error)))
    );

  @Effect()
  updateProduct$: Observable<Action> = this.actions$
    .ofType(savingActions.UPDATE)
    .map((action: savingActions.UpdateProductDefinitionAction) => action.payload)
    .mergeMap(payload =>
      this.depositService.changeSavingsProduct(payload.saving.shortCode, payload.saving)
        .map(() => new savingActions.UpdateProductDefinitionSuccessAction({
          resource: payload.saving,
          activatedRoute: payload.activatedRoute
        }))
        .catch((error) => of(new savingActions.UpdateProductDefinitionFailAction(error)))
    );

  @Effect()
  deleteProduct$: Observable<Action> = this.actions$
    .ofType(savingActions.DELETE)
    .map((action: savingActions.DeleteProductDefinitionAction) => action.payload)
    .mergeMap(payload =>
      this.depositService.deleteShareProduct(payload.saving.shortCode)
        .map(() => new savingActions.DeleteProductDefinitionSuccessAction({
          resource: payload.saving,
          activatedRoute: payload.activatedRoute
        }))
        .catch((error) => of(new savingActions.DeleteProductDefinitionFailAction(error)))
    );

  @Effect()
  executeCommand$: Observable<Action> = this.actions$
    .ofType(savingActions.EXECUTE_COMMAND)
    .map((action: savingActions.ExecuteCommandAction) => action.payload)
    .mergeMap(payload =>
      this.depositService.processTermCommand(payload.definitionId, payload.command)
        .map(() => new savingActions.ExecuteCommandSuccessAction(payload))
        .catch((error) => of(new savingActions.ExecuteCommandFailAction(error)))
    );

  constructor(private actions$: Actions, private depositService: DepositAccountService) { }
}
