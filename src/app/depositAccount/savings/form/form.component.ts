/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Currency} from '../../../core/services/currency/domain/currency.model';
import {Action} from '../../../core/services/depositAccount/domain/definition/action.model';
import {Observable} from 'rxjs/Observable';
import {DepositProductDetails, DetailsFormService} from '../../form/services/details-form.service';
import {InterestFormService} from '../../form/services/interest-form.service';
import {AdministrationFeeFormService} from '../../form/services/administration-fee-form.service';
import {ValueAddedTaxFormService} from '../../form/services/value-added-tax-form.service';
import {InstallmentFormService} from '../../form/services/installment-form.service';
import {SavingsProductDefinition} from '../../../core/services/depositAccount/domain/definition/savings-product-definition.model';
import {Type} from '../../../core/services/depositAccount/domain/type.model';

@Component({
  selector: 'aten-deposit-savings-form',
  templateUrl: './form.component.html'
})
export class DepositSavingsProductFormComponent implements OnChanges {

  @Input() saving: SavingsProductDefinition;
  @Input() editMode = false;

  @Output() save: EventEmitter<SavingsProductDefinition> = new EventEmitter<SavingsProductDefinition>();
  @Output() cancel: EventEmitter<void> = new EventEmitter<void>();
  @Output() typeChanged: EventEmitter<Type> = new EventEmitter<Type>();

  actions$: Observable<Action[]>;
  currencies: Currency[];
  detailFormGroup: FormGroup;
  chargesFormGroup: FormGroup;
  interestFormGroup: FormGroup;
  administrationFormGroup: FormGroup;
  valueAddedTaxFormGroup: FormGroup;
  installmentFormGroup: FormGroup;

  constructor(private detailsFormService: DetailsFormService, private interestFormService: InterestFormService,
              private administrationFeeFormService: AdministrationFeeFormService,
              private valueAddedTaxFormService: ValueAddedTaxFormService, private installmentFormService: InstallmentFormService) {
    detailsFormService.fetchAndInitCurrencies().subscribe(currencies => this.currencies = currencies);

    this.detailFormGroup = detailsFormService.initDetailForm();
    this.chargesFormGroup = detailsFormService.initChargesForm();
    this.interestFormGroup = interestFormService.init();
    this.administrationFormGroup = administrationFeeFormService.init();
    this.valueAddedTaxFormGroup = valueAddedTaxFormService.init();
    this.installmentFormGroup = installmentFormService.init();

    this.actions$ = this.detailsFormService.fetchActions();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.saving && this.saving) {
      this.detailsFormService.setDepositProductDetails({
        shortCode: this.saving.shortCode,
        name: this.saving.name,
        description: this.saving.description,
        minimumBalance: this.saving.minimumBalance,
        equityLedger: this.saving.equityLedger,
        payableAccount: this.saving.payableAccount,
        currency: this.saving.currency,
        charges: this.saving.charges
      });

      this.interestFormService.setInterest(this.saving.interest, true);
      this.administrationFeeFormService.setAdministrationFee(this.saving.administrationFee);
      this.valueAddedTaxFormService.setValueAddedTax(this.saving.valueAddedTax);
      this.installmentFormService.setInstallment(this.saving.installment, this.saving.inUse);
    }
  }

  onSave(): void {
    const details: DepositProductDetails = this.detailsFormService.getDepositProductDetails();

    const saving: SavingsProductDefinition = {
      shortCode: details.shortCode,
      name: details.name,
      description: details.description,
      minimumBalance: details.minimumBalance,
      currency: details.currency,
      charges: details.charges,
      equityLedger: details.equityLedger,
      payableAccount: details.payableAccount,
      active: this.saving.active,
      interest: this.interestFormService.getInterest(),
      administrationFee: this.administrationFeeFormService.getAdministrationFee(),
      valueAddedTax: this.valueAddedTaxFormService.getValueAddedTax(),
      installment: this.installmentFormService.getInstallment(),
      inUse: this.saving ? this.saving.inUse : false
    };

    this.save.emit(saving);
  }

  onCancel(): void {
    this.cancel.emit();
  }

  onTypeChanged(type: Type): void {
    this.typeChanged.emit(type);
  }

  onAddCharge(): void {
    this.detailsFormService.addCharge();
  }

  onRemoveCharge(index: number): void {
    this.detailsFormService.removeCharge(index);
  }

  isValid(): boolean {
    return this.detailsFormService.valid && this.interestFormGroup.valid && this.administrationFormGroup.valid
      && this.valueAddedTaxFormGroup.valid;
  }
}
