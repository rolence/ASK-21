/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import * as fromDepositAccounts from './store';
import {Observable} from 'rxjs/Observable';
import {LOAD_ALL} from './store/search/search.actions';
import {Store} from '@ngrx/store';
import {TableData} from '../shared/data-table/data-table.component';
import {DisplayFimsNumber} from '../shared/number/fims-number.pipe';
import {FetchRequest} from '../core/services/domain/paging/fetch-request.model';
import {ProductOutline} from '../core/services/depositAccount/domain/definition/product-outline.model';

@Component({
  templateUrl: './deposit-account.component.html',
  providers: [DisplayFimsNumber]
})
export class DepositProductComponent {

  productData: Observable<TableData>;

  columns: any[] = [
    { name: 'shortCode', label: 'Short name' },
    { name: 'name', label: 'Name' },
    { name: 'type', label: 'Type' },
    { name: 'minimumBalance', label: 'Minimum balance', format: v => this.displayFimsNumber.transform(v) },
    { name: 'interest', label: 'Interest', format: v => this.displayFimsNumber.transform(v) },
    { name: 'active', label: 'Enabled'}
  ];

  constructor(private router: Router, private route: ActivatedRoute,
              private store: Store<fromDepositAccounts.State>, private displayFimsNumber: DisplayFimsNumber) {
    this.productData = this.store.select(fromDepositAccounts.getSearchProducts)
      .map(products => ({
        data: products,
        totalElements: products.length,
        totalPages: 1
      }));
  }

  fetchProducts(fetchRequest?: FetchRequest): void {
    this.store.dispatch({ type: LOAD_ALL });
  }

  rowSelect(productOutline: ProductOutline): void {
    this.router.navigate(['type', productOutline.type, productOutline.shortCode], { relativeTo: this.route });
  }

  addProduct(): void {
    this.router.navigate(['create/TERM'], { relativeTo: this.route });
  }
}
