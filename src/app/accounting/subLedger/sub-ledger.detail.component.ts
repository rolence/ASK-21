/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';
import * as fromAccounting from '../store';
import * as fromRoot from '../../core/store';
import {DELETE} from '../store/ledger/ledger.actions';
import {Account} from '../../core/services/accounting/domain/account.model';
import {Store} from '@ngrx/store';
import {FetchRequest} from '../../core/services/domain/paging/fetch-request.model';
import {Ledger} from '../../core/services/accounting/domain/ledger.model';
import {TableData, TableFetchRequest} from '../../shared/data-table/data-table.component';
import {SEARCH_BY_LEDGER} from '../../core/store/account/account.actions';
import {DialogService} from '../../core/services/dialog/dialog.service';
import {DisplayFimsNumber} from '../../shared/number/fims-number.pipe';

@Component({
  templateUrl: './sub-ledger.detail.component.html',
  providers: [DisplayFimsNumber]
})
export class SubLedgerDetailComponent implements OnInit, OnDestroy {

  private ledgerSubscription: Subscription;
  private lastFetchRequest: FetchRequest = {};

  ledger: Ledger;
  accountData$: Observable<TableData>;
  loading$: Observable<boolean>;

  columns: any[] = [
    { name: 'identifier', label: 'Id' },
    { name: 'name', label: 'Name' },
    { name: 'state', label: 'State' },
    { name: 'balance', label: 'Balance', format: value => value ? this.displayFimsNumber.transform(value) : '-' }
  ];

  constructor(private router: Router, private route: ActivatedRoute,
              private dialogService: DialogService, private store: Store<fromAccounting.State>,
              private displayFimsNumber: DisplayFimsNumber) {}

  ngOnInit(): void {
    this.ledgerSubscription = this.store.select(fromAccounting.getSelectedLedger)
      .filter(ledger => !!ledger)
      .subscribe(ledger => {
        this.ledger = ledger;
        this.fetchAccounts();
      });

    this.accountData$ = this.store.select(fromRoot.getAccountSearchResults)
      .map(accountPage => ({
        data: accountPage.accounts,
        totalElements: accountPage.totalElements,
        totalPages: accountPage.totalPages
      }));

    this.loading$ = this.store.select(fromRoot.getAccountSearchLoading);
  }

  ngOnDestroy(): void {
    this.ledgerSubscription.unsubscribe();
  }

  rowSelect(account: Account): void {
    this.router.navigate(['../../../../accounts/detail', account.identifier], { relativeTo: this.route });
  }

  confirmDeletion(): Observable<boolean> {
    return this.dialogService.openConfirm({
      message: 'Do you want to delete this ledger?',
      title: 'Confirm deletion',
      acceptButton: 'DELETE LEDGER'
    });
  }

  deleteLedger(): void {
    this.confirmDeletion()
      .filter(accept => accept)
      .subscribe(() => {
        this.store.dispatch({ type: DELETE, payload: {
          ledger: this.ledger,
          activatedRoute: this.route
        }});
      });
  }

  fetchAccounts(fetchRequest?: TableFetchRequest): void {
    if (fetchRequest) {
      this.lastFetchRequest = fetchRequest;
    }

    this.store.dispatch({ type: SEARCH_BY_LEDGER, payload: {
      ledgerId: this.ledger.identifier,
      fetchRequest: this.lastFetchRequest
    } });

  }

  addAccount(): void {
    this.router.navigate(['accounts/create'], { relativeTo: this.route });
  }

  editLedger(): void {
    this.router.navigate(['edit'], { relativeTo: this.route });
  }

  viewSubLedger(): void {
    this.router.navigate(['ledgers'], { relativeTo: this.route });
  }

}
