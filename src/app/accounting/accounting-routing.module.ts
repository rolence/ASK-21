/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {RouterModule, Routes} from '@angular/router';
import {GeneralLedgerComponent} from './general-ledger.component';
import {SubLedgerDetailComponent} from './subLedger/sub-ledger.detail.component';
import {AccountDetailComponent} from './accounts/account.detail.component';
import {AccountStatusComponent} from './status/status.component';
import {CommandsResolver} from './activity/commands.resolver';
import {AccountActivityComponent} from './activity/activity.component';
import {CreateAccountFormComponent} from './accounts/form/create/create.form.component';
import {SubLedgerComponent} from './subLedger/sub-ledger.component';
import {EditAccountFormComponent} from './accounts/form/edit/edit.form.component';
import {JournalEntryListComponent} from './journalEntries/journal-entry.list.component';
import {AccountEntryListComponent} from './accounts/entries/account-entry.list.component';
import {CreateLedgerFormComponent} from './form/create/create.form.component';
import {EditLedgerFormComponent} from './form/edit/edit.form.component';
import {LedgerExistsGuard} from './ledger-exists.guard';
import {AccountExistsGuard} from './accounts/account-exists.guard';
import {ChartOfAccountComponent} from './reporting/chartOfAccounts/chart-of-accounts.component';
import {SubLedgerListComponent} from './subLedger/sub-ledger.list.component';
import {TransactionTypeListComponent} from './transactionTypes/transaction-types.list.component';
import {EditTransactionTypeFormComponent} from './transactionTypes/form/edit/edit.form.component';
import {CreateTransactionTypeFormComponent} from './transactionTypes/form/create/create.form.component';
import {TransactionTypeExistsGuard} from './transactionTypes/transaction-type-exists.guard';
import {ChequesListComponent} from './cheques/cheques.list.component';
import {PayrollListComponent} from './payroll/payroll.list.component';
import {CreatePayrollFormComponent} from './payroll/form/create.form.component';
import {PaymentsListComponent} from './payroll/payments.list.component';
import {UploadPayrollFormComponent} from './payroll/form/upload/upload.form.component';
import {ChequeTemplatesComponent} from './accounts-payable/cheque-templates.component';
import {AccountsPayableComponent} from './accounts-payable/accounts-payable.component';
import {CreateChequeComponent} from './accounts-payable/form/create.component';
import {PayrollIndexComponent} from './payroll/payroll.index.component';
import {TransactionsListComponent} from './transactions/transactions.list.component';
import {TransactionsIndexComponent} from './transactions/transactions.index.component';
import {CreateTransactionFormComponent} from './transactions/form/create.form.component';
import {NgModule} from '@angular/core';
import {IndexComponent} from '../shared/util/index.component';
import {AccountIndexComponent} from './accounts/account-index.component';
import {BankInfosIndexComponent} from './bankInfos/bank-infos-index.component';
import {BankInfoListComponent} from './bankInfos/bank-info.list.component';
import {BankInfoCreateComponent} from './bankInfos/form/create/create.form.component';
import {BankInfoExistsGuard} from './bankInfos/bank-info-exists.guard';
import {BankInfoEditComponent} from './bankInfos/form/edit/edit.form.component';
import {BankInfoIndexComponent} from './bankInfos/bank-info.index.component';
import {BankInfoDetailComponent} from './bankInfos/bank-info.detail.component';

const AccountingRoutes: Routes = [
  {
    path: '', component: IndexComponent,
    data: {
      title: 'General ledger'
    },
    children: [
      {
        path: '', component: GeneralLedgerComponent,
      },
      {
        path: 'ledgers/detail/:id',
        component: SubLedgerComponent,
        canActivate: [LedgerExistsGuard],
        data: {
          hasPermission: {id: 'accounting_ledgers', accessLevel: 'READ'}
        },
        children: [
          {
            path: '',
            component: SubLedgerDetailComponent,
            data: {
              title: 'Sub ledger'
            }
          },
          {
            path: 'edit',
            component: EditLedgerFormComponent,
            data: {
              hasPermission: {id: 'accounting_ledgers', accessLevel: 'CHANGE'},
              title: 'Edit ledger'
            }
          },
          {
            path: 'ledgers',
            component: IndexComponent,
            data: {
              title: 'Sub ledgers'
            },
            children: [
              {
                path: '',
                component: SubLedgerListComponent,
              },
              {
                path: 'edit',
                component: EditLedgerFormComponent,
                data: {
                  hasPermission: {id: 'accounting_ledgers', accessLevel: 'CHANGE'},
                  title: 'Edit ledger'
                }
              },
              {
                path: 'create',
                component: CreateLedgerFormComponent,
                data: {
                  hasPermission: {id: 'accounting_ledgers', accessLevel: 'CHANGE'},
                  title: 'Add ledger'
                }
              }
            ]
          },
          {
            path: 'accounts/create',
            component: CreateAccountFormComponent,
            data: {
              hasPermission: {id: 'accounting_accounts', accessLevel: 'CHANGE'},
              title: 'Add account'
            }
          }
        ]
      },
      {
        path: 'create',
        component: CreateLedgerFormComponent,
        data: {
          hasPermission: {id: 'accounting_ledgers', accessLevel: 'CHANGE'},
          title: 'Add ledger'
        }
      }
    ]
  },
  {
    path: 'accounts/detail/:id',
    component: AccountIndexComponent,
    canActivate: [AccountExistsGuard],
    data: {
      hasPermission: {id: 'accounting_accounts', accessLevel: 'READ'},
    },
    children: [
      {
        path: '',
        component: AccountDetailComponent
      },
      {
        path: 'edit',
        component: EditAccountFormComponent,
        data: {
          title: 'Add account'
        }
      },
      {
        path: 'tasks',
        component: AccountStatusComponent,
        data: {
          title: 'Tasks'
        }
      },
      {
        path: 'activities',
        component: AccountActivityComponent,
        resolve: {commands: CommandsResolver},
        data: {
          title: 'Activities'
        }
      },
      {
        path: 'entries',
        component: AccountEntryListComponent,
        data: {
          title: 'Account entries'
        }
      }
    ]
  },
  {
    path: 'transactiontypes',
    component: IndexComponent,
    data: {
      hasPermission: {id: 'accounting_tx_types', accessLevel: 'READ'},
      title: 'Transaction types'
    },
    children: [
      {
        path: '',
        component: TransactionTypeListComponent,
      },
      {
        path: 'create',
        component: CreateTransactionTypeFormComponent,
        data: {
          hasPermission: {id: 'accounting_tx_types', accessLevel: 'CHANGE'},
          title: 'Add transaction type'
        }
      },
      {
        path: 'edit/:code',
        component: EditTransactionTypeFormComponent,
        canActivate: [TransactionTypeExistsGuard],
        data: {
          hasPermission: {id: 'accounting_tx_types', accessLevel: 'CHANGE'},
          title: 'Edit transaction type'
        }
      }
    ]
  },
  {
    path: 'journalEntries',
    component: JournalEntryListComponent,
    data: {
      hasPermission: {id: 'accounting_journals', accessLevel: 'READ'},
      title: 'Journal entries'
    }
  },
  {
    path: 'transactions',
    component: TransactionsIndexComponent,
    data: {
      title: 'Pending journal entries',
      hasPermission: {id: 'accounting_tx', accessLevel: 'READ'},
    },
    children: [
      {
        path: '',
        component: TransactionsListComponent,
      },
      {
        path: 'create',
        component: CreateTransactionFormComponent,
        data: {
          hasPermission: {id: 'accounting_tx', accessLevel: 'CHANGE'},
          title: 'Add journal entry'
        }
      }
    ]
  },
  {
    path: 'cheques',
    component: ChequesListComponent,
    data: {
      hasPermission: {id: 'cheque_management', accessLevel: 'READ'},
      title: 'Cheques'
    }
  },
  {
    path: 'accountsPayable',
    component: AccountsPayableComponent,
    data: {
      title: 'Accounts payable',
      hasPermission: {id: 'cheque_management', accessLevel: 'READ'},
    },
    children: [
      {
        path: '',
        component: ChequeTemplatesComponent,
      },
      {
        path: 'create',
        component: CreateChequeComponent,
        data: {
          title: 'Add cheque',
          hasPermission: {id: 'cheque_management', accessLevel: 'CHANGE'}
        }
      }
    ]
  },
  {
    path: 'payrolls',
    component: PayrollIndexComponent,
    data: {
      hasPermission: {id: 'payroll_distribution', accessLevel: 'READ'},
      title: 'Payrolls'
    },
    children: [
      {
        path: '',
        component: PayrollListComponent,
        data: {
          hasPermission: {id: 'payroll_distribution', accessLevel: 'READ'},
          title: 'Payrolls'
        }
      },
      {
        path: 'create',
        component: CreatePayrollFormComponent,
        data: {
          hasPermission: {id: 'payroll_distribution', accessLevel: 'CHANGE'},
          title: 'Add payroll'
        }
      },
      {
        path: 'upload',
        component: UploadPayrollFormComponent,
        data: {
          hasPermission: {id: 'payroll_distribution', accessLevel: 'CHANGE'},
          title: 'Upload payroll'
        }
      },
      {
        path: 'payments/:id',
        component: PaymentsListComponent,
        data: {
          hasPermission: {id: 'payroll_distribution', accessLevel: 'READ'},
          title: 'Payments'
        }
      }
    ]
  },
  {
    path: 'chartOfAccounts',
    component: ChartOfAccountComponent,
    data: {
      hasPermission: {
        id: 'accounting_ledgers', accessLevel: 'READ'
      },
      title: 'Chart of accounts'
    }
  },
  {
    path: 'bankInfos',
    component: BankInfosIndexComponent,
    data: {
      hasPermission: {
        id: 'accounting_bank_infos', accessLevel: 'READ'
      },
      title: 'All bank infos'
    },
    children: [
      {
        path: '',
        component: BankInfoListComponent
      },
      {
        path: 'create',
        component: BankInfoCreateComponent,
        data: {
          title: 'Add bank info'
        }
      },
      {
        path: 'detail/:branchSortCode',
        component: BankInfoIndexComponent,
        canActivate: [BankInfoExistsGuard],
        children: [
          {
            path: '',
            component: BankInfoDetailComponent
          },
          {
            path: 'edit',
            component: BankInfoEditComponent,
            data: {
              hasPermission: {
                id: 'accounting_bank_infos', accessLevel: 'CHANGE'
              }
            }
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(AccountingRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AccountingRoutingModule {}
