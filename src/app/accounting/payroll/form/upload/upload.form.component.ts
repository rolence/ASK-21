/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {CreateUploadAction} from '../../../store/payroll/payroll-collection.actions';
import {CreateUploadEvent, FetchEvent} from './form.component';
import {Store} from '@ngrx/store';
import * as fromAccounting from '../../../store';
import {PayrollPaymentPage} from '../../../../core/services/payroll/domain/payroll-payment-page.model';
import {PayrollService} from '../../../../core/services/payroll/payroll.service';

@Component({
  templateUrl: './upload.form.component.html'
})
export class UploadPayrollFormComponent {

  identifier$: Observable<string>;
  payments$: Observable<PayrollPaymentPage>;

  constructor(private store: Store<fromAccounting.State>, private router: Router,
              private route: ActivatedRoute, private payrollService: PayrollService) {}

  onUpload(file: File): void {
    this.identifier$ = this.payrollService.upload(file)
      .share();

    this.payments$ = this.identifier$
      .mergeMap(identifier => this.payrollService.fetchPayments(identifier));
  }

  onFetch(event: FetchEvent): void {
    this.payments$ = this.payrollService.fetchPayments(event.identifier, event.fetchRequest);
  }

  onSave(event: CreateUploadEvent): void {
    this.store.dispatch(new CreateUploadAction({
      identifier: event.identifier,
      processData: event.processData,
      activatedRoute: this.route
    }));
  }

  onCancel(): void {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

}
