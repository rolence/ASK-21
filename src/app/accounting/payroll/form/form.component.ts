/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {AbstractControl, FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {customerWithConfigExists} from './validator/customer-payroll-exists.validator';
import {PayrollPayment} from '../../../core/services/payroll/domain/payroll-payment.model';
import {PayrollCollectionSheet} from '../../../core/services/payroll/domain/payroll-collection-sheet.model';
import {AccountingService} from '../../../core/services/accounting/accounting.service';
import {PayrollService} from '../../../core/services/payroll/payroll.service';
import {accountExists} from '../../../core/validator/account-exists.validator';
import {FimsValidators} from '../../../core/validator/validators';

@Component({
  selector: 'aten-payroll-form',
  templateUrl: './form.component.html'
})

export class PayrollFormComponent implements OnChanges {

  form: FormGroup;

  @Input('allpayrollPayments') allpayrollPayments: PayrollPayment[];

  @Output() save = new EventEmitter<PayrollCollectionSheet>();
  @Output() cancel = new EventEmitter<void>();

  constructor(private formBuilder: FormBuilder, private accountingService: AccountingService, private payrollService: PayrollService) {
    this.form = this.formBuilder.group({
      sourceAccountNumber: ['', [Validators.required], accountExists(accountingService)],
      payments: this.initPayments()
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.allpayrollPayments && this.allpayrollPayments) {
      this.addPayments(this.allpayrollPayments);
    }
  }

  onSave(): void {
    const sheet: PayrollCollectionSheet = {
      sourceAccountNumber: this.form.get('sourceAccountNumber').value,
      payrollPayments: this.form.get('payments').value
    };

    this.save.emit(sheet);
  }

  onCancel(): void {
    this.cancel.emit();
  }

  private initPayments(): FormArray {
    const formControls: FormGroup[] = [];
    return this.formBuilder.array(formControls);
  }

  private initPayment(customerIdentifier?: string, employer?: string, salary?: string): FormGroup {
    return this.formBuilder.group({
      customerIdentifier: [customerIdentifier, [Validators.required], customerWithConfigExists(this.payrollService)],
      employer: [employer, [Validators.required]],
      salary: [salary, [Validators.required, FimsValidators.greaterThanValue(0), FimsValidators.maxValue(9999999999.99999)]]
    });
  }

  addPayment(): void {
    const commands: FormArray = this.form.get('payments') as FormArray;
    commands.push(this.initPayment());
  }

  addPayments(payrollPayments: PayrollPayment[]): void {
    const commands: FormArray = this.form.get('payments') as FormArray;
    payrollPayments.forEach(payrollPayment => {
        commands.push(this.initPayment(payrollPayment.customerIdentifier, payrollPayment.employer, payrollPayment.salary));
    });
  }

  removePayment(index: number): void {
    const commands: FormArray = this.form.get('payments') as FormArray;
    commands.removeAt(index);
  }

  get payments(): AbstractControl[] {
    const commands: FormArray = this.form.get('payments') as FormArray;
    return commands.controls;
  }
}
