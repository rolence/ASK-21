/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {createEntityAdapter, EntityAdapter, EntityState} from '@ngrx/entity';
import {TransactionActions, TransactionActionTypes} from './transaction.actions';
import {Transaction} from '../../../core/services/accounting/domain/transaction.model';

export const adapter: EntityAdapter<Transaction> = createEntityAdapter<Transaction>({
  selectId: transaction => transaction.identifier
});

export const initialState: EntityState<Transaction> = adapter.getInitialState();

export function reducer(state = initialState, action: TransactionActions): EntityState<Transaction> {

  switch (action.type) {

    case TransactionActionTypes.LOAD_ALL: {
      return adapter.removeAll(state);
    }

    case TransactionActionTypes.LOAD_ALL_COMPLETE: {
      return adapter.addAll(action.payload.transactions, state);
    }

    case TransactionActionTypes.CREATE_SUCCESS: {
      return adapter.addOne(action.payload.transaction, state);
    }

    case TransactionActionTypes.APPROVE_SUCCESS: {
      return adapter.updateOne({
        id: action.payload.identifier,
        changes: {
          state: 'PROCESSED',
          journalEntry: action.payload.journalEntry
        }
      }, state);
    }

    case TransactionActionTypes.DECLINE_SUCCESS: {
      return adapter.updateOne({
        id: action.payload.identifier,
        changes: {
          state: 'DECLINED'
        }
      }, state);
    }

    default: {
      return state;
    }
  }
}

export const {
  selectAll: selectAllTransactions
} = adapter.getSelectors();
