/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Action} from '@ngrx/store';
import {State, Transaction} from '../../../core/services/accounting/domain/transaction.model';
import {RoutePayload} from '../../../core/store/util/route-payload';

export enum TransactionActionTypes {
  LOAD_ALL = '[Transaction] Load All',
  LOAD_ALL_COMPLETE = '[Transaction] Load All Complete',

  CREATE = '[Transaction] Create',
  CREATE_SUCCESS = '[Transaction] Create Success',
  CREATE_FAIL = '[Transaction] Create Fail',

  APPROVE = '[Transaction] Approve',
  APPROVE_SUCCESS = '[Transaction] Approve Success',
  APPROVE_FAIL = '[Transaction] Approve Fail',

  DECLINE = '[Transaction] Decline',
  DECLINE_SUCCESS = '[Transaction] Decline Success',
  DECLINE_FAIL = '[Transaction] Decline Fail'
}

export interface TransactionPayload extends RoutePayload {
  transaction: Transaction;
}

export class LoadAllAction implements Action {
  readonly type = TransactionActionTypes.LOAD_ALL;

  constructor(public payload: { state: State }) { }
}

export class LoadAllCompleteAction implements Action {
  readonly type = TransactionActionTypes.LOAD_ALL_COMPLETE;

  constructor(public payload: { transactions: Transaction[] }) { }
}

export class CreateTransactionAction implements Action {
  readonly type = TransactionActionTypes.CREATE;

  constructor(public payload: TransactionPayload) { }
}

export class CreateTransactionSuccessAction implements Action {
  readonly type = TransactionActionTypes.CREATE_SUCCESS;

  constructor(public payload: TransactionPayload) { }
}

export class CreateTransactionFailAction implements Action {
  readonly type = TransactionActionTypes.CREATE_FAIL;

  constructor(public payload: { error: Error }) { }
}

export class ApproveTransactionAction implements Action {
  readonly type = TransactionActionTypes.APPROVE;

  constructor(public payload: { identifier: string }) { }
}

export class ApproveTransactionSuccessAction implements Action {
  readonly type = TransactionActionTypes.APPROVE_SUCCESS;

  constructor(public payload: { identifier: string, journalEntry: string }) { }
}

export class ApproveTransactionFailAction implements Action {
  readonly type = TransactionActionTypes.APPROVE_FAIL;

  constructor(public payload: { error: Error }) { }
}

export class DeclineTransactionAction implements Action {
  readonly type = TransactionActionTypes.DECLINE;

  constructor(public payload: { identifier: string }) { }
}

export class DeclineTransactionSuccessAction implements Action {
  readonly type = TransactionActionTypes.DECLINE_SUCCESS;

  constructor(public payload: { identifier: string }) { }
}

export class DeclineTransactionFailAction implements Action {
  readonly type = TransactionActionTypes.DECLINE_FAIL;

  constructor(public payload: { error: Error }) { }
}

export type TransactionActions
  = LoadAllAction
  | LoadAllCompleteAction
  | CreateTransactionAction
  | CreateTransactionSuccessAction
  | CreateTransactionFailAction
  | ApproveTransactionAction
  | ApproveTransactionSuccessAction
  | ApproveTransactionFailAction
  | DeclineTransactionAction
  | DeclineTransactionSuccessAction
  | DeclineTransactionFailAction;
