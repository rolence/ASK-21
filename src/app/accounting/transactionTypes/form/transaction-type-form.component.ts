/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {TransactionType} from '../../../core/services/accounting/domain/transaction-type.model';
import {FimsValidators} from '../../../core/validator/validators';
import {FormComponent} from '../../../shared/forms/form.component';

@Component({
  selector: 'aten-transaction-type-form',
  templateUrl: './transaction-type-form.component.html'
})
export class TransactionTypeFormComponent extends FormComponent<TransactionType> implements OnInit {

  @Input() transactionType: TransactionType;
  @Input() editMode = false;

  @Output() save = new EventEmitter<TransactionType>();
  @Output() cancel = new EventEmitter<void>();

  constructor(private formBuilder: FormBuilder) {
    super();
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      code: [this.transactionType.code, [Validators.required, Validators.minLength(3), Validators.maxLength(32), FimsValidators.urlSafe]],
      name: [this.transactionType.name, [Validators.required, Validators.maxLength(256)]],
      description: [this.transactionType.description, [Validators.maxLength(2048)]]
    });
  }

  showNumberValidationError(): void {
    this.setError('code', 'unique', true);
  }

  get formData(): TransactionType {
    // Not needed
    return;
  }

  onCancel(): void {
    this.cancel.emit();
  }

  onSave(): void {
    const transactionType: TransactionType = {
      code: this.form.get('code').value,
      name: this.form.get('name').value,
      description: this.form.get('description').value
    };

    this.save.emit(transactionType);
  }

}
