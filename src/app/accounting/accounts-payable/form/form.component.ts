/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ChequePayable} from '../../../core/services/cheque/domain/cheques-payable.model';
import {ChequeTemplate} from '../../../core/services/cheque/domain/cheque-template.model';
import {Currency} from '../../../core/services/currency/domain/currency.model';
import {PrintChequeInfo} from '../../../core/services/cheque/domain/print-cheque-info.model';
import {AccountingValidationService} from '../../../core/validator/services/accounting-validation.service';
import {OfficeValidationService} from '../../../core/validator/services/office-validation.service';
import {FimsValidators} from '../../../core/validator/validators';
import {todayAsDate} from '../../../core/services/domain/date.converter';

export interface ProcessChequeEvent {
  chequePayable: ChequePayable;
  chequeTemplate?: ChequeTemplate;
}

@Component({
  selector: 'aten-cheque-form',
  templateUrl: './form.component.html'
})
export class ChequeFormComponent {

  form: FormGroup;

  @Input() chequeTemplates: ChequeTemplate[];
  @Input() currencies: Currency[];
  @Input() chequeProcessed: boolean;

  @Output() processCheque = new EventEmitter<ProcessChequeEvent>();
  @Output() printCheque = new EventEmitter<PrintChequeInfo>();
  @Output() cancel = new EventEmitter<void>();

  constructor(private formBuilder: FormBuilder,
              private accountingValidationService: AccountingValidationService,
              private officeValidationService: OfficeValidationService) {
    this.form = this.formBuilder.group({
      template: [''],
      branch: ['', [Validators.required], officeValidationService.officeExists()],
      currencyCode: ['', [Validators.required]],
      bankAccount: ['', [Validators.required], accountingValidationService.accountExists()],
      payableAccount: ['', [Validators.required], accountingValidationService.accountExists()],
      chequeNumber: ['', [Validators.required]],
      payeeName: ['', [Validators.required]],
      amount: ['', [Validators.required, FimsValidators.greaterThanValue(0)]],
      saveAsTemplate: [false],
      templateName: ['', [Validators.required, Validators.minLength(3),
        Validators.maxLength(256), FimsValidators.urlSafe]]
    });

    this.form.get('template').valueChanges
      .subscribe((template: ChequeTemplate) => this.toggleTemplate(template));

    this.form.get('saveAsTemplate').valueChanges
      .startWith(false)
      .subscribe((saveAsTemplate: boolean) => {
        const templateNameControl = this.form.get('templateName');
        if (saveAsTemplate) {
          templateNameControl.enable();
        } else {
          templateNameControl.disable();
        }
      });
  }

  private toggleTemplate(template: ChequeTemplate): void {
    this.branchControl.setValue(template.branch);
    this.currencyControl.setValue(template.currencyCode);
    this.amountControl.setValue(template.amount);
    this.payeeNameControl.setValue(template.payeeName);
    this.bankAccountControl.setValue(template.bankAccount);
    this.payableAccountControl.setValue(template.payableAccount);
  }

  onProcessCheque(): void {
    const chequeTemplate: ChequeTemplate = {
      name: this.form.get('templateName').value,
      branch: this.branchControl.value,
      currencyCode: this.currencyControl.value,
      amount: this.amountControl.value,
      payeeName: this.payeeNameControl.value,
      bankAccount: this.bankAccountControl.value,
      payableAccount: this.payableAccountControl.value
    };

    const event: ProcessChequeEvent = {
      chequePayable: {
        bankAccount: this.bankAccountControl.value,
        payableAccount: this.payableAccountControl.value,
        amount: this.amountControl.value
      },
      chequeTemplate: this.form.get('saveAsTemplate').value ? chequeTemplate : null
    };
    this.processCheque.emit(event);
  }

  onPrintCheque(): void {
    const chequeInfo: PrintChequeInfo = {
      chequeNumber: this.form.get('chequeNumber').value,
      customerNumber: '',
      branch: this.branchControl.value,
      payeeName: this.payeeNameControl.value,
      amount: this.amountControl.value,
      chequeDate: todayAsDate(),
      currencyCode: this.currencyControl.value,
      transactionType: 'BCHQ'
    };
    this.printCheque.emit(chequeInfo);
  }

  onCancel(): void {
    this.cancel.emit();
  }

  get branchControl(): FormControl {
    return this.form.get('branch') as FormControl;
  }

  get currencyControl(): FormControl {
    return this.form.get('currencyCode') as FormControl;
  }

  get amountControl(): FormControl {
    return this.form.get('amount') as FormControl;
  }

  get payeeNameControl(): FormControl {
    return this.form.get('payeeName') as FormControl;
  }

  get bankAccountControl(): FormControl {
    return this.form.get('bankAccount') as FormControl;
  }

  get payableAccountControl(): FormControl {
    return this.form.get('payableAccount') as FormControl;
  }
}
