/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {ITdDataTableColumn} from '@covalent/core';
import * as fromAccounting from '../store';
import {ChequeCRUDActions, ProcessAction} from '../store/cheques/cheque.actions';
import {DatePipe} from '@angular/common';
import {MatDialog} from '@angular/material';
import {ChequeDialogComponent} from './cheque-dialog.component';
import {Store} from '@ngrx/store';
import {FimsCheque} from '../../core/services/cheque/domain/fims-cheque.model';
import {DialogService} from '../../core/services/dialog/dialog.service';

@Component({
  templateUrl: './cheques.list.component.html',
  providers: [DatePipe]
})
export class ChequesListComponent implements OnInit {

  cheques$: Observable<FimsCheque[]>;

  columns: ITdDataTableColumn[] = [
    {name: 'identifier', label: 'Identifier'},
    {name: 'drawee', label: 'Drawee'},
    {name: 'drawer', label: 'Drawer'},
    {name: 'payee', label: 'Payee'},
    {name: 'amount', label: 'Amount'},
    {name: 'dateIssued', label: 'Date issued', format: value => this.datePipe.transform(value, 'shortDate')},
    {name: 'state', label: 'State'}
  ];

  constructor(private store: Store<fromAccounting.State>, public dialog: MatDialog,
              private dialogService: DialogService, private datePipe: DatePipe) {
    this.cheques$ = store.select(fromAccounting.getAllChequeEntities);
  }

  ngOnInit(): void {
    this.store.dispatch(ChequeCRUDActions.loadAllAction({
      state: 'PENDING'
    }));
  }

  approveCheque(cheque: FimsCheque): void {
    this.openDialog()
      .filter(bankAccount => !!bankAccount)
      .subscribe(bankAccount => {
        this.store.dispatch(new ProcessAction({
          chequeIdentifier: cheque.identifier,
          command: {
            bankAccount,
            action: 'APPROVE'
          }
        }));
      });
  }

  cancelCheque(cheque: FimsCheque): void {
    this.dialogService.openConfirm({
      message: 'Do you want to CANCEL this cheque?',
      title: 'Confirm action',
      acceptButton: 'CANCEL cheque'
    })
      .filter(accept => accept)
      .subscribe(() => {
        this.store.dispatch(new ProcessAction({
          chequeIdentifier: cheque.identifier,
          command: {
            action: 'CANCEL'
          }
        }));
      });
  }

  private openDialog(): Observable<string | undefined> {
    return this.dialog.open(ChequeDialogComponent, {
      width: '40%',
      height: '40%'
    }).afterClosed();
  }
}
