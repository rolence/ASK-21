/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import * as fromAccounting from '../../store';
import {LOAD_CHART_OF_ACCOUNTS} from '../../store/ledger/ledger.actions';
import {Store} from '@ngrx/store';
import {ChartOfAccountEntry} from '../../../core/services/accounting/domain/chart-of-account-entry.model';
import {AccountingService} from '../../../core/services/accounting/accounting.service';

@Component({
  templateUrl: './chart-of-accounts.component.html'
})
export class ChartOfAccountComponent {

  chartOfAccountEntries$: Observable<ChartOfAccountEntry[]>;
  loading$: Observable<boolean>;

  constructor(private store: Store<fromAccounting.State>, private accountingService: AccountingService) {
    this.chartOfAccountEntries$ = this.store.select(fromAccounting.getChartOfAccountEntries);

    this.loading$ = this.store.select(fromAccounting.getChartOfAccountLoading);

    this.store.dispatch({ type: LOAD_CHART_OF_ACCOUNTS });
  }

  download(): void {
    this.accountingService.downloadChartOfAccounts();
  }

}
