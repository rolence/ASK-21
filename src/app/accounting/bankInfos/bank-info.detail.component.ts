/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import * as fromAccounting from '../store';
import {Store} from '@ngrx/store';
import {Component} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {BankInfo} from '../../core/services/accounting/domain/bank-info.model';
import {DeleteBankInfoAction} from '../store/bankInfo/bank-info.actions';
import {DialogService} from '../../core/services/dialog/dialog.service';

@Component({
  templateUrl: './bank-info.detail.component.html',
})
export class BankInfoDetailComponent {

  bankInfo$: Observable<BankInfo>;

  constructor(private router: Router, private route: ActivatedRoute,
              private store: Store<fromAccounting.State>, private dialogService: DialogService) {
    this.bankInfo$ = store.select(fromAccounting.getSelectedBankInfo);

    store.select(fromAccounting.getSelectedBankInfo)
      .subscribe(bankInfo => console.log(bankInfo));
  }

  edit(): void {
    this.router.navigate(['edit'], { relativeTo: this.route });
  }

  confirmDeletion(): Observable<boolean> {
    return this.dialogService.openConfirm({
      message: 'Do you want to delete this bank info?',
      title: 'Confirm deletion',
      acceptButton: 'DELETE BANK INFO',
    });
  }

  deleteBankInfo(bankInfo: BankInfo): void {
    this.confirmDeletion()
      .filter(accept => accept)
      .subscribe(() => this.store.dispatch(new DeleteBankInfoAction({
        bankInfo,
        activatedRoute: this.route
      })));
  }

}
