/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import * as fromAccounting from '../store';
import {Store} from '@ngrx/store';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Component} from '@angular/core';
import {BankInfo} from '../../core/services/accounting/domain/bank-info.model';
import {TableData} from '../../shared/data-table/data-table.component';
import {ITdDataTableColumn} from '@covalent/core';

@Component({
  templateUrl: './bank-info.list.component.html',
})
export class BankInfoListComponent {

  columns: ITdDataTableColumn[] = [
    { name: 'branchSortCode', label: 'Branch sort code' },
    { name: 'bic', label: 'BIC' },
    { name: 'checkDigitMethod', label: 'Check digit method' }
  ];

  bankInfos$: Observable<TableData>;

  constructor(private router: Router, private route: ActivatedRoute,
              private store: Store<fromAccounting.State>) {
    this.bankInfos$ = store.select(fromAccounting.selectAllBankInfos)
      .map(data => ({
        data,
        totalElements: data.length,
        totalPages: 1
      }));
  }

  addBankInfo(): void {
    this.router.navigate(['create'], { relativeTo: this.route });
  }

  rowSelect(bankInfo: BankInfo): void {
    this.router.navigate(['detail', bankInfo.branchSortCode], { relativeTo: this.route });
  }

}
