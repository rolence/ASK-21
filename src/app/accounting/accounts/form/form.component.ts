/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {Account} from '../../../core/services/accounting/domain/account.model';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import {AccountTypeOption, accountTypes} from '../../account-types.model';
import {FormComponent} from '../../../shared/forms/form.component';
import {FinancialInstitution} from '../../../core/services/office/domain/financial-institution.model';
import {OfficeService} from '../../../core/services/office/office.service';
import {FimsValidators, isObject} from '../../../core/validator/validators';
import {institutionExists} from '../../../core/validator/instituion-exists.validator';

@Component({
  selector: 'aten-account-form',
  templateUrl: './form.component.html'
})
export class AccountFormComponent extends FormComponent<Account> implements OnChanges {

  accounts: Observable<Account[]>;
  filteredInstitutions$: Observable<FinancialInstitution[]>;
  externalAccountReferenceForm: FormGroup;

  @Input() account: Account;
  @Input() editMode: boolean;

  @Output() save = new EventEmitter<Account>();
  @Output() cancel = new EventEmitter<void>();

  accountTypeOptions: AccountTypeOption[] = accountTypes;

  constructor(private formBuilder: FormBuilder, private officeService: OfficeService) {
    super();

    this.form = this.formBuilder.group({
      identifier: ['', [Validators.required, Validators.minLength(3),
        Validators.maxLength(34), FimsValidators.urlSafe]],
      name: ['', [Validators.required, Validators.maxLength(256)]],
      type: [{value: '', disabled: true}, [Validators.required]],
      ledger: ['', [Validators.required]],
      balance: [{value: '', disabled: this.editMode}, [Validators.required]],
    });

    this.externalAccountReferenceForm = this.formBuilder.group({
      enabled: [false],
      financialInstitution: ['', [Validators.required], institutionExists(this.officeService)],
      accountNumber: ['', [Validators.required]]
    });

    this.externalAccountReferenceForm.get('enabled').valueChanges
      .startWith(false)
      .subscribe( enabled => this.toggleExternalAccountEnabled(enabled));

    this.filteredInstitutions$ = this.externalAccountReferenceForm.get('financialInstitution').valueChanges
      .distinctUntilChanged()
      .debounceTime(500)
      .filter(searchTerm => searchTerm)
      .map(searchTerm => isObject(searchTerm) ? searchTerm.code : searchTerm)
      .switchMap(searchTerm =>
        this.officeService.fetchFinancialInstitutions({ searchTerm })
          .map(page => page.financialInstitutions)
      );
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.account && this.account) {
      this.form.reset({
        identifier: this.account.identifier,
        name: this.account.name,
        type: this.account.type,
        ledger: this.account.ledger,
        balance: this.account.balance
      });

      const externalReference = this.account.externalReference;
      if (externalReference) {
        this.officeService.findFinancialInstitution(externalReference.branchSortCode)
          .subscribe(institution => {
            this.externalAccountReferenceForm.reset({
              enabled: true,
              financialInstitution: institution,
              accountNumber: externalReference.accountNumber
            });
          });
      } else {
        this.externalAccountReferenceForm.reset({
          enabled: false
        });
      }
    }
  }

  private toggleExternalAccountEnabled(enabled: boolean): void {
    const financialInstitutionControl: FormControl = this.externalAccountReferenceForm.get('financialInstitution') as FormControl;
    const accountNumberControl: FormControl = this.externalAccountReferenceForm.get('accountNumber') as FormControl;

    if (enabled) {
      financialInstitutionControl.enable();
      accountNumberControl.enable();
    } else {
      financialInstitutionControl.disable();
      accountNumberControl.disable();
    }
  }

  showIdentifierValidationError(): void {
    this.setError('identifier', 'unique', true);
  }

  get formData(): Account {
    // Not needed
    return;
  }

  onSave(): void {
    const account: Account = {
      identifier: this.form.get('identifier').value,
      name: this.form.get('name').value,
      type: this.form.get('type').value,
      ledger: this.form.get('ledger').value,
      balance: this.form.get('balance').value
    };

    if (this.externalAccountReferenceForm.get('enabled').value) {
      const institution = this.externalAccountReferenceForm.get('financialInstitution').value as FinancialInstitution;
      account.externalReference = {
        financialInstitution: institution.name,
        branchSortCode: institution.code,
        accountNumber: this.externalAccountReferenceForm.get('accountNumber').value,
      };
    }

    this.save.emit(account);
  }

  onCancel(): void {
    this.cancel.emit();
  }


  get valid(): boolean {
    return this.form.valid && this.externalAccountReferenceForm.valid;
  }
}
