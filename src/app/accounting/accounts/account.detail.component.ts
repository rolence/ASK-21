/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import * as fromAccounting from '../store';
import * as fromRoot from '../../core/store';
import {Subscription} from 'rxjs/Subscription';
import {DELETE} from '../store/account/account.actions';
import {Observable} from 'rxjs/Observable';
import {Store} from '@ngrx/store';
import {DialogService} from '../../core/services/dialog/dialog.service';
import {FimsPermission} from '../../core/services/security/authz/fims-permission.model';
import {Account} from '../../core/services/accounting/domain/account.model';

@Component({
  templateUrl: './account.detail.component.html'
})
export class AccountDetailComponent implements OnInit, OnDestroy {

  private accountSubscription: Subscription;

  canDelete$: Observable<boolean>;
  account: Account;

  constructor(private router: Router, private route: ActivatedRoute,
              private dialogService: DialogService, private store: Store<fromAccounting.State>) {
  }

  ngOnInit(): void {
    const account$: Observable<Account> = this.store.select(fromAccounting.getSelectedAccount)
      .filter(account => !!account);

    this.canDelete$ = Observable.combineLatest(
      this.store.select(fromRoot.getPermissions),
      account$,
      (permissions, account: Account) => ({
        hasPermission: this.hasDeletePermission(permissions),
        isAccountClosed: account.state === 'CLOSED'
      }))
      .map(result => result.hasPermission && result.isAccountClosed);

    this.accountSubscription = account$
      .subscribe(account => this.account = account);
  }

  private hasDeletePermission(permissions: FimsPermission[]): boolean {
    return permissions.filter(permission =>
      permission.id === 'accounting_accounts' &&
      permission.accessLevel === 'DELETE'
    ).length > 0;
  }

  ngOnDestroy(): void {
    this.accountSubscription.unsubscribe();
  }

  confirmDeletion(): Observable<boolean> {
    return this.dialogService.openConfirm({
      message: 'Do you want to delete this account?',
      title: 'Confirm deletion',
      acceptButton: 'DELETE ACCOUNT'
    });
  }

  deleteAccount(): void {
    this.confirmDeletion()
      .filter(accept => accept)
      .subscribe(() => {
        this.store.dispatch({
          type: DELETE, payload: {
            account: this.account,
            activatedRoute: this.route
          }
        });
      });
  }

  editAccount(): void {
    this.router.navigate(['edit'], { relativeTo: this.route });
  }

  viewAccountEntries(): void {
    this.router.navigate(['entries'], { relativeTo: this.route });
  }

  viewTasks(): void {
    this.router.navigate(['tasks'], { relativeTo: this.route });
  }

  viewActivities(): void {
    this.router.navigate(['activities'], { relativeTo: this.route });
  }

  viewReferenceAccount(referenceAccount: string): void {
    this.router.navigate(['../', referenceAccount], { relativeTo: this.route });
  }

}
