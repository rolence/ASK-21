/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component} from '@angular/core';
import {DatePipe} from '@angular/common';
import {Observable} from 'rxjs/Observable';
import {TableData} from '../../../shared/data-table/data-table.component';
import {ReportingService} from '../../../core/services/reporting/reporting.service';
import {DisplayFimsNumber} from '../../../shared/number/fims-number.pipe';

@Component({
  templateUrl: './standing-order-listing.component.html',
  providers: [DatePipe, DisplayFimsNumber]
})
export class StandingOrderListingComponent {

  standingOrderListEntries$: Observable<TableData>;

  columns: any[] = [
    { name: 'member', label: 'Member id' },
    { name: 'memberAccount', label: 'Member account' },
    { name: 'payee', label: 'Payee' },
    { name: 'branchSortCode', label: 'Branch short code' },
    { name: 'accountNumber', label: 'Account number' },
    { name: 'interval', label: 'Interval' },
    { name: 'amount', label: 'Amount', format: (v: any) => this.displayFimsNumber.transform(v) },
    { name: 'purpose', label: 'Purpose' },
    { name: 'startDate', label: 'Start date', format: (v: any) => {
        return this.datePipe.transform(v, 'shortDate');
      }
    },
    {
      name: 'endDate', label: 'End date',  format: (v: any) => {
        return this.datePipe.transform(v, 'shortDate');
      }
    },
    {
      name: 'nextPaymentDate', label: 'Next payment date',  format: (v: any) => {
        return this.datePipe.transform(v, 'shortDate');
      }
    }
  ];

  constructor(private reportingService: ReportingService, private datePipe: DatePipe, private displayFimsNumber: DisplayFimsNumber) {
    this.standingOrderListEntries$ = this.reportingService.standingOrderListing()
      .map(data => ({
        data,
        totalElements: data.length,
        totalPages: 1
      })).share();
  }

  exportStandingOrderList(): void {
    this.reportingService.exportStandingOrderListing().subscribe();
  }
}
