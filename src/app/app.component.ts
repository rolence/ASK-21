import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {UpdateNotificationService} from './core/services/serviceWorker/update-notification.service';
import {getSelectedLanguage} from './core/services/i18n/translate';
import {ReLoginSuccessAction} from './core/store/security/security.actions';
import {Store} from '@ngrx/store';
import * as fromRoot from './core/store';
import {registerLocaleData} from '@angular/common';
import localeEn from '@angular/common/locales/en';
import localeEs from '@angular/common/locales/es';
import localeId from '@angular/common/locales/id';
import localeFr from '@angular/common/locales/fr';

@Component({
  selector: 'aten-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {

  constructor(private translate: TranslateService, private updateService: UpdateNotificationService,
              private store: Store<fromRoot.State>) {

  }

  ngOnInit(): void {
    this.initLanguages();
    this.relogin();
  }

  relogin(): void {
    this.store.select(fromRoot.getAuthenticationState)
      .filter(state => !!state.authentication)
      .take(1)
      .map(state => ({
        username: state.username,
        tenant: state.tenant,
        authentication: state.authentication
      }))
      .map(payload => new ReLoginSuccessAction(payload))
      .subscribe(this.store);
  }

  initLanguages(): void {
    registerLocaleData(localeEn, 'en');
    registerLocaleData(localeEs, 'es');
    registerLocaleData(localeId, 'id');
    registerLocaleData(localeFr, 'fr');

    this.translate.addLangs(['en', 'es', 'id', 'fr']);

    this.translate.setDefaultLang('en');

    this.translate.use(getSelectedLanguage(this.translate));
  }
}
