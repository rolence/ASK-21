/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild} from '@angular/core';
import {Customer} from '../../core/services/customer/domain/customer.model';
import {CustomerDetailFormComponent, CustomerDetailFormData} from './detail/detail.component';
import {Address} from '../../core/services/domain/address/address.model';
import {CustomerContactFormComponent} from './contact/contact.component';
import {ContactDetail} from '../../core/services/domain/contact/contact-detail.model';
import {Catalog} from '../../core/services/catalog/domain/catalog.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import {Country} from '../../core/services/country/model/country.model';
import {CountryService} from '../../core/services/country/country.service';
import {CustomFieldValueService} from '../../shared/custom-fields/services/custom-field-value.service';
import {countryExists} from '../../core/validator/country-exists.validator';

@Component({
  selector: 'aten-customer-form',
  templateUrl: './form.component.html'
})
export class CustomerFormComponent implements OnChanges {

  customValuesForm = new FormGroup({});
  addressForm: FormGroup;

  @Input('customer') customer: Customer;
  @Input('catalog') catalog: Catalog;
  @Input('editMode') editMode: boolean;

  @Output() save = new EventEmitter<Customer>();
  @Output() cancel = new EventEmitter<void>();

  @ViewChild('detailForm') detailForm: CustomerDetailFormComponent;
  detailFormData: CustomerDetailFormData;
  @ViewChild('contactForm') contactForm: CustomerContactFormComponent;
  contactFormData: ContactDetail[];

  selectedOffices: string[] = [];
  selectedEmployees: string[] = [];
  filteredCountries$: Observable<Country[]>;

  constructor(private formBuilder: FormBuilder, private customFieldValueService: CustomFieldValueService,
              private countryService: CountryService) {
    this.addressForm = this.formBuilder.group({
      street: ['', [Validators.required, Validators.maxLength(256)]],
      city: ['', [Validators.required, Validators.maxLength(256)]],
      postalCode: ['', Validators.maxLength(32)],
      region: ['', Validators.maxLength(256)],
      country: ['', [Validators.required], countryExists(this.countryService)]
    });

    this.filteredCountries$ = this.addressForm.get('country').valueChanges
      .map(country => country && typeof country === 'object' ? country.displayName : country)
      .map(searchTerm => this.countryService.fetchCountries(searchTerm));
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.catalog && this.catalog) {
      this.customValuesForm = this.customFieldValueService.initValuesForm(this.catalog.fields);
    }

    if (this.catalog && this.customer) {
      this.customFieldValueService.setValues(this.customer.customValues);
    }

    if (changes.customer && this.customer) {
      this.detailFormData = {
        identifier: this.customer.identifier,
        firstName: this.customer.givenName,
        middleName: this.customer.middleName,
        lastName: this.customer.surname,
        dateOfBirth: this.customer.dateOfBirth,
        member: this.customer.member
      };

      const address = this.customer.address;

      if (address) {
        const country = this.countryService.fetchByCountryCode(address.countryCode);

        this.addressForm.reset({
          street: address.street,
          city: address.city,
          postalCode: address.postalCode,
          region: address.region,
          country
        });
      }

      this.contactFormData = this.customer.contactDetails;
      this.selectedOffices = this.customer.assignedOffice ? [this.customer.assignedOffice] : [];
      this.selectedEmployees = this.customer.assignedEmployee ? [this.customer.assignedEmployee] : [];
    }
  }

  showIdentifierValidationError(): void {
    this.detailForm.setError('identifier', 'unique', true);
  }

  selectOffice(selections: string[]): void {
    this.selectedOffices = selections;
  }

  selectEmployee(selections: string[]): void {
    this.selectedEmployees = selections;
  }

  get isValid(): boolean {
    return (this.detailForm.valid && this.addressForm.valid)
      && this.customValuesForm.valid
      && this.contactForm.validWhenOptional;
  }

  onSave() {
    const detailFormData = this.detailForm.formData;

    const country: Country = this.addressForm.get('country').value;

    const address: Address = {
      street: this.addressForm.get('street').value,
      city: this.addressForm.get('city').value,
      postalCode: this.addressForm.get('postalCode').value,
      region: this.addressForm.get('region').value,
      country: country.name,
      countryCode: country.alpha2Code
    };

    const customer: Customer = {
      identifier: detailFormData.identifier,
      currentState: this.customer.currentState,
      givenName: detailFormData.firstName,
      surname: detailFormData.lastName,
      middleName: detailFormData.middleName,
      type: 'PERSON',
      address,
      contactDetails: this.contactForm.formData,
      dateOfBirth: detailFormData.dateOfBirth,
      member: detailFormData.member,
      assignedOffice: this.selectedOffices && this.selectedOffices.length > 0 ? this.selectedOffices[0] : undefined,
      assignedEmployee: this.selectedEmployees && this.selectedEmployees.length > 0 ? this.selectedEmployees[0] : undefined,
      customValues: this.customFieldValueService.getValues('customers')
    };
    this.save.emit(customer);
  }

  onCancel() {
    this.cancel.emit();
  }

}
