/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Catalog} from '../../../core/services/catalog/domain/catalog.model';
import {Field} from '../../../core/services/catalog/domain/field.model';

@Component({
  selector: 'aten-customer-catalog-form',
  templateUrl: './form.component.html'
})
export class CustomerCatalogFormComponent implements OnChanges {

  fields: Field[] = [];

  form: FormGroup;

  @Input('catalog') catalog: Catalog;

  @Output() save = new EventEmitter<Catalog>();
  @Output() cancel = new EventEmitter<void>();

  constructor(private formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(256)]],
      description: ['', [Validators.maxLength(4096)]]
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.catalog) {
      this.form.reset({
        name: this.catalog.name,
        description: this.catalog.description
      });

      this.fields = [...this.catalog.fields];
    }
  }

  onSave(): void {
    const catalog: Catalog = Object.assign({}, this.catalog, {
      name: this.form.get('name').value,
      description: this.form.get('description').value,
      fields: this.fields
    });

    this.save.emit(catalog);
  }

  onCancel(): void {
    this.cancel.emit();
  }

  saveField(fieldToSave: Field): void {
    const fields = this.fields.filter(field => field.identifier !== fieldToSave.identifier);

    this.fields = [...fields, fieldToSave];
  }

  removeField(fieldToRemove: Field): void {
    this.fields = this.fields.filter(field => field.identifier !== fieldToRemove.identifier);
  }
}
