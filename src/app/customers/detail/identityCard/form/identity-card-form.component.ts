/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {FormComponent} from '../../../../shared/forms/form.component';
import {IdentificationCard} from '../../../../core/services/customer/domain/identification-card.model';
import {FimsValidators} from '../../../../core/validator/validators';
import {ExpirationDate} from '../../../../core/services/customer/domain/expiration-date.model';

@Component({
  selector: 'aten-identity-card-form',
  templateUrl: './identity-card-form.component.html'
})
export class IdentityCardFormComponent extends FormComponent<IdentificationCard> implements OnInit {

  @Input() identificationCard: IdentificationCard;
  @Input() editMode = false;

  @Output() save = new EventEmitter<IdentificationCard>();
  @Output() cancel = new EventEmitter<void>();

  constructor(private formBuilder: FormBuilder) {
    super();
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      number: [this.identificationCard.number, [Validators.required, Validators.minLength(3), Validators.maxLength(32),
        FimsValidators.urlSafe]],
      type: [this.identificationCard.type, [Validators.required, Validators.maxLength(128)]],
      expirationDate: [this.formatDate(this.identificationCard.expirationDate), [Validators.required, FimsValidators.afterToday]],
      issuer: [this.identificationCard.issuer, [Validators.required, Validators.maxLength(256)]]
    });
  }

  showNumberValidationError(): void {
    this.setError('number', 'unique', true);
  }

  private formatDate(expirationDate: ExpirationDate): string {
    if (!expirationDate) {
      return '';
    }
    return `${expirationDate.year}-${this.addZero(expirationDate.month)}-${this.addZero(expirationDate.day)}`;
  }

  private addZero(value: number): string {
    return ('0' + value).slice(-2);
  }

  get formData(): IdentificationCard {
    // Not needed
    return;
  }

  onCancel(): void {
    this.cancel.emit();
  }

  onSave(): void {
    const expirationDate: string = this.form.get('expirationDate').value;
    const chunks: string[] = expirationDate.split('-');

    const identificationCard: IdentificationCard = {
      type: this.form.get('type').value,
      number: this.form.get('number').value,
      expirationDate: {
        day: Number(chunks[2]),
        month: Number(chunks[1]),
        year: Number(chunks[0])
      },
      issuer: this.form.get('issuer').value
    };

    this.save.emit(identificationCard);
  }

}
