/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {ActivatedRoute, Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';
import {Component} from '@angular/core';
import {CustomerDoubletValidation} from '../../../../core/services/customer/domain/customer-doublet-validation.model';
import * as fromCustomers from '../../../store';
import {UpdateDoubletValidationAction} from '../../../store/settings/doublet-validation.actions';

@Component({
  templateUrl: './create.form.component.html'
})
export class CreateDoubletValidationFormComponent {

  doubletValidation$: Observable<CustomerDoubletValidation>;

  constructor(private router: Router, private route: ActivatedRoute,
              private store: Store<fromCustomers.State>) {
    this.doubletValidation$ = store.select(fromCustomers.getCustomerDoubletValidation);
  }

  onSave(doubletValidation: CustomerDoubletValidation): void {
    this.store.dispatch(new UpdateDoubletValidationAction({
      doubletValidation,
      activatedRoute: this.route
    }));
  }

  onCancel(): void {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

}
