/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {AbstractControl, FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {CustomerDoubletValidation} from '../../../../core/services/customer/domain/customer-doublet-validation.model';
import {
  CustomerDoubletValidationValue,
  Field,
  Verification
} from '../../../../core/services/customer/domain/customer-doublet-validation-value.model';
import {FimsValidators} from '../../../../core/validator/validators';
import {fieldUnique} from './validator/field-unique.validator';

@Component({
  selector: 'aten-customer-doublet-validation-form',
  templateUrl: './form.component.html'
})
export class CustomerDoubletValidationFormComponent implements OnChanges {

  form: FormGroup;

  @Input('doubletValidation') doubletValidation: CustomerDoubletValidation;

  @Output() save = new EventEmitter<CustomerDoubletValidation>();
  @Output() cancel = new EventEmitter<void>();

  constructor(private formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({
      enabled: [false, [Validators.required]],
      values: this.initValues([])
    }, { validator: fieldUnique });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.doubletValidation) {
      this.form.reset({
        enabled: this.doubletValidation.enabled,
      });

      if (this.doubletValidation.values) {
        this.doubletValidation.values.forEach(value => this.addValue(value));
      }
    }
  }

  onSave(): void {
    const formArray = this.form.get('values') as FormArray;

    const format: CustomerDoubletValidation = {
      enabled: this.form.get('enabled').value,
      values: formArray.getRawValue()
    };

    this.save.emit(format);
  }

  onCancel(): void {
    this.cancel.emit();
  }

  private initValues(values: CustomerDoubletValidationValue[]): FormArray {
    const formControls: FormGroup[] = [];
    values.forEach(value => formControls.push(this.initValue(value)));
    return this.formBuilder.array(formControls);
  }

  initValue(value?: CustomerDoubletValidationValue): FormGroup {
    const form = this.formBuilder.group({
      field: [null, [Validators.required]],
      verification: [null, [Validators.required]],
      threshold: [null, [Validators.required, FimsValidators.minValue(1), FimsValidators.maxValue(10)]]
    });

    const verificationControl = form.get('verification');
    const thresholdControl = form.get('threshold');

    verificationControl.valueChanges.subscribe((verification: Verification) => {
      if (verification === 'EXACT') {
        thresholdControl.setValue(null);
        thresholdControl.disable();
      } else {
        thresholdControl.enable();
      }
    });

    form.get('field').valueChanges.subscribe((field: Field) => {
      verificationControl.setValue('EXACT');
      if (field === 'DATE_OF_BIRTH' || field === 'IDENTIFICATION_CARD') {
        verificationControl.disable();
      } else {
        verificationControl.enable();
      }
    });

    form.reset({
      field: value ? value.field : '',
      verification: value ? value.verification : '',
      threshold: value ? value.threshold : null
    });

    return form;
  }

  addValue(value?: CustomerDoubletValidationValue): void {
    const values: FormArray = this.form.get('values') as FormArray;
    values.push(this.initValue(value));
  }

  removeValue(index: number): void {
    const values: FormArray = this.form.get('values') as FormArray;
    values.removeAt(index);
  }

  get values(): AbstractControl[] {
    const values: FormArray = this.form.get('values') as FormArray;
    return values.controls;
  }
}
