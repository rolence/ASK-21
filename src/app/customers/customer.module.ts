/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {CustomerRoutingModule} from './customer-routing.module';
import {NgModule} from '@angular/core';
import {CustomerComponent} from './customer.component';
import {CustomerFormComponent} from './form/form.component';
import {CreateCustomerFormComponent} from './form/create/create.form.component';
import {CustomerDetailComponent} from './detail/customer.detail.component';
import {CustomerDetailFormComponent} from './form/detail/detail.component';
import {CustomerOfficesComponent} from './form/offices/offices.component';
import {CustomerEmployeesComponent} from './form/employees/employees.component';
import {CustomerContactFormComponent} from './form/contact/contact.component';
import {EditCustomerFormComponent} from './form/edit/edit.form.component';
import {CustomerStatusComponent} from './detail/status/status.component';
import {CustomerActivityComponent} from './detail/activity/activity.component';
import {CustomerIndexComponent} from './detail/customer.index.component';
import {CustomerExistsGuard} from './customer-exists.guard';
import {reducerProvider, reducerToken} from './store';
import {StoreModule} from '@ngrx/store';
import {CustomerNotificationEffects} from './store/effects/notification.effects';
import {CustomerRouteEffects} from './store/effects/route.effects';
import {EffectsModule} from '@ngrx/effects';
import {CustomerApiEffects} from './store/effects/service.effects';
import {CustomerCommandApiEffects} from './store/commands/effects/service.effects';
import {CustomerTasksNotificationEffects} from './store/customerTasks/effects/notification.effects';
import {CustomerTasksApiEffects} from './store/customerTasks/effects/service.effects';
import {CustomerTasksRouteEffects} from './store/customerTasks/effects/route.effects';
import {CustomerPortraitComponent} from './detail/portrait/portrait.component';
import {DatePipe} from '@angular/common';
import {TaskListComponent} from './tasks/task.list.component';
import {TasksApiEffects} from './store/tasks/effects/service.effects';
import {TasksRouteEffects} from './store/tasks/effects/route.effects';
import {TasksNotificationEffects} from './store/tasks/effects/notification.effects';
import {TaskCreateFormComponent} from './tasks/form/create.form.component';
import {TaskEditFormComponent} from './tasks/form/edit.form.component';
import {TaskFormComponent} from './tasks/form/form.component';
import {TaskExistsGuard} from './tasks/task-exists.guard';
import {TaskIndexComponent} from './tasks/task.index.component';
import {TaskDetailComponent} from './tasks/task.detail.component';
import {CustomerTaskComponent} from './detail/status/customer-task.component';
import {CustomerPayrollFormComponent} from './detail/payroll/form/form.component';
import {CustomerPayrollDetailComponent} from './detail/payroll/payroll.detail.component';
import {CreateCustomerPayrollFormComponent} from './detail/payroll/form/create.form.component';
import {PayrollExistsGuard} from './detail/payroll/payroll-exists.guard';
import {CustomerPayrollApiEffects} from './store/payroll/effects/service.effects';
import {CustomerPayrollRouteEffects} from './store/payroll/effects/route.effects';
import {CustomerPayrollNotificationEffects} from './store/payroll/effects/notification.effects';
import {CatalogExistsGuard} from './customFields/catalog-exists.guard';
import {CreateCustomerCatalogFormComponent} from './customFields/form/create.form.component';
import {CatalogDetailComponent} from './customFields/catalog.detail.component';
import {CustomerCatalogFormComponent} from './customFields/form/form.component';
import {CatalogApiEffects} from './store/catalogs/effects/service.effects';
import {CatalogRouteEffects} from './store/catalogs/effects/route.effects';
import {CatalogNotificationEffects} from './store/catalogs/effects/notification.effects';
import {EditCatalogFieldFormComponent} from './customFields/fields/form/edit.form.component';
import {FieldDetailComponent} from './customFields/fields/field.detail.component';
import {FieldIndexComponent} from './customFields/fields/field.index.component';
import {CatalogFieldFormComponent} from './customFields/fields/form/form.component';
import {FieldExistsGuard} from './customFields/fields/field-exists.guard';
import {LoanAgreementRouteEffects} from './store/loan-agreements/effects/route.effects';
import {LoanAgreementDocumentRouteEffects} from './store/loan-agreements/documents/effects/route.effects';
import {LoanAgreementNotificationEffects} from './store/loan-agreements/effects/notification.effects';
import {LoanAgreementDocumentApiEffects} from './store/loan-agreements/documents/effects/service.effects';
import {LoanAgreementApiEffects} from './store/loan-agreements/effects/service.effects';
import {DepositProductInstanceRouteEffects} from './store/deposits/effects/route.effects';
import {DepositProductInstanceNotificationEffects} from './store/deposits/effects/notification.effects';
import {DepositProductInstanceApiEffects} from './store/deposits/effects/service.effects';
import {DepositStandingOrderApiEffects} from './store/deposits/standing-orders/effects/service.effects';
import {DepositStandingOrderRouteEffects} from './store/deposits/standing-orders/effects/route.effects';
import {DepositStandingOrderNotificationEffects} from './store/deposits/standing-orders/effects/notification.effects';
import {LoanAgreementDocumentNotificationEffects} from './store/loan-agreements/documents/effects/notification.effects';
import {SharedModule} from '../shared/shared.module';
import {CustomFieldValueService} from '../shared/custom-fields/services/custom-field-value.service';
import {DisplayCustomerName} from './pipes/customer-name.pipe';
import {SettingsIndexComponent} from './settings/settings-index.component';
import {AccountNumberComponent} from './settings/accountNumber/account-number.component';
import {DoubletValidationComponent} from './settings/doubletValidation/doublet-validation.component';
import {CreateCustomerNumberFormatFormComponent} from './settings/accountNumber/form/create.form.component';
import {CustomerNumberFormatFormComponent} from './settings/accountNumber/form/form.component';
import {CustomerSettingsNotificationEffects} from './store/settings/effects/notification.effects';
import {CustomerSettingsApiEffects} from './store/settings/effects/service.effects';
import {CustomerSettingsRouteEffects} from './store/settings/effects/route.effects';
import {CustomerAccountNumberFormatExistsGuard} from './settings/accountNumber/account-number-format-exists.guard';
import {CustomerDoubletValidationFormComponent} from './settings/doubletValidation/form/form.component';
import {CreateDoubletValidationFormComponent} from './settings/doubletValidation/form/create.form.component';
import {CustomerDoubletValidationFormatExistsGuard} from './settings/doubletValidation/doublet-validation-exists.guard';
import {CustomerValidationGuard} from './customer-validation.guard';
import {CustomerValidationComponent} from './customer-validation.component';

@NgModule({
  imports: [
    CustomerRoutingModule,
    SharedModule,

    StoreModule.forFeature('customer', reducerToken),

    EffectsModule.forFeature([
      CustomerApiEffects,
      CustomerRouteEffects,
      CustomerNotificationEffects,

      TasksApiEffects,
      TasksRouteEffects,
      TasksNotificationEffects,

      CustomerTasksApiEffects,
      CustomerTasksRouteEffects,
      CustomerTasksNotificationEffects,
      CustomerCommandApiEffects,

      CustomerPayrollApiEffects,
      CustomerPayrollRouteEffects,
      CustomerPayrollNotificationEffects,

      CustomerSettingsApiEffects,
      CustomerSettingsRouteEffects,
      CustomerSettingsNotificationEffects,

      CatalogApiEffects,
      CatalogRouteEffects,
      CatalogNotificationEffects,

      DepositProductInstanceApiEffects,
      DepositProductInstanceRouteEffects,
      DepositProductInstanceNotificationEffects,

      DepositStandingOrderApiEffects,
      DepositStandingOrderRouteEffects,
      DepositStandingOrderNotificationEffects,

      LoanAgreementApiEffects,
      LoanAgreementRouteEffects,
      LoanAgreementNotificationEffects,

      LoanAgreementDocumentApiEffects,
      LoanAgreementDocumentRouteEffects,
      LoanAgreementDocumentNotificationEffects
    ])
  ],
  declarations: [
    CustomerComponent,
    CustomerValidationComponent,
    CustomerDetailFormComponent,
    CustomerContactFormComponent,
    CustomerOfficesComponent,
    CustomerEmployeesComponent,
    CustomerFormComponent,
    CreateCustomerFormComponent,
    EditCustomerFormComponent,
    CustomerIndexComponent,
    CustomerDetailComponent,
    CustomerStatusComponent,
    CustomerActivityComponent,
    CustomerPortraitComponent,
    TaskListComponent,
    TaskIndexComponent,
    TaskCreateFormComponent,
    TaskEditFormComponent,
    TaskFormComponent,
    TaskDetailComponent,
    CustomerTaskComponent,

    CustomerPayrollDetailComponent,
    CreateCustomerPayrollFormComponent,
    CustomerPayrollFormComponent,

    CatalogDetailComponent,
    CreateCustomerCatalogFormComponent,
    CustomerCatalogFormComponent,
    CatalogFieldFormComponent,
    EditCatalogFieldFormComponent,
    FieldIndexComponent,
    FieldDetailComponent,
    DisplayCustomerName,

    SettingsIndexComponent,
    AccountNumberComponent,
    CustomerNumberFormatFormComponent,
    CreateCustomerNumberFormatFormComponent,
    DoubletValidationComponent,
    CreateDoubletValidationFormComponent,
    CustomerDoubletValidationFormComponent
  ],
  providers: [
    CustomerExistsGuard,
    CustomerValidationGuard,
    TaskExistsGuard,
    PayrollExistsGuard,
    CustomerAccountNumberFormatExistsGuard,
    CustomerDoubletValidationFormatExistsGuard,
    CatalogExistsGuard,
    FieldExistsGuard,
    CustomFieldValueService,
    DatePipe,
    reducerProvider
  ]
})
export class CustomerModule {}
