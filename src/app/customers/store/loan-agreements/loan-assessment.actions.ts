/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Action} from '@ngrx/store';
import {Assessment} from '../../../core/services/loan/domain/agreement/assessment.model';
import {type} from '../../../core/store/util';

export const LOAD_ALL_ASSESSMENTS = type('[Loan Agreement] Load All Assessments');
export const LOAD_ALL_ASSESSMENTS_COMPLETE = type('[Loan Agreement] Load All Assessments Complete');

export interface LoadAllAssessmentPayload {
  customerNumber: string;
  loanAccount: string;
}

export class LoadAllAssessmentsAction implements Action {
  readonly type = LOAD_ALL_ASSESSMENTS;

  constructor(public payload: LoadAllAssessmentPayload) { }
}

export class LoadAllAssessmentsCompleteAction implements Action {
  readonly type = LOAD_ALL_ASSESSMENTS_COMPLETE;

  constructor(public payload: Assessment[]) { }
}

export type Actions
  = LoadAllAssessmentsAction
  | LoadAllAssessmentsCompleteAction;
