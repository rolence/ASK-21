/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Injectable} from '@angular/core';
import {of} from 'rxjs/observable/of';
import {Observable} from 'rxjs/Observable';
import {Actions, Effect} from '@ngrx/effects';
import {Action} from '@ngrx/store';
import * as actions from '../document.actions';
import {DeletePayload, DownloadPayload, UploadPayload} from '../document.actions';
import {CustomerLoanService} from '../../../../../core/services/loan/customer-loan.service';

@Injectable()
export class LoanAgreementDocumentApiEffects {

  @Effect()
  search$: Observable<Action> = this.actions$
    .ofType(actions.SEARCH)
    .debounceTime(300)
    .map((action: actions.SearchAction) => action.payload)
    .switchMap((payload) => {
      const nextSearch$ = this.actions$.ofType(actions.SEARCH).skip(1);

      return this.customerLoanService.fetchDocuments(payload.customerNumber, payload.loanAccount)
        .takeUntil(nextSearch$)
        .map(documents => new actions.SearchCompleteAction({
          elements: documents,
          totalElements: documents.length,
          totalPages: 1
        }))
        .catch(() => of(new actions.SearchCompleteAction({
          elements: [],
          totalElements: 0,
          totalPages: 0
        })));
    });

  @Effect()
  uploadDocument$: Observable<Action> = this.actions$
    .ofType(actions.CREATE)
    .map((action: actions.CreateAction) => action.payload)
    .mergeMap((payload: UploadPayload) =>
      this.customerLoanService.attachDocuments(payload.customerNumber, payload.loanAccount, payload.documents)
        .map(() => new actions.CreateSuccessAction({
          resource: payload.documents[0],
          activatedRoute: payload.activatedRoute
        }))
        .catch((error) => of(new actions.CreateFailAction(error)))
    );

  @Effect({ dispatch: false })
  downloadDocument$: Observable<Action> = this.actions$
    .ofType(actions.DOWNLOAD)
    .map((action: actions.DownloadAction) => action.payload)
    .mergeMap((payload: DownloadPayload) =>
      this.customerLoanService.downloadDocument(payload.customerNumber, payload.loanAccount, payload.fileName)
        .map(() => new actions.DownloadSuccessAction(payload))
        .catch((error) => of(new actions.DownloadFailAction(error)))
    );

  @Effect()
  deleteDocument$: Observable<Action> = this.actions$
    .ofType(actions.DELETE)
    .map((action: actions.DeleteAction) => action.payload)
    .mergeMap((payload: DeletePayload) =>
      this.customerLoanService.deleteDocument(payload.customerNumber, payload.loanAccount, payload.document.fileName)
        .map(() => new actions.DeleteSuccessAction({
          resource: payload.document,
          activatedRoute: payload.activatedRoute
        }))
        .catch((error) => of(new actions.DeleteFailAction(error)))
    );

  constructor(private actions$: Actions, private customerLoanService: CustomerLoanService) { }
}
