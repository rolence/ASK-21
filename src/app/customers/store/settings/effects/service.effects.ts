/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Injectable} from '@angular/core';
import {Action} from '@ngrx/store';
import * as accountNumberActions from '../account-number.actions';
import {AccountNumberActionTypes} from '../account-number.actions';
import * as doubletValidationActions from '../doublet-validation.actions';
import {DoubletValidationActionTypes} from '../doublet-validation.actions';
import {Observable} from 'rxjs/Observable';
import {Actions, Effect} from '@ngrx/effects';
import {of} from 'rxjs/observable/of';
import {SettingsService} from '../../../../core/services/customer/settings.service';

@Injectable()
export class CustomerSettingsApiEffects {

  @Effect()
  updateCustomerNumberFormat$: Observable<Action> = this.actions$
    .ofType(AccountNumberActionTypes.UPDATE)
    .map((action: accountNumberActions.UpdateAccountNumberAction) => action.payload)
    .mergeMap(payload =>
      this.settingsService.setCustomerNumberFormat(payload.customerNumberFormat)
        .map(() => new accountNumberActions.UpdateAccountNumberSuccessAction(payload))
        .catch((error) => of(new accountNumberActions.UpdateAccountNumberFailAction(error)))
    );

  @Effect()
  updateCustomerDoubletValidation$: Observable<Action> = this.actions$
    .ofType(DoubletValidationActionTypes.UPDATE)
    .map((action: doubletValidationActions.UpdateDoubletValidationAction) => action.payload)
    .mergeMap(payload =>
      this.settingsService.setCustomerDoubletValidation(payload.doubletValidation)
        .map(() => new doubletValidationActions.UpdateDoubletValidationSuccessAction(payload))
        .catch((error) => of(new doubletValidationActions.UpdateDoubletValidationFailAction(error)))
    );

  constructor(private actions$: Actions, private settingsService: SettingsService) { }

}
