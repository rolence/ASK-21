/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import * as accountNumberActions from './account-number.actions';
import {AccountNumberActionTypes} from './account-number.actions';
import {CustomerNumberFormat} from '../../../core/services/customer/domain/customer-number-format.model';

export interface State {
  customerNumberFormat: CustomerNumberFormat;
  loadedAt: number;
}

const initialState: State = {
  customerNumberFormat: null,
  loadedAt: null
};

export function reducer(state: State = initialState, action: accountNumberActions.AccountNumberActions): State {

  switch (action.type) {

    case AccountNumberActionTypes.LOAD: {
      const customerNumberFormat: CustomerNumberFormat = action.payload.customerNumberFormat;

      return {
        customerNumberFormat,
        loadedAt: Date.now()
      };
    }

    case AccountNumberActionTypes.UPDATE_SUCCESS: {
      const customerNumberFormat: CustomerNumberFormat = action.payload.customerNumberFormat;

      return {
        customerNumberFormat,
        loadedAt: state.loadedAt
      };
    }

    default: {
      return state;
    }
  }
}

export const getCustomerNumberFormat = (state: State) => state.customerNumberFormat;
export const getCustomerNumberFormatLoadedAt = (state: State) => state.loadedAt;
