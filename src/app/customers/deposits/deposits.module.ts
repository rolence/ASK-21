/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {NgModule} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {DepositCreateComponent} from './form/create/create.component';
import {DepositCreateFormComponent} from './form/create/form.component';
import {DepositsListComponent} from './deposits.list.component';
import {DepositsRoutingModule} from './deposits-routing.module';
import {DepositIndexComponent} from './detail/deposit.index.component';
import {DepositDetailComponent} from './detail/deposit.detail.component';
import {DepositInstanceExistsGuard} from './deposit-instance-exists.guard';
import {DepositEditComponent} from './form/edit/edit.component';
import {IssueChequesFormComponent} from './detail/cheques/form.component';
import {IssueChequeComponent} from './detail/cheques/cheques.component';
import {CustomerDepositInstallmentFormComponent} from './form/installment/installment-form.component';
import {CustomerDepositTermsFormComponent} from './form/terms/terms-form.component';
import {CustomerDepositInstallmentDetailComponent} from './detail/components/installment/installment-detail.component';
import {CustomerDepositAdministrationFeeDetailComponent} from './detail/components/administration-fee/administration-fee-detail.component';
import {CustomerDepositTermDetailComponent} from './detail/components/term/term-detail.component';
import {CustomerDepositExternalAccountFormComponent} from './form/externalAccount/external-account-form.component';
import {StandingOrdersListComponent} from './detail/standing-orders/standing-orders.component';
import {StandingOrdersIndexComponent} from './detail/standing-orders/standing-orders.index.component';
import {CreateStandingOrderComponent} from './detail/standing-orders/form/create.component';
import {EditStandingOrderComponent} from './detail/standing-orders/form/edit.component';
import {StandingOrderFormComponent} from './detail/standing-orders/form/form.component';
import {StandingOrderDetailComponent} from './detail/standing-orders/detail/detail.component';
import {StandingOrderExistsGuard} from './detail/standing-orders/standing-order-exists.guard';
import {StandingOrderIndexComponent} from './detail/standing-orders/standing-order.index.component';
import {DepositEditFormComponent} from './form/edit/form.component';
import {CustomerDepositBeneficiariesFormComponent} from './form/beneficiaries/beneficiaries-form.component';
import {DepositFormService} from './form/service/deposit-form.service';
import {SharedModule} from '../../shared/shared.module';
import {AccountingValidationService} from '../../core/validator/services/accounting-validation.service';

@NgModule({
  imports: [
    DepositsRoutingModule,
    SharedModule,
    TranslateModule
  ],
  declarations: [
    DepositsListComponent,
    DepositCreateFormComponent,
    DepositIndexComponent,
    DepositCreateComponent,
    DepositEditComponent,
    DepositEditFormComponent,
    DepositDetailComponent,
    IssueChequeComponent,
    IssueChequesFormComponent,
    CustomerDepositTermsFormComponent,
    CustomerDepositInstallmentFormComponent,
    CustomerDepositAdministrationFeeDetailComponent,
    CustomerDepositInstallmentDetailComponent,
    CustomerDepositTermDetailComponent,
    CustomerDepositExternalAccountFormComponent,
    CustomerDepositBeneficiariesFormComponent,
    StandingOrdersIndexComponent,
    StandingOrdersListComponent,
    CreateStandingOrderComponent,
    StandingOrderIndexComponent,
    EditStandingOrderComponent,
    StandingOrderFormComponent,
    StandingOrderDetailComponent
  ],
  providers: [
    DepositInstanceExistsGuard,
    StandingOrderExistsGuard,
    DepositFormService,
    AccountingValidationService
  ]
})
export class DepositsModule {}
