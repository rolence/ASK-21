/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {Component} from '@angular/core';
import * as fromCustomers from '../../../store';
import {Observable} from 'rxjs/Observable';
import {ProductInstance} from '../../../../core/services/depositAccount/domain/instance/product-instance.model';
import {IssuingCount} from '../../../../core/services/cheque/domain/issuing-count.model';
import {ISSUE_CHEQUES} from '../../../store/deposits/deposit.actions';
import {ActivatedRoute, Router} from '@angular/router';
import {Store} from '@ngrx/store';

@Component({
  templateUrl: './cheques.component.html'
})
export class IssueChequeComponent {

  depositInstance$: Observable<ProductInstance>;

  constructor(private store: Store<fromCustomers.State>, private router: Router, private route: ActivatedRoute) {
    this.depositInstance$ = this.store.select(fromCustomers.getSelectedDepositInstance);
  }

  issueCheques(issuingCount: IssuingCount): void {
    this.store.dispatch({
      type: ISSUE_CHEQUES,
      payload: {
        issuingCount,
        activatedRoute: this.route
      }
    });
  }

  cancel(): void {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

}
