/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, OnInit} from '@angular/core';
import {Customer} from '../../../../core/services/customer/domain/customer.model';
import {ProductInstance} from '../../../../core/services/depositAccount/domain/instance/product-instance.model';
import {ActivatedRoute, Router} from '@angular/router';
import * as fromCustomers from '../../../store/index';
import {Observable} from 'rxjs/Observable';
import {UpdateProductInstanceAction} from '../../../store/deposits/deposit.actions';
import {ProductInstanceOutline} from '../../../../core/services/depositAccount/domain/instance/product-instance-outline.model';
import {Store} from '@ngrx/store';
import {ProductInstanceChangeSet} from '../../../../core/services/depositAccount/domain/instance/product-instance-change-set.model';

@Component({
  templateUrl: './edit.component.html'
})
export class DepositEditComponent implements OnInit {

  productInstances$: Observable<ProductInstanceOutline[]>;
  customer$: Observable<Customer>;
  productInstance$: Observable<ProductInstance>;

  constructor(private router: Router, private route: ActivatedRoute,
              private store: Store<fromCustomers.State>) {}

  ngOnInit(): void {
    this.customer$ = this.store.select(fromCustomers.getSelectedCustomer);
    this.productInstance$ = this.store.select(fromCustomers.getSelectedDepositInstance);
    this.productInstances$ = this.store.select(fromCustomers.getActiveSearchDeposits);
  }

  onSave(productChangeSet: ProductInstanceChangeSet): void {
      this.productInstance$
        .take(1)
        .map(productInstance => new UpdateProductInstanceAction({
          accountIdentifier: productInstance.accountIdentifier,
          productInstance,
          productChangeSet,
          activatedRoute: this.route
        }))
        .subscribe(this.store);
  }

  onCancel(): void {
    this.navigateAway();
  }

  navigateAway(): void {
    this.router.navigate(['../'], { relativeTo: this.route });
  }
}
