/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {DepositsListComponent} from './deposits.list.component';
import {RouterModule, Routes} from '@angular/router';
import {DepositCreateComponent} from './form/create/create.component';
import {DepositIndexComponent} from './detail/deposit.index.component';
import {DepositDetailComponent} from './detail/deposit.detail.component';
import {DepositInstanceExistsGuard} from './deposit-instance-exists.guard';
import {DepositEditComponent} from './form/edit/edit.component';
import {IssueChequeComponent} from './detail/cheques/cheques.component';
import {StandingOrdersIndexComponent} from './detail/standing-orders/standing-orders.index.component';
import {StandingOrdersListComponent} from './detail/standing-orders/standing-orders.component';
import {CreateStandingOrderComponent} from './detail/standing-orders/form/create.component';
import {StandingOrderIndexComponent} from './detail/standing-orders/standing-order.index.component';
import {StandingOrderDetailComponent} from './detail/standing-orders/detail/detail.component';
import {EditStandingOrderComponent} from './detail/standing-orders/form/edit.component';
import {StandingOrderExistsGuard} from './detail/standing-orders/standing-order-exists.guard';
import {NgModule} from '@angular/core';
import {IndexComponent} from '../../shared/util/index.component';

const DepositRoutes: Routes = [
  {
    path: '',
    component: IndexComponent,
    data: {
      title: 'All deposit accounts',
      hasPermission: {id: 'deposit_instances', accessLevel: 'READ'}
    },
    children: [
      {
        path: '',
        component: DepositsListComponent,
      },
      {
        path: 'create',
        component: DepositCreateComponent,
        data: {
          title: 'Add deposit account',
          hasPermission: {id: 'deposit_instances', accessLevel: 'CHANGE'}
        }
      },
      {
        path: 'detail/:id',
        component: DepositIndexComponent,
        canActivate: [DepositInstanceExistsGuard],
        data: {
          hasPermission: {id: 'deposit_instances', accessLevel: 'READ'}
        },
        children: [
          {
            path: '',
            component: DepositDetailComponent
          },
          {
            path: 'edit',
            component: DepositEditComponent,
            data: {
              title: 'Edit',
              hasPermission: {id: 'deposit_instances', accessLevel: 'CHANGE'}
            }
          },
          {
            path: 'cheques',
            component: IssueChequeComponent,
            data: {
              title: 'Issue cheque',
              hasPermission: {id: 'cheque_management', accessLevel: 'CHANGE'}
            }
          },
          {
            path: 'standingOrders',
            component: StandingOrdersIndexComponent,
            data: {
              title: 'All standing orders'
            },
            children: [
              {
                path: '',
                component: StandingOrdersListComponent,
              },
              {
                path: 'create',
                component: CreateStandingOrderComponent,
                data: {
                  title: 'Add standing order'
                }
              },
              {
                path: 'detail/:sequence',
                component: StandingOrderIndexComponent,
                canActivate: [StandingOrderExistsGuard],
                children: [
                  {
                    path: '',
                    component: StandingOrderDetailComponent,
                  },
                  {
                    path: 'edit',
                    component: EditStandingOrderComponent,
                    data: {
                      title: 'Edit'
                    }
                  }
                ]
              }
            ]
          }
        ]
      },
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(DepositRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class DepositsRoutingModule {}
