/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Customer} from '../../core/services/customer/domain/customer.model';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductInstance} from '../../core/services/depositAccount/domain/instance/product-instance.model';
import * as fromCustomers from '../store';
import {DatePipe} from '@angular/common';
import {Store} from '@ngrx/store';
import {TableData} from '../../shared/data-table/data-table.component';

@Component({
  templateUrl: './deposits.list.component.html',
  providers: [DatePipe]
})
export class DepositsListComponent {

  customer$: Observable<Customer>;
  productInstancesData$: Observable<TableData>;

  columns: any[] = [
    { name: 'productShortCode', label: 'Deposit product' },
    { name: 'accountIdentifier', label: 'Account identifier' },
    { name: 'balance', label: 'Balance', numeric: true, format: v => v.toFixed(2) },
    { name: 'state', label: 'State' },
    {
      name: 'lastTransactionDate', label: 'Last transaction', format: (v: any) => {
        return this.datePipe.transform(v, 'short');
      }
    }
  ];

  constructor(private router: Router, private route: ActivatedRoute,
              private store: Store<fromCustomers.State>, private datePipe: DatePipe) {
    this.productInstancesData$ = this.store.select(fromCustomers.getSearchDeposits)
      .map(depositOutlines => ({
        totalElements: depositOutlines.length,
        totalPages: 1,
        data: depositOutlines
      }));

    this.customer$ = this.store.select(fromCustomers.getSelectedCustomer);
  }

  rowSelect(productInstance: ProductInstance): void {
    this.router.navigate(['detail', productInstance.accountIdentifier], { relativeTo: this.route });
  }

  addDepositAccount(): void {
    this.router.navigate(['create'], { relativeTo: this.route });
  }
}
