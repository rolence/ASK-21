/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Observable} from 'rxjs/Observable';
import {Component} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import * as fromCustomers from '../store';
import {Store} from '@ngrx/store';
import {TableData} from '../../shared/data-table/data-table.component';
import {Customer} from '../../core/services/customer/domain/customer.model';
import {LoanAgreementOutline} from '../../core/services/loan/domain/agreement/loan-agreement-outline.model';

@Component({
  templateUrl: './loan-agreement.list.component.html'
})
export class LoanAgreementListComponent {

  agreementData$: Observable<TableData>;
  canCreateAgreements$: Observable<boolean>;
  customer$: Observable<Customer>;

  constructor(private router: Router, private route: ActivatedRoute, private store: Store<fromCustomers.State>) {
    this.agreementData$ = this.store.select(fromCustomers.getSearchAgreements)
      .map(outlines => ({
        data: outlines,
        totalElements: outlines.length,
        totalPages: 1
      }));
    this.canCreateAgreements$ = this.store.select(fromCustomers.canCreateAgreements);
    this.customer$ = this.store.select(fromCustomers.getSelectedCustomer);
  }

  rowSelect(outline: LoanAgreementOutline): void {
    this.router.navigate(['detail', outline.loanAccount], { relativeTo: this.route });
  }

  createAgreement(): void {
    this.router.navigate(['create'], { relativeTo: this.route });
  }

}
