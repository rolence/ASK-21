/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {RouterModule, Routes} from '@angular/router';
import {LoanAgreementListComponent} from './loan-agreement.list.component';
import {LoanAgreementDetailComponent} from './loan-agreement.detail.component';
import {LoanAgreementExistsGuard} from './loan-agreement-exists.guard';
import {LoanAgreementIndexComponent} from './loan-agreement.index.component';
import {LoanAgreementCreateComponent} from './form/create.component';
import {LoanAgreementDocumentListComponent} from './documents/document.list.component';
import {CreateLoanAgreementDocumentComponent} from './documents/form/create.component';
import {LoanAgreementEditComponent} from './form/edit.component';
import {LoanAgreementRestructuringListComponent} from './restructurings/restructuring.list.component';
import {LoanAgreementRestructuringDetailComponent} from './restructurings/restructuring.detail.component';
import {NgModule} from '@angular/core';
import {IndexComponent} from '../../shared/util/index.component';

const LoanAgreementRoutes: Routes = [
  {
    path: '',
    component: IndexComponent,
    data: {
      title: 'All loan agreements',
      hasPermission: { id: 'loan_agreement', accessLevel: 'READ' }
    },
    children: [
      {
        path: '',
        component: LoanAgreementListComponent,
      },
      {
        path: 'create',
        component: LoanAgreementCreateComponent,
        data: {
          title: 'Add agreement',
          hasPermission: { id: 'loan_agreement', accessLevel: 'CHANGE' }
        }
      },
      {
        path: 'detail/:id',
        component: LoanAgreementIndexComponent,
        canActivate: [LoanAgreementExistsGuard],
        data: {
          hasPermission: { id: 'loan_agreement', accessLevel: 'READ' }
        },
        children: [
          {
            path: '',
            component: LoanAgreementDetailComponent
          },
          {
            path: 'edit',
            component: LoanAgreementEditComponent,
            data: {
              title: 'Edit'
            }
          },
          {
            path: 'documents',
            component: IndexComponent,
            data: {
              title: 'All documents'
            },
            children: [
              {
                path: '',
                component: LoanAgreementDocumentListComponent,
              },
              {
                path: 'upload',
                component: CreateLoanAgreementDocumentComponent,
                data: {
                  title: 'Upload document',
                  hasPermission: { id: 'loan_agreement', accessLevel: 'CHANGE' }
                }
              }
            ]
          },
          {
            path: 'restructurings',
            component: IndexComponent,
            data: {
              title: 'Past restructurings'
            },
            children: [
              {
                path: '',
                component: LoanAgreementRestructuringListComponent
              },
              {
                path: 'detail/:sequence',
                component: LoanAgreementRestructuringDetailComponent,
                data: {
                  title: 'Restructuring'
                }
              }
            ]
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(LoanAgreementRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class LoanAgreementRoutingModule {}
