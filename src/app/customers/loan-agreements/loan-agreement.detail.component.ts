/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Component} from '@angular/core';
import {LoanAgreement} from '../../core/services/loan/domain/agreement/loan-agreement.model';
import {Observable} from 'rxjs/Observable';
import * as fromCustomers from '../store';
import * as fromRoot from '../../core/store';
import {MatDialog} from '@angular/material';
import {ConfirmationDialogData, LoanAgreementConfirmationDialogComponent} from './components/confirmation/confirmation-dialog.component';
import {Customer} from '../../core/services/customer/domain/customer.model';
import {paymentPeriodOptionsList} from './form/model/payment-period-options.model';
import {FimsPermission} from '../../core/services/security/authz/fims-permission.model';
import {ActivatedRoute, Router} from '@angular/router';
import {DeleteLoanAgreementAction} from '../store/loan-agreements/loan-agreement.actions';
import {DialogService} from '../../core/services/dialog/dialog.service';
import {Store} from '@ngrx/store';

@Component({
  templateUrl: './loan-agreement.detail.component.html'
})
export class LoanAgreementDetailComponent {

  paymentPeriodOptions = paymentPeriodOptionsList;
  customer$: Observable<Customer>;
  loanAgreement$: Observable<LoanAgreement>;
  permissions$: Observable<FimsPermission[]>;
  canDeleteAgreement$: Observable<boolean>;
  canChangeAgreement$: Observable<boolean>;

  constructor(private router: Router, private route: ActivatedRoute, private store: Store<fromCustomers.State>,
              public dialogService: DialogService, public dialog: MatDialog) {
    this.loanAgreement$ = store.select(fromCustomers.getSelectedAgreement);
    this.permissions$ = store.select(fromRoot.getPermissions);
    this.canDeleteAgreement$ = store.select(fromCustomers.canDeleteAgreement);
    this.canChangeAgreement$ = store.select(fromCustomers.canChangeAgreement);
    this.customer$ = store.select(fromCustomers.getSelectedCustomer);
  }

  underwrite(loanAgreement: LoanAgreement): void {
    this.customer$
      .take(1)
      .map((customer: Customer) => ({
        customerNumber: customer.identifier,
        loanAgreement,
        command: 'UNDERWRITE'
      }))
      .switchMap((data: ConfirmationDialogData) => this.openDialog(data)).subscribe();
  }

  approve(loanAgreement: LoanAgreement): void {
    this.customer$
      .take(1)
      .map((customer: Customer) => ({
        customerNumber: customer.identifier,
        loanAgreement,
        command: 'APPROVE'
      }))
      .switchMap((data: ConfirmationDialogData) => this.openDialog(data)).subscribe();
  }

  decline(loanAgreement: LoanAgreement): void {
    this.customer$
      .take(1)
      .map((customer: Customer) => ({
        customerNumber: customer.identifier,
        loanAgreement,
        command: 'DECLINE'
      }))
      .switchMap((data: ConfirmationDialogData) => this.openDialog(data)).subscribe();
  }

  return(loanAgreement: LoanAgreement): void {
    this.customer$
      .take(1)
      .map((customer: Customer) => ({
        customerNumber: customer.identifier,
        loanAgreement,
        command: 'RETURN'
      }))
      .switchMap((data: ConfirmationDialogData) => this.openDialog(data)).subscribe();
  }

  restructure(loanAgreement: LoanAgreement): void {
    this.customer$
      .take(1)
      .map((customer: Customer) => ({
        customerNumber: customer.identifier,
        loanAgreement,
        command: 'RESTRUCTURE'
      }))
      .switchMap((data: ConfirmationDialogData) => this.openDialog(data)).subscribe();
  }

  private openDialog(data: ConfirmationDialogData): Observable<any> {
    return this.dialog.open(LoanAgreementConfirmationDialogComponent, {
      data,
      width: '40%',
      height: '40%'
    }).afterClosed();
  }

  confirmDeletion(): Observable<boolean> {
    return this.dialogService.openConfirm({
      message: 'Do you want to delete this agreement?',
      title: 'Confirm deletion',
      acceptButton: 'DELETE AGREEMENT',
    });
  }

  delete(agreement: LoanAgreement): void {
    this.confirmDeletion()
      .filter(accept => accept)
      .mergeMap(() => this.customer$.take(1))
      .map(customer => new DeleteLoanAgreementAction({
        customerNumber: customer.identifier,
        agreement,
        activatedRoute: this.route
      }))
      .subscribe(this.store);
  }

  editAgreement(): void {
    this.router.navigate(['edit'], { relativeTo: this.route });
  }

  goToDocuments(): void {
    this.router.navigate(['documents'], { relativeTo: this.route });
  }

  goToRestructurings(): void {
    this.router.navigate(['restructurings'], { relativeTo: this.route });
  }

}
