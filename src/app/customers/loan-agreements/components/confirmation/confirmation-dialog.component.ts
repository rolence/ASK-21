/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Component, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSelectionList} from '@angular/material';
import {LoanAgreement} from '../../../../core/services/loan/domain/agreement/loan-agreement.model';
import {CustomerLoanService} from '../../../../core/services/loan/customer-loan.service';
import {Assessment} from '../../../../core/services/loan/domain/agreement/assessment.model';
import {Observable} from 'rxjs/Observable';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {
  ActionPayload,
  ApproveLoanAgreementAction,
  DeclineLoanAgreementAction,
  RestructureLoanAgreementAction,
  ReturnLoanAgreementAction,
  UnderwriteLoanAgreementAction
} from '../../../store/loan-agreements/loan-agreement.actions';
import * as fromCustomers from '../../../store';
import {Store} from '@ngrx/store';

export type ConfirmationCommand = 'UNDERWRITE' | 'APPROVE' | 'DECLINE' | 'RETURN' | 'RESTRUCTURE';

export interface ConfirmationDialogData {
  command: ConfirmationCommand;
  customerNumber: string;
  loanAgreement: LoanAgreement;
}

@Component({
  templateUrl: './confirmation-dialog.component.html'
})
export class LoanAgreementConfirmationDialogComponent {

  @ViewChild('assessmentSelection') assessmentSelection: MatSelectionList;

  formGroup: FormGroup;

  pendingAssessments$: Observable<Assessment[]>;

  constructor(@Inject(MAT_DIALOG_DATA) public data: ConfirmationDialogData,
              public dialogRef: MatDialogRef<LoanAgreementConfirmationDialogComponent>,
              private loanService: CustomerLoanService,
              private store: Store<fromCustomers.State>,
              private formBuilder: FormBuilder) {

    this.formGroup = formBuilder.group({
      note: ['', [Validators.required, Validators.maxLength(256)]],
      guarantorNeeded: [false, [Validators.required]]
    });

    if (data.loanAgreement.hasPendingAssessments) {
      const noteControl = this.formGroup.get('note');
      noteControl.disable();
    }

    if (data.command !== 'RETURN') {
      const guarantorControl = this.formGroup.get('guarantorNeeded');
      guarantorControl.disable();
    }

    this.pendingAssessments$ = loanService.fetchAssessments(data.customerNumber, data.loanAgreement.loanAccount, true);
  }

  underwrite(): void {
    this.pendingAssessments$
      .take(1)
      .map((assessments: Assessment[]) => new UnderwriteLoanAgreementAction({
        customerNumber: this.data.customerNumber,
        loanAccount: this.data.loanAgreement.loanAccount,
        note: this.formGroup ? this.formGroup.get('note').value : undefined,
        assessments
      }))
      .do(() => this.dialogRef.close('UNDERWRITE'))
      .subscribe(this.store);
  }

  approve(): void {
    this.store.dispatch(new ApproveLoanAgreementAction(this.buildPayload()));
    this.dialogRef.close('APPROVE');
  }

  decline(): void {
    this.store.dispatch(new DeclineLoanAgreementAction(this.buildPayload()));
    this.dialogRef.close('DECLINE');
  }

  return(): void {
    this.store.dispatch(new ReturnLoanAgreementAction({
      customerNumber: this.data.customerNumber,
      loanAccount: this.data.loanAgreement.loanAccount,
      note: this.formGroup.get('note').value,
      guarantorNeeded: this.formGroup.get('guarantorNeeded').value
    }));
    this.dialogRef.close('RETURN');
  }

  restructure(): void {
    this.store.dispatch(new RestructureLoanAgreementAction({
      customerNumber: this.data.customerNumber,
      loanAccount: this.data.loanAgreement.loanAccount,
      note: this.formGroup.get('note').value
    }));
    this.dialogRef.close('RESTRUCTURE');
  }

  private buildPayload(): ActionPayload {
    return {
      customerNumber: this.data.customerNumber,
      loanAccount: this.data.loanAgreement.loanAccount,
      note: this.formGroup.get('note').value,
    };
  }

  get isValid(): boolean {
    if (this.formGroup && this.assessmentSelection) {
      return !this.formGroup.invalid
        && this.assessmentSelection.selectedOptions.selected.length === this.assessmentSelection.options.length;
    }

    return true;
  }

}
