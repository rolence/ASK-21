/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Component, EventEmitter, Input, Output} from '@angular/core';
import {AbstractControl, FormArray, FormGroup} from '@angular/forms';

@Component({
  selector: 'aten-debt-to-income-entry-form',
  templateUrl: './debt-to-income-entry.component.html'
})
export class DebtToIncomeEntryFormComponent {

  @Input() factorName: string;
  @Input() form: FormGroup;
  @Input() arrayName: string;

  @Output() onAddEntry = new EventEmitter<void>();
  @Output() onRemoveEntry = new EventEmitter<number>();

  addEntry(): void {
    this.onAddEntry.emit();
  }

  removeEntry(index: number): void {
    this.onRemoveEntry.emit(index);
  }

  get entries(): AbstractControl[] {
    const formArray = this.form.get(this.arrayName) as FormArray;
    return formArray.controls;
  }

}
