/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Component, EventEmitter, Input, Output} from '@angular/core';
import {LoanAgreement} from '../../../../core/services/loan/domain/agreement/loan-agreement.model';
import {FimsPermission} from '../../../../core/services/security/authz/fims-permission.model';

@Component({
  selector: 'aten-loan-agreement-state-message',
  templateUrl: './state-message.component.html'
})
export class LoanAgreementStateMessageComponent {

  @Input() agreement: LoanAgreement;
  @Input() permissions: FimsPermission[];

  @Output() onUnderwrite = new EventEmitter();
  @Output() onApprove = new EventEmitter();
  @Output() onDecline = new EventEmitter();
  @Output() onReturn = new EventEmitter();
  @Output() onRestructure = new EventEmitter();

  underwrite(): void {
    this.onUnderwrite.emit();
  }

  approve(): void {
    this.onApprove.emit();
  }

  decline(): void {
    this.onDecline.emit();
  }

  return(): void {
    this.onReturn.emit();
  }

  restructure(): void {
    this.onRestructure.emit();
  }

  get canApprove(): boolean {
    const hasPermission = !!this.permissions.find(
      permission => permission.id === 'loan_approve_agreement' && permission.accessLevel === 'CHANGE'
    );
    return hasPermission && this.agreement.currentUserCanApprove;
  }
}
