/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {LoanProductOutline} from '../../../core/services/loan/domain/definition/product-outline.model';
import {LoanAgreement} from '../../../core/services/loan/domain/agreement/loan-agreement.model';
import {PrincipalRange} from '../../../core/services/loan/domain/definition/principal-range.model';
import {Interest} from '../../../core/services/loan/domain/definition/interest.model';
import {Duration} from '../../../core/services/loan/domain/definition/duration.model';
import {AmortizationSchedule} from '../../../core/services/loan/domain/finance/amortization-schedule.model';
import {CalculateScheduleData, LoanAgreementScheduleDialogComponent} from './schedule/schedule-dialog.component';
import {MatDialog} from '@angular/material';
import {paymentPeriodOptionsList} from './model/payment-period-options.model';
import {defaultDisbursementMethodOptions} from './model/disbursement-method-options.model';
import {DebtToIncomeFormService} from './services/deb-to-income.service';
import {ChargesFormService} from './services/charges.service';
import {AttachedCollateral} from '../../../core/services/loan/domain/agreement/attached-collateral.model';
import {ProductInstanceOutline} from '../../../core/services/depositAccount/domain/instance/product-instance-outline.model';
import {Field} from '../../../core/services/catalog/domain/field.model';
import {DisplayFimsNumber} from '../../../shared/number/fims-number.pipe';
import {CustomFieldValueService} from '../../../shared/custom-fields/services/custom-field-value.service';
import {FimsValidators} from '../../../core/validator/validators';
import {dateAsISOString, parseDate, todayAsDateWithOffset, toShortISOString} from '../../../core/services/domain/date.converter';
import {BankInfo} from '../../../core/services/accounting/domain/bank-info.model';

export interface LoanAgreementFormData {
  loanAgreement?: LoanAgreement;
  loanProducts: LoanProductOutline[];
  productInstances: ProductInstanceOutline[];
  bankInfos: BankInfo[];
  editMode?: boolean;
}

@Component({
  selector: 'aten-loan-agreement-form',
  templateUrl: './form.component.html',
  providers: [DisplayFimsNumber]
})
export class LoanAgreementFormComponent implements OnChanges {

  paymentPeriodOptions = paymentPeriodOptionsList;
  disbursementOptions = defaultDisbursementMethodOptions;
  selectedProduct: LoanProductOutline;
  formGroup: FormGroup;
  termFormGroup: FormGroup;
  debtToIncomeFormGroup: FormGroup;
  chargesFormGroup: FormGroup;
  collateralFormGroup: FormGroup;
  fields: Field[] = [];
  locked: boolean;
  amountToSecure: string;

  @Input() formData: LoanAgreementFormData;

  @Output() save = new EventEmitter<LoanAgreement>();
  @Output() cancel = new EventEmitter<void>();

  constructor(private formBuilder: FormBuilder, private displayFimsNumber: DisplayFimsNumber,
              public dialog: MatDialog, public debtToIncomeService: DebtToIncomeFormService,
              private chargesFormService: ChargesFormService, private customFieldValueService: CustomFieldValueService) {
    this.formGroup = this.formBuilder.group({
      productShortName: ['', [Validators.required]],
      automaticPaymentAccount: ['', []],
      purpose: [],
      disbursementMethod: [],
      calculateValue: [],
      bankInfoEnabled: [false],
      branchSortCode: ['', [Validators.required]]
    });

    this.termFormGroup = this.formBuilder.group({
      length: [null, [Validators.required, FimsValidators.isNumber]],
      principal: [null, [Validators.required]],
      interestRate: [null, [Validators.required]],
      payment: [null, [Validators.required]],
      period: ['MONTH', [Validators.required]],
      interestPaymentFirstDueDate: [todayAsDateWithOffset(1), [Validators.required, FimsValidators.afterToday]]
    });

    this.debtToIncomeFormGroup = this.debtToIncomeService.initDebtToIncomeForm();
    this.chargesFormGroup = this.chargesFormService.initChargesForm();

    this.formGroup.get('calculateValue').valueChanges.subscribe(value => {
      if (this.isRestructure) {
        this.enable(this.principalControl);
        this.disable(this.paymentControl);

        return;
      }

      if (this.selectedProduct && this.selectedProduct.term.principalRange.min) {
        if (value === 'principal') {
          this.enable(this.principalControl);
          this.disable(this.paymentControl);
        } else {
          this.enable(this.paymentControl);
          this.disable(this.principalControl);
        }

        return;
      }

      this.disable(this.principalControl);
      this.disable(this.paymentControl);
    });

    this.formGroup.get('productShortName').valueChanges
      .filter(productShortName => !!productShortName)
      .map(productShortName => this.formData.loanProducts.find(product => product.shortName === productShortName))
      .subscribe(outline => this.toggleLoanProduct(outline));

    this.formGroup.get('bankInfoEnabled').valueChanges
      .startWith(false)
      .subscribe(enabled => this.toggleBankInfoEnabled(enabled));
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.formData && this.formData) {
      if (this.formData.editMode) {
        this.formGroup.get('productShortName').disable();

        const loanAgreement = this.formData.loanAgreement;

        if (loanAgreement) {
          // This will trigger toggleLoanProduct
          this.formGroup.reset({
            productShortName: loanAgreement.productShortName,
            automaticPaymentAccount: loanAgreement.term.automaticPaymentAccount,
            purpose: loanAgreement.purpose,
            disbursementMethod: loanAgreement.disbursementMethod,
            calculateValue: 'principal',
            bankInfoEnabled: false
          });

          if (loanAgreement.attachedCollateral) {
            if (loanAgreement.attachedCollateral.type === 'DEPOSIT_SECURED') {
              this.collateralFormGroup.reset({
                attachedResource: loanAgreement.attachedCollateral.attachedResource,
                securedAmount: loanAgreement.attachedCollateral.securedAmount
              });
            } else {
              this.customFieldValueService.setValues(loanAgreement.attachedCollateral.values);
            }
          }

          const term = loanAgreement.term;
          const interestPaymentFirstDueDate = dateAsISOString(parseDate(term.payment.firstPaymentDate));

          const scale = this.selectedProduct.currency.scale;

          this.termFormGroup.reset({
            length: term.length,
            principal: parseFloat(term.principal).toFixed(scale),
            interestRate: parseFloat(term.interestRate).toFixed(scale),
            payment: parseFloat(term.payment.amount).toFixed(scale),
            period: term.payment.period,
            interestPaymentFirstDueDate
          });

          if (loanAgreement.debtToIncome) {
            this.debtToIncomeService.setDebtToIncome(loanAgreement.debtToIncome);
          }

          this.chargesFormService.setCharges(loanAgreement.appliedCharges, true);

          this.lockCalculationControl();
        }
      }
    }
  }

  private enable(formControl: FormControl): void {
    formControl.enable();
  }

  private disable(formControl: FormControl): void {
    formControl.disable();
  }

  private toggleLoanProduct(outline: LoanProductOutline): void {
    this.selectedProduct = outline;
    this.chargesFormService.resetCharges();
    this.initCollateralForm(outline);
    this.unlockCalculationControl(outline);
  }

  private toggleBankInfoEnabled(enabled: boolean): void {
    const branchSortCodeControl = this.formGroup.get('branchSortCode');
    if (enabled) {
      branchSortCodeControl.enable();
    } else {
      branchSortCodeControl.disable();
    }
  }

  private initCollateralForm(outline: LoanProductOutline): void {
    this.fields = [];

    if (outline.loanType === 'COLLATERAL' && outline.collateral) {
      if (outline.collateral.type === 'DEPOSIT_SECURED') {
        this.collateralFormGroup = this.formBuilder.group({
          attachedResource: [{value: '', disabled: false}, [Validators.required]],
          securedAmount: [{value: '', disabled: false}, []]
        });
      } else {
        this.fields = outline.collateral.fields;
        this.collateralFormGroup = this.formBuilder.group({
          attachedResource: [{value: '', disabled: true}],
          securedAmount: [{value: '', disabled: true}],
          values: this.customFieldValueService.initValuesForm(outline.collateral.fields)
        });
      }
    } else {
      this.collateralFormGroup = this.formBuilder.group({
        attachedResource: [{value: '', disabled: true}],
        securedAmount: [{value: '', disabled: true}]
      });
    }
  }

  private setRangeValidators(control: FormControl, min: number, max: number, scale?: number): void {
    if (min) {
      control.setValue(null);
      this.enable(control);
      control.setValidators([
        Validators.required,
        FimsValidators.minValue(min),
        FimsValidators.maxValue(max)
      ]);
    } else {
      control.setValue(scale ? max.toFixed(scale) : max);
      this.disable(control);
    }
  }

  onSave(): void {
    const existingAgreement = this.formData.loanAgreement;

    let attachedCollateral: AttachedCollateral;

    if (this.selectedProduct.loanType === 'COLLATERAL' && this.selectedProduct.collateral) {
      attachedCollateral = {
        type: this.selectedProduct.collateral.type
      };

      if (this.selectedProduct.collateral.type === 'DEPOSIT_SECURED') {
        attachedCollateral.attachedResource = this.collateralFormGroup.get('attachedResource').value;
        attachedCollateral.securedAmount = this.collateralFormGroup.get('securedAmount').value;
      } else {
        attachedCollateral.values = this.customFieldValueService.getValues(this.selectedProduct.collateral.catalogIdentifier);
        attachedCollateral.fields = existingAgreement ?
          existingAgreement.attachedCollateral.fields : this.selectedProduct.collateral.fields;
      }
    }

    const loanPeriod = this.selectedProduct.term.loanPeriod;

    let branchSortCode: string;

    if (this.formGroup.get('bankInfoEnabled').value) {
      branchSortCode = this.formGroup.get('branchSortCode').value;
    }

    const loanAgreement: LoanAgreement = {
      loanAccount: existingAgreement ? existingAgreement.loanAccount : undefined,
      loanType: this.selectedProduct.loanType,
      productShortName: this.selectedProduct.shortName,
      term: {
        period: this.selectedProduct.term.duration.period,
        length: this.length,
        interestType: this.selectedProduct.term.interest.interestType,
        interestRate: this.termFormGroup.get('interestRate').value,
        principal: this.principalControl.value,
        payment: {
          firstPaymentDate: toShortISOString(this.termFormGroup.get('interestPaymentFirstDueDate').value),
          period: this.termFormGroup.get('period').value,
          amount: this.paymentControl.value
        },
        loanPeriod: loanPeriod ? loanPeriod.period : undefined,
        loanPeriodDuration: loanPeriod ? loanPeriod.duration : undefined
      },
      appliedCharges: this.chargesFormService.getCharges(),
      purpose: this.formGroup.get('purpose').value,
      disbursementMethod: this.formGroup.get('disbursementMethod').value,
      attachedCollateral,
      hasPendingAssessments: existingAgreement ? existingAgreement.hasPendingAssessments : false,
      state: existingAgreement ? existingAgreement.state : 'PENDING',
      currentUserCanApprove: existingAgreement ? existingAgreement.currentUserCanApprove : false,
      inRestructuring: existingAgreement ? existingAgreement.inRestructuring : false,
      branchSortCode
    };

    if (this.selectedProduct.dtiConfig) {
      loanAgreement.debtToIncome = this.debtToIncomeService.getDebtToIncome();
    }

    this.save.emit(loanAgreement);
  }

  get isValid(): boolean {
    return this.formGroup.valid
      && this.chargesFormGroup.valid
      && this.debtToIncomeFormGroup.valid
      && this.isCollateralFormGroupValid
      && this.locked;
  }

  get isCollateralFormGroupValid(): boolean {
    if (this.selectedProduct && this.selectedProduct.loanType === 'COLLATERAL') {
      // FormGroup is only set when product is selected
      if (this.collateralFormGroup) {
        return this.collateralFormGroup.valid;
      } else {
        return false;
      }
    }

    // If not collateral it should not be active
    return true;
  }

  onCancel(): void {
    this.cancel.emit();
  }

  get length(): number {
    return parseInt(this.termFormGroup.get('length').value, 10);
  }

  calculate(): void {
    const principalActive = this.calculateValueControl.value === 'principal';
    const paymentActive = this.calculateValueControl.value === 'payment';

    const loanPeriod = this.selectedProduct.term.loanPeriod;

    const data: CalculateScheduleData = {
      shortName: this.selectedProduct.shortName,
      term: {
        period: this.selectedProduct.term.duration.period,
        length: this.length,
        interestType: this.selectedProduct.term.interest.interestType,
        interestRate: this.termFormGroup.get('interestRate').value,
        principal: principalActive ? this.principalControl.value : undefined,
        payment: {
          firstPaymentDate: toShortISOString(this.termFormGroup.get('interestPaymentFirstDueDate').value),
          period: this.termFormGroup.get('period').value,
          amount: paymentActive ? this.paymentControl.value : undefined
        },
        loanPeriod: loanPeriod ? loanPeriod.period : undefined,
        loanPeriodDuration: loanPeriod ? loanPeriod.duration : undefined
      }
    };

    const dialogRef = this.dialog.open(LoanAgreementScheduleDialogComponent, {
      data,
      width: '60%',
      height: '80%'
    });

    dialogRef.afterClosed().subscribe((amortizationSchedule: AmortizationSchedule) => {
      const scale = this.selectedProduct.currency.scale;
      if (amortizationSchedule) {
        const detail = amortizationSchedule.amortizationDetail;
        this.principalControl.setValue(detail.loanAmount.toFixed(scale));
        this.paymentControl.setValue(detail.payment.toFixed(scale));
        this.chargesFormService.setCharges(amortizationSchedule.appliedCharges);
        this.amountToSecure = amortizationSchedule.amountToSecure;
        this.lockCalculationControl();
      }
    });
  }

  reset(): void {
    this.unlockCalculationControl(this.selectedProduct);
  }

  private lockCalculationControl(): void {
    this.disable(this.durationControl);
    this.disable(this.interestControl);
    this.disable(this.periodControl);
    this.disable(this.firstDueDateControl);
    this.disable(this.principalControl);
    this.disable(this.paymentControl);

    // For some reason disable triggeres a value changes, therefore we are preventing emitting a event here
    // as it would reenable principal/payment control
    this.calculateValueControl.disable({
      emitEvent: false,
      onlySelf: true
    });

    this.locked = true;
  }

  private unlockCalculationControl(outline: LoanProductOutline): void {
    const durationRange = outline.term.duration;
    this.setRangeValidators(this.durationControl, durationRange.min, durationRange.max);

    const scale = outline.currency.scale;

    const interest = outline.term.interest;
    this.setRangeValidators(this.interestControl, interest.min, interest.max, scale);

    const principalRange = outline.term.principalRange;
    this.setRangeValidators(this.principalControl, principalRange.min, principalRange.max, scale);

    this.calculateValueControl.setValue('principal');
    this.paymentControl.setValue(null);

    if (principalRange.min) {
      this.enable(this.principalControl);

      if (!this.isRestructure) {
        this.enable(this.calculateValueControl);
      }
    } else {
      this.disable(this.principalControl);
      this.disable(this.paymentControl);
      this.disable(this.calculateValueControl);
    }

    this.enable(this.periodControl);
    this.enable(this.firstDueDateControl);

    this.locked = false;
  }

  get principalControl(): FormControl {
    return this.termFormGroup.get('principal') as FormControl;
  }

  get paymentControl(): FormControl {
    return this.termFormGroup.get('payment') as FormControl;
  }

  get calculateValueControl(): FormControl {
    return this.formGroup.get('calculateValue') as FormControl;
  }

  get durationControl(): FormControl {
    return this.termFormGroup.get('length') as FormControl;
  }

  get interestControl(): FormControl {
    return this.termFormGroup.get('interestRate') as FormControl;
  }

  get periodControl(): FormControl {
    return this.termFormGroup.get('period') as FormControl;
  }

  get firstDueDateControl(): FormControl {
    return this.termFormGroup.get('interestPaymentFirstDueDate') as FormControl;
  }

  get showFeesStep(): boolean {
    const chargesArray = this.chargesFormGroup.get('charges') as FormArray;
    return chargesArray.length > 0;
  }

  addMonthlyIncomeEntry(): void {
    this.debtToIncomeService.addMonthlyIncomeEntry();
  }

  removeMonthlyIncomeEntry(index): void {
    this.debtToIncomeService.removeMonthlyIncomeEntry(index);
  }

  addObligationEntry(): void {
    this.debtToIncomeService.addObligationEntry();
  }

  removeObligationEntry(index): void {
    this.debtToIncomeService.removeObligationEntry(index);
  }

  durationPlaceholder(duration?: Duration): string {
    return duration && duration.period ? `Duration in ${duration.period.toLowerCase()}` : 'Duration';
  }

  durationHint(duration?: Duration): string {
    if (!duration) {
      return '-';
    }

    const max = duration.max;
    if (duration.min) {
      return `${duration.min} - ${max}`;
    } else {
      return `Max: ${max}`;
    }
  }

  principalHint(principalRange?: PrincipalRange): string {
    if (!principalRange) {
      return '-';
    }

    const max = this.displayFimsNumber.transform(principalRange.max);
    if (principalRange.min) {
      return `${this.displayFimsNumber.transform(principalRange.min)} - ${max}`;
    } else {
      return `Max: ${max}`;
    }
  }

  interestHint(interest?: Interest): string {
    if (!interest) {
      return '-';
    }

    const max = this.displayFimsNumber.transform(interest.max);
    if (interest.min) {
      return `${this.displayFimsNumber.transform(interest.min)} - ${max}`;
    } else {
      return `Max: ${max}`;
    }
  }

  get isRestructure(): boolean {
    if (this.formData && this.formData.loanAgreement) {
      return this.formData.loanAgreement.state === 'RESTRUCTURE';
    }

    return false;
  }

  get isDecliningInterest(): boolean {
    if (this.selectedProduct) {
      return this.selectedProduct.term.interest.interestType === 'DECLINING';
    }
    return false;
  }

  get isFlatInterest(): boolean {
    if (this.selectedProduct) {
      return this.selectedProduct.term.interest.interestType === 'FLAT';
    }
    return false;
  }

}
