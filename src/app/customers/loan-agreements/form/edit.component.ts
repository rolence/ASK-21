/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Component} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {ConsumerLoanService} from '../../../core/services/loan/consumer-loan.service';
import {LoanAgreement} from '../../../core/services/loan/domain/agreement/loan-agreement.model';
import {UpdateLoanAgreementAction} from '../../store/loan-agreements/loan-agreement.actions';
import {ActivatedRoute, Router} from '@angular/router';
import * as fromCustomers from '../../store';
import {LoanAgreementFormData} from './form.component';
import {Store} from '@ngrx/store';
import {Customer} from '../../../core/services/customer/domain/customer.model';
import {AccountingService} from '../../../core/services/accounting/accounting.service';

@Component({
  templateUrl: './edit.component.html'
})
export class LoanAgreementEditComponent {

  formData$: Observable<LoanAgreementFormData>;
  customer$: Observable<Customer>;
  loanAgreement$: Observable<LoanAgreement>;

  constructor(private router: Router, private route: ActivatedRoute,
              private store: Store<fromCustomers.State>, private consumerLoanService: ConsumerLoanService,
              private accountingService: AccountingService) {
    const loanProducts$ = consumerLoanService.fetchAllProducts()
      .map(products => products.filter(product => product.active));

    this.loanAgreement$ = this.store.select(fromCustomers.getSelectedAgreement);
    this.customer$ = this.store.select(fromCustomers.getSelectedCustomer);

    this.formData$ = Observable.combineLatest(
      loanProducts$,
      this.store.select(fromCustomers.getActiveSearchDeposits),
      accountingService.fetchBankInformations(),
      this.loanAgreement$
    ).map(([loanProducts, productInstances, bankInfos, loanAgreement]) => ({
      loanProducts,
      productInstances,
      bankInfos,
      loanAgreement,
      editMode: true
    }));
  }

  onSave(agreement: LoanAgreement): void {
    this.customer$
      .take(1)
      .map(customer => new UpdateLoanAgreementAction({
        customerNumber: customer.identifier,
        agreement,
        activatedRoute: this.route
      }))
      .subscribe(this.store);
  }

  onCancel(): void {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

}
