/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Injectable} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AppliedCharge} from '../../../../core/services/loan/domain/agreement/applied-charge.model';
import {FimsValidators} from '../../../../core/validator/validators';

@Injectable()
export class ChargesFormService {

  private _chargesFormGroup: FormGroup = new FormGroup({});

  constructor(private formBuilder: FormBuilder) {}

  initChargesForm(): FormGroup {
    this._chargesFormGroup = this.buildChargesForm();

    return this._chargesFormGroup;
  }

  private buildChargesForm(): FormGroup {
    return this.formBuilder.group({
      charges: this.formBuilder.array([])
    });
  }

  get chargesFormGroup(): FormGroup {
    return this._chargesFormGroup;
  }

  getCharges(): AppliedCharge[] {
    const appliedCharges: AppliedCharge[] = (this.chargesFormGroup.get('charges') as FormArray).getRawValue()
      .filter(charge => charge.active);

    return appliedCharges;
  }

  setCharges(appliedCharges: AppliedCharge[], allActive: boolean = false): void {
    appliedCharges.sort((a, b) => {
      if (a.adHoc === b.adHoc) {
        return 0;
      }
      if (a.adHoc) {
        return 1;
      }
      return -1;
    });

    const chargeFormGroups: FormGroup[] = appliedCharges.map(charge =>
      this.formBuilder.group({
        active: [{ value: true, disabled: !charge.adHoc}],
        description: [{ value: charge.description, disabled: true }],
        action: [{ value: charge.action, disabled: true }],
        revenueAccount: [{ value: charge.revenueAccount, disabled: true }],
        adHoc: [charge.adHoc],
        amount: [{ value: charge.amount, disabled: !charge.adHoc }, [Validators.required, FimsValidators.greaterThanValue(0)]]
      })
    );

    chargeFormGroups.forEach((chargeGroup: FormGroup) => {
      const activeControl = chargeGroup.get('active');
      const adHoc = chargeGroup.get('adHoc').value;

      activeControl.valueChanges.subscribe(active => {
        const amountControl = chargeGroup.get('amount');
        if (active && adHoc) {
          amountControl.enable();
        } else {
          amountControl.disable();
        }
      });

      if (!allActive) {
        activeControl.setValue(!adHoc);
      }
    });

    const chargesArray = this.resetCharges();
    chargeFormGroups.forEach(formGroup => chargesArray.push(formGroup));
  }

  resetCharges(): FormArray {
    const chargesArray = this.chargesFormGroup.get('charges') as FormArray;

    while (chargesArray.length !== 0) {
      chargesArray.removeAt(0);
    }

    return chargesArray;
  }
}
