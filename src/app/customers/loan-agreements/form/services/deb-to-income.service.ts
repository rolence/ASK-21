/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */


import {Injectable} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DebtToIncomeEntry} from '../../../../core/services/loan/domain/agreement/debt-to-income-entry.model';
import {DebtToIncome} from '../../../../core/services/loan/domain/agreement/debt-to-income.model';

@Injectable()
export class DebtToIncomeFormService {

  private _debtToIncomeForm: FormGroup = new FormGroup({});

  constructor(private formBuilder: FormBuilder) {}

  initDebtToIncomeForm(): FormGroup {
    this._debtToIncomeForm = this.buildDebtToIncomeForm();

    this._debtToIncomeForm.valueChanges
      .startWith(null)
      .subscribe(() => {
        this._debtToIncomeForm.get('ratio').setValue(this.divideIfNotZero(this.debtTotal, this.incomeTotal), {
          onlySelf: true,
          emitEvent: false
        });
      });

    return this._debtToIncomeForm;
  }

  private buildDebtToIncomeForm(): FormGroup {
    return this.formBuilder.group({
      ratio: [0, [Validators.required]],
      monthlyIncome: this.initDebtToIncomeEntries([]),
      monthlyObligations: this.initDebtToIncomeEntries([])
    });
  }

  private initDebtToIncomeEntries(entries: DebtToIncomeEntry[]): FormArray {
    const formControls: FormGroup[] = [];
    entries.forEach(entry => formControls.push(this.initEntry(entry)));
    return this.formBuilder.array(formControls);
  }

  private initEntry(entry?: DebtToIncomeEntry): FormGroup {
    return this.formBuilder.group({
      type: [entry ? entry.type : '', [Validators.required, Validators.maxLength(256)]],
      value: [entry ? entry.value : '', Validators.required]
    });
  }

  addMonthlyIncomeEntry(entry?: DebtToIncomeEntry): void {
    this.monthlyIncome.push(this.initEntry(entry));
  }

  removeMonthlyIncomeEntry(index: number): void {
    this.monthlyIncome.removeAt(index);
  }

  addObligationEntry(entry?: DebtToIncomeEntry): void {
    this.obligations.push(this.initEntry(entry));
  }

  removeObligationEntry(index: number): void {
    this.obligations.removeAt(index);
  }

  divideIfNotZero(numerator, denominator): number {
    if (denominator === 0 || isNaN(denominator)) {
      return null;
    } else {
      return (numerator / denominator) * 100;
    }
  }

  private get monthlyIncome(): FormArray {
    return this.debtToIncomeForm.get('monthlyIncome') as FormArray;
  }

  private get obligations(): FormArray {
    return this.debtToIncomeForm.get('monthlyObligations') as FormArray;
  }

  private get debtToIncomeForm(): FormGroup {
    return this._debtToIncomeForm;
  }

  get incomeTotal(): number {
    return this.sum(this.monthlyIncome.value);
  }

  get debtTotal(): number {
    return this.sum(this.obligations.value);
  }

  private sum(factors: DebtToIncomeEntry[]): number {
    return factors.reduce((acc, val) => acc + parseFloat(val.value), 0);
  }


  setDebtToIncome(debtToIncome: DebtToIncome): void {
    this.debtToIncomeForm.reset({
      ratio: debtToIncome.ratio,
    });

    debtToIncome.monthlyIncome.forEach(entry => this.addMonthlyIncomeEntry(entry));
    debtToIncome.monthlyObligations.forEach(entry => this.addObligationEntry(entry));
  }

  getDebtToIncome(): DebtToIncome {
    return {
      ratio: this.debtToIncomeForm.get('ratio').value,
      monthlyIncome: this.monthlyIncome.value,
      monthlyObligations: this.obligations.value
    };
  }

}
