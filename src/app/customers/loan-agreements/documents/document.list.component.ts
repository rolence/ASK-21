/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Component, OnDestroy} from '@angular/core';
import {DocumentOutline} from '../../../core/services/loan/domain/agreement/document-outline.model';
import {Observable} from 'rxjs/Observable';
import {DeleteAction, DownloadAction, SearchAction} from '../../store/loan-agreements/documents/document.actions';
import * as fromCustomers from '../../store';
import {Subscription} from 'rxjs/Subscription';
import {ActivatedRoute, Router} from '@angular/router';
import {DialogService} from '../../../core/services/dialog/dialog.service';
import {Store} from '@ngrx/store';
import {LoanAgreement} from '../../../core/services/loan/domain/agreement/loan-agreement.model';

@Component({
  templateUrl: './document.list.component.html'
})
export class LoanAgreementDocumentListComponent implements OnDestroy {

  private selectionSubscription: Subscription;
  private downloadSubscription: Subscription;
  private deletionSubscription: Subscription;

  documents$: Observable<DocumentOutline[]>;
  canChangeAgreement$: Observable<boolean>;
  canUploadDocument$: Observable<boolean>;
  canDeleteDocument$: Observable<boolean>;
  loanAgreement$: Observable<LoanAgreement>;

  constructor(private router: Router, private route: ActivatedRoute,
              private store: Store<fromCustomers.State>, private dialogService: DialogService) {
    this.documents$ = store.select(fromCustomers.getSearchDocuments);
    this.loanAgreement$ = store.select(fromCustomers.getSelectedAgreement);

    this.selectionSubscription = Observable.combineLatest(
      store.select(fromCustomers.getSelectedCustomer),
      this.loanAgreement$
    ).map(([customer, agreement]) => new SearchAction({
      customerNumber: customer.identifier,
      loanAccount: agreement.loanAccount
    })).subscribe(this.store);

    this.canChangeAgreement$ = store.select(fromCustomers.canChangeAgreement);
    this.canUploadDocument$ = store.select(fromCustomers.canUploadDocument);
    this.canDeleteDocument$ = store.select(fromCustomers.canDeleteDocument);
  }

  ngOnDestroy(): void {
    this.selectionSubscription.unsubscribe();

    if (this.deletionSubscription) {
      this.deletionSubscription.unsubscribe();
    }

    if (this.downloadSubscription) {
      this.downloadSubscription.unsubscribe();
    }
  }

  download(document: DocumentOutline): void {
    this.downloadSubscription = Observable.combineLatest(
      this.store.select(fromCustomers.getSelectedCustomer).take(1),
      this.store.select(fromCustomers.getSelectedAgreement).take(1)
    ).map(([customer, agreement]) => new DownloadAction({
      customerNumber: customer.identifier,
      loanAccount: agreement.loanAccount,
      fileName: document.fileName
    })).subscribe(this.store);
  }

  deleteDocument(document: DocumentOutline): void {
    const confirmDelete = this.confirmDeletion().filter(accept => accept);

    this.deletionSubscription = Observable.combineLatest(
      confirmDelete,
      this.store.select(fromCustomers.getSelectedCustomer).take(1),
      this.store.select(fromCustomers.getSelectedAgreement).take(1)
    ).map(([accept, customer, agreement]) => new DeleteAction({
      customerNumber: customer.identifier,
      loanAccount: agreement.loanAccount,
      document,
      activatedRoute: this.route
    })).subscribe(this.store);
  }

  private confirmDeletion(): Observable<boolean> {
    return this.dialogService.openConfirm({
      message: 'Do you want to delete this document?',
      title: 'Confirm deletion',
      acceptButton: 'DELETE DOCUMENT',
    });
  }

  uploadDocument(): void {
    this.router.navigate(['upload'], { relativeTo: this.route });
  }
}
