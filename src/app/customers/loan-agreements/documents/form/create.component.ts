/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Component, OnDestroy} from '@angular/core';
import {Document} from '../../../../core/services/loan/domain/agreement/document-meta-data.model';
import {ActivatedRoute, Router} from '@angular/router';
import {CreateAction} from '../../../store/loan-agreements/documents/document.actions';
import {Observable} from 'rxjs/Observable';
import * as fromCustomers from '../../../store';
import {Subscription} from 'rxjs/Subscription';
import {Store} from '@ngrx/store';

@Component({
  templateUrl: './create.component.html'
})
export class CreateLoanAgreementDocumentComponent implements OnDestroy {

  private saveSubscription: Subscription;

  constructor(private router: Router, private route: ActivatedRoute, private store: Store<fromCustomers.State>) {
  }

  onSave(document: Document): void {
    this.saveSubscription = Observable.combineLatest(
      this.store.select(fromCustomers.getSelectedCustomer),
      this.store.select(fromCustomers.getSelectedAgreement)
    ).map(([customer, agreement]) => new CreateAction({
      customerNumber: customer.identifier,
      loanAccount: agreement.loanAccount,
      documents: [document],
      activatedRoute: this.route
    })).subscribe(this.store);
  }

  onCancel(): void {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

  ngOnDestroy(): void {
    if (this.saveSubscription) {
      this.saveSubscription.unsubscribe();
    }
  }
}
