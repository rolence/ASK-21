/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {ChangeDetectorRef, Pipe, PipeTransform} from '@angular/core';
import {TranslatePipe, TranslateService} from '@ngx-translate/core';
import {paymentPeriodOptionsList} from '../form/model/payment-period-options.model';
import {Period} from '../../../core/services/loan/domain/agreement/payment.model';

@Pipe({
  name: 'paymentPeriodLabel',
  pure: true
})
export class PaymentPeriodLabelPipe extends TranslatePipe implements PipeTransform {

  constructor(private translateService: TranslateService, private changeDetectorRef: ChangeDetectorRef) {
    super(translateService, changeDetectorRef);
  }

  transform(period: Period): string {
    const label = paymentPeriodOptionsList.find(option => option.period === period).label;
    return super.transform(label);
  }
}
