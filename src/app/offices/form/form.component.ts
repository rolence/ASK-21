/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import {Office} from '../../core/services/office/domain/office.model';
import {Country} from '../../core/services/country/model/country.model';
import {CountryService} from '../../core/services/country/country.service';
import {FimsValidators} from '../../core/validator/validators';
import {countryExists} from '../../core/validator/country-exists.validator';

@Component({
  selector: 'aten-office-form',
  templateUrl: './form.component.html'
})
export class OfficeFormComponent implements OnChanges {

  filteredCountries$: Observable<Country[]>;
  detailForm: FormGroup;
  addressForm: FormGroup;

  @Input('editMode') editMode: boolean;
  @Input('office') office: Office;

  @Output() save = new EventEmitter<Office>();
  @Output() cancel = new EventEmitter<void>();

  constructor(private formBuilder: FormBuilder, private countryService: CountryService) {
    this.detailForm = this.formBuilder.group({
      identifier: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(32), FimsValidators.urlSafe]],
      name: ['', [Validators.required, Validators.maxLength(256)]],
      description: ['', Validators.maxLength(2048)]
    });

    this.addressForm = this.formBuilder.group({
      street: ['', [Validators.required, Validators.maxLength(256)]],
      city: ['', [Validators.required, Validators.maxLength(256)]],
      postalCode: ['', Validators.maxLength(32)],
      region: ['', Validators.maxLength(256)],
      country: ['', [Validators.required], countryExists(this.countryService)]
    });

    this.filteredCountries$ = this.addressForm.get('country').valueChanges
      .map(country => country && typeof country === 'object' ? country.displayName : country)
      .map(searchTerm => this.countryService.fetchCountries(searchTerm));
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.office && this.office) {
      this.detailForm.reset({
        identifier: this.office.identifier,
        name: this.office.name,
        description: this.office.description
      });

      const address = this.office.address;

      if (address) {
        const country = this.countryService.fetchByCountryCode(address.countryCode);

        this.addressForm.reset({
          street: address.street,
          city: address.city,
          postalCode: address.postalCode,
          region: address.region,
          country
        });
      }
    }
  }

  get valid(): boolean {
    return this.detailForm.valid && (this.addressForm.pristine ? true : this.addressForm.valid);
  }

  onSave(): void {
    const office: Office = {
      identifier: this.detailForm.get('identifier').value,
      name: this.detailForm.get('name').value,
      description: this.detailForm.get('description').value
    };

    if (this.addressForm.pristine) {
      office.address = this.office.address;
    } else {
      const country: Country = this.addressForm.get('country').value;

      office.address = {
        street: this.addressForm.get('street').value,
        city: this.addressForm.get('city').value,
        postalCode: this.addressForm.get('postalCode').value,
        region: this.addressForm.get('region').value,
        country: country.name,
        countryCode: country.alpha2Code
      };
    }

    this.save.emit(office);
  }

  onCancel(): void {
    this.cancel.emit();
  }

}
