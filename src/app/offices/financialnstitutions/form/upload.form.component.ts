/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {ActivatedRoute, Router} from '@angular/router';
import * as fromAccounting from '../../../accounting/store';
import {Store} from '@ngrx/store';
import {Component} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {OfficeService} from '../../../core/services/office/office.service';
import {FimsValidators} from '../../../core/validator/validators';

@Component({
  templateUrl: './upload.form.component.html'
})
export class UploadFinancialInstitutionFormComponent {

  form: FormGroup;

  constructor(private store: Store<fromAccounting.State>, private router: Router,
              private route: ActivatedRoute, private officeService: OfficeService, private formBuilder: FormBuilder) {
    this.form = formBuilder.group({
      file: ['', [
        Validators.required,
        FimsValidators.mediaType(['text/csv', 'application/vnd.ms-excel'])
      ]]
    });
  }

  upload(): void {
    const file = this.form.get('file').value as File;
    this.officeService.uploadFinancialInstitutions(file).subscribe(() => {
      this.cancel();
    });
  }

  cancel(): void {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

}
