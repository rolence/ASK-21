/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Country} from '../../../core/services/country/model/country.model';
import {FinancialInstitution} from '../../../core/services/office/domain/financial-institution.model';
import {CountryService} from '../../../core/services/country/country.service';
import {OfficeService} from '../../../core/services/office/office.service';
import {FimsValidators} from '../../../core/validator/validators';
import {countryExists} from '../../../core/validator/country-exists.validator';

@Component({
  selector: 'aten-institution-form',
  templateUrl: './form.component.html'
})
export class FinancialInstitutionFormComponent implements OnChanges {

  filteredCountries$: Observable<Country[]>;
  detailForm: FormGroup;
  addressForm: FormGroup;

  @Input() institution: FinancialInstitution;

  @Output() save = new EventEmitter<FinancialInstitution>();
  @Output() cancel = new EventEmitter<void>();

  constructor(private officeService: OfficeService, private formBuilder: FormBuilder, private countryService: CountryService) {
    this.detailForm = formBuilder.group({
      code: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(34), FimsValidators.urlSafe ]],
      name: ['', [Validators.required ]],
      network: ['ACH', [Validators.required ]]
    });

    this.addressForm = this.formBuilder.group({
      street: ['', [Validators.required, Validators.maxLength(256)]],
      city: ['', [Validators.required, Validators.maxLength(256)]],
      postalCode: ['', Validators.maxLength(32)],
      region: ['', Validators.maxLength(256)],
      country: ['', [Validators.required], countryExists(this.countryService)]
    });

    this.filteredCountries$ = this.addressForm.get('country').valueChanges
      .map(country => country && typeof country === 'object' ? country.displayName : country)
      .map(searchTerm => this.countryService.fetchCountries(searchTerm));
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.institution && this.institution) {
      this.detailForm.reset({
        code: this.institution.code,
        name: this.institution.name,
        network: this.institution.network
      });

      const address = this.institution.address;

      if (address) {
        const country = this.countryService.fetchByCountryCode(address.countryCode);

        this.addressForm.reset({
          street: address.street,
          city: address.city,
          postalCode: address.postalCode,
          region: address.region,
          country
        });
      }
    }
  }

  onSave(): void {
    const institution: FinancialInstitution = this.detailForm.getRawValue();

    if (this.addressForm.pristine) {
      institution.address = this.institution ? this.institution.address : undefined;
    } else {
      const country: Country = this.addressForm.get('country').value;

      institution.address = {
        street: this.addressForm.get('street').value,
        city: this.addressForm.get('city').value,
        postalCode: this.addressForm.get('postalCode').value,
        region: this.addressForm.get('region').value,
        country: country.name,
        countryCode: country.alpha2Code
      };
    }

    this.save.emit(institution);
  }

  get valid(): boolean {
    return this.detailForm.valid && (this.addressForm.pristine ? true : this.addressForm.valid);
  }

  get editMode(): boolean {
    return !!this.institution;
  }

  onCancel(): void {
    this.cancel.emit();
  }

}
