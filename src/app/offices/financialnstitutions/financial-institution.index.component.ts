/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {ActivatedRoute, Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {Component, OnDestroy} from '@angular/core';
import * as fromOffices from '../store';
import {SelectAction} from '../store/financialInstitutions/institution-actions';
import {Subscription} from 'rxjs/Subscription';
import {BreadCrumbService} from '../../shared/bread-crumbs/bread-crumb.service';
import {FinancialInstitution} from '../../core/services/office/domain/financial-institution.model';

@Component({
  templateUrl: './financial-institution.index.component.html'
})
export class FinancialInstitutionIndexComponent implements OnDestroy {

  private actionsSubscription: Subscription;
  private institutionSubscription: Subscription;

  constructor(private router: Router, private route: ActivatedRoute,
              private store: Store<fromOffices.State>, private breadCrumbService: BreadCrumbService) {
    this.actionsSubscription = this.route.params
      .map(params => new SelectAction({
        code: params['code']
      }))
      .subscribe(this.store);

    this.institutionSubscription = store.select(fromOffices.getSelectedInstitution)
      .filter(institution => !!institution)
      .subscribe((institution: FinancialInstitution) =>
        this.breadCrumbService.setCustomBreadCrumb(this.route.snapshot, institution.name)
      );
  }

  ngOnDestroy(): void {
    this.actionsSubscription.unsubscribe();
    this.institutionSubscription.unsubscribe();

    this.store.dispatch(new SelectAction({
      code: null
    }));
  }

}
