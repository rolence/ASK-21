/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import * as fromRoot from '../../core/store';
import * as fromTellers from '../store/teller/tellers.reducer';
import * as fromDenominations from '../store/teller/denomination/denominations.reducer';
import {ActionReducerMap, createFeatureSelector} from '@ngrx/store';
import {createSelector} from 'reselect';
import {
  createResourceReducer,
  getResourceAll,
  getResourceEntities,
  getResourceLoadedAt,
  getResourceSelected,
  ResourceState
} from '../../core/store/util/resource.reducer';
import {createFormReducer, FormState, getFormError} from '../../core/store/util/form.reducer';
import {InjectionToken} from '@angular/core';
import * as fromInstitutions from '../store/financialInstitutions/institution.reducer';
import {Teller} from '../../core/services/teller/domain/teller.model';
import {Office} from '../../core/services/office/domain/office.model';

export interface OfficeState {
  offices: ResourceState;
  officeForm: FormState;
  tellers: ResourceState;
  tellerForm: FormState;
  denominations: fromDenominations.State;
  institutions: fromInstitutions.State;
}

export const reducers: ActionReducerMap<OfficeState> = {
  offices: createResourceReducer('Office'),
  officeForm: createFormReducer('Office'),
  tellers: createResourceReducer('Office Teller', fromTellers.reducer, 'code'),
  tellerForm: createFormReducer('Office Teller'),
  denominations: fromDenominations.reducer,
  institutions: fromInstitutions.reducer
};

export const reducerToken = new InjectionToken<ActionReducerMap<OfficeState>>('Office reducers');

export const reducerProvider = [
  { provide: reducerToken, useValue: reducers }
];

export interface State extends fromRoot.State {
  office: OfficeState;
}

export const selectOffice = createFeatureSelector<OfficeState>('office');

export const getOfficesState = createSelector(selectOffice, (state: OfficeState) => state.offices);

export const getOfficeFormState = createSelector(selectOffice, (state: OfficeState) => state.officeForm);
export const getOfficeFormError = createSelector(getOfficeFormState, getFormError);

export const getOfficeEntities = createSelector(getOfficesState, getResourceEntities);
export const getOfficesLoadedAt = createSelector(getOfficesState, getResourceLoadedAt);
export const getSelectedOffice = createSelector(getOfficesState, getResourceSelected);

export const getTellerState = createSelector(selectOffice, (state: OfficeState) => state.tellers);

export const getTellerFormState = createSelector(selectOffice, (state: OfficeState) => state.tellerForm);
export const getTellerFormError = createSelector(getTellerFormState, getFormError);

export const getAllTellerEntities = createSelector(getTellerState, getResourceAll);

export const getTellersLoadedAt = createSelector(getTellerState, getResourceLoadedAt);
export const getSelectedTeller = createSelector(getTellerState, getResourceSelected);

export const getDenominationState = createSelector(selectOffice, (state: OfficeState) => state.denominations);
export const getDenominationsEntities = createSelector(getDenominationState, fromDenominations.getDenominationEntities);

export const getCurrentSelected = createSelector(getSelectedOffice, getSelectedTeller, (office: Office, teller: Teller) => {
  return {
    officeIdentifier: office.identifier,
    tellerCode: teller.code
  };
});

/**
 * Institution
 */

export const selectInstitutionState = createSelector(selectOffice, (state: OfficeState) => state.institutions);
export const getSelectedInstitution = createSelector(selectInstitutionState, fromInstitutions.getSelectedInstitution);
export const getInstitutionsLoadedAt = createSelector(selectInstitutionState, fromInstitutions.getLoadedAt);
