/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {TdStepComponent} from '@covalent/core';
import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {TellerTransactionCosts} from '../../../../core/services/teller/domain/teller-transaction-costs.model';
import {ProductInstanceOutline} from '../../../../core/services/depositAccount/domain/instance/product-instance-outline.model';
import {CreateTransactionEvent} from '../domain/create-transaction-event.model';
import {ConfirmTransactionEvent} from '../domain/confirm-transaction-event.model';
import {FimsValidators} from '../../../../core/validator/validators';

@Component({
  selector: 'aten-teller-ach-form',
  templateUrl: './form.component.html'
})
export class TellerACHFormComponent implements OnInit, OnChanges {

  form: FormGroup;
  currentBalance: number;
  voucherNumber: string;
  chargesIncluded = true;

  @Input('transactionCosts') transactionCosts: TellerTransactionCosts;
  @Input('productInstances') productInstances: ProductInstanceOutline[];
  @Input('transactionConfirmed') transactionConfirmed: boolean;

  @Output('onCreateTransaction') onCreateTransaction = new EventEmitter<CreateTransactionEvent>();
  @Output('onConfirmTransaction') onConfirmTransaction = new EventEmitter<ConfirmTransactionEvent>();
  @Output('onCancelTransaction') onCancelTransaction = new EventEmitter<string>();
  @Output('onCancel') onCancel = new EventEmitter<void>();
  @Output('onPrintReceipt') onPrintReceipt = new EventEmitter<string>();

  @ViewChild('transactionStep') transactionStep: TdStepComponent;
  @ViewChild('confirmationStep') confirmationStep: TdStepComponent;

  constructor(private formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({
      productInstance: ['', Validators.required],
      amount: ['', [Validators.required, FimsValidators.greaterThanValue(0)]]
    });

    this.form.get('productInstance').valueChanges
      .subscribe(productInstance => this.toggleProductInstance(productInstance));
  }

  private toggleProductInstance(productInstance: ProductInstanceOutline): void {
    this.currentBalance = parseFloat(productInstance.balance);

    const amountValidators: ValidatorFn[] = [Validators.required, FimsValidators.greaterThanValue(0)];
    amountValidators.push(FimsValidators.maxValue(this.currentBalance));

    const amountControl = this.form.get('amount') as FormControl;
    amountControl.setValidators(amountValidators);
    amountControl.updateValueAndValidity();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.transactionCosts && this.transactionCosts) {
      this.confirmationStep.open();
    }
  }

  ngOnInit(): void {
    this.transactionStep.open();
  }

  createTransaction(): void {
    const productInstance: ProductInstanceOutline = this.form.get('productInstance').value;

    const formData: CreateTransactionEvent = {
      productIdentifier: productInstance.productShortCode,
      accountIdentifier: productInstance.accountIdentifier,
      paymentType: 'TRANSFER',
      amount: this.form.get('amount').value
    };

    this.onCreateTransaction.emit(formData);
  }

  confirmTransaction(chargesIncluded: boolean, voucherNumber: string): void {
    this.onConfirmTransaction.emit({
      chargesIncluded,
      voucherNumber,
      tellerTransactionIdentifier: this.transactionCosts.tellerTransactionIdentifier
    });
  }

  cancelTransaction(): void {
    this.onCancelTransaction.emit(this.transactionCosts.tellerTransactionIdentifier);
  }

  printReceipt(): void {
    this.onPrintReceipt.emit(this.transactionCosts.tellerTransactionIdentifier);
  }

  cancel(): void {
    this.onCancel.emit();
  }

  get createTransactionDisabled(): boolean {
    return this.form.invalid || this.transactionCreated;
  }

  get confirmTransactionDisabled(): boolean {
    return !this.transactionCreated || this.transactionConfirmed;
  }

  get printReceiptDisabled(): boolean {
    return !this.transactionConfirmed;
  }

  get transactionCreated(): boolean {
    return !!this.transactionCosts;
  }

}
