/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, OnDestroy} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {ActivatedRoute, Router} from '@angular/router';
import {TellerTransactionCosts} from '../../../../core/services/teller/domain/teller-transaction-costs.model';
import {TransactionService} from '../services/transaction.service';
import {ProductInstanceOutline} from '../../../../core/services/depositAccount/domain/instance/product-instance-outline.model';
import * as fromTeller from '../../../store';
import {RESET_TRANSACTION_FORM} from '../../../store/transaction.actions';
import {CreateTransactionEvent} from '../domain/create-transaction-event.model';
import {ConfirmTransactionEvent} from '../domain/confirm-transaction-event.model';
import {Store} from '@ngrx/store';

@Component({
  templateUrl: './ach.component.html'
})
export class TellerACHComponent implements OnDestroy {

  transactionCosts$: Observable<TellerTransactionCosts>;
  productInstances$: Observable<ProductInstanceOutline[]>;
  transactionConfirmed$: Observable<boolean>;

  constructor(private router: Router, private route: ActivatedRoute,
              private store: Store<fromTeller.State>, private transactionService: TransactionService) {
    this.productInstances$ = this.store.select(fromTeller.getAllActiveDepositAccounts)
      .map((instances: ProductInstanceOutline[]) => instances.filter(instance => instance.hasExternalAccountReference));
    this.transactionCosts$ = this.store.select(fromTeller.getTransactionCosts);
    this.transactionConfirmed$ = this.store.select(fromTeller.getTransactionConfirmed);
  }

  createTransaction(event: CreateTransactionEvent): void {
    this.transactionService.createTransaction(event, 'ATXN');
  }

  confirmTransaction(event: ConfirmTransactionEvent): void {
    this.transactionService.confirmTransaction(event, this.route);
  }

  cancelTransaction(tellerTransactionIdentifier: string): void {
    this.transactionService.cancelTransaction(tellerTransactionIdentifier, this.route);
  }

  printReceipt(tellerTransactionIdentifier: string): void {
    this.transactionService.printReceipt(tellerTransactionIdentifier);
  }

  cancel(): void {
    this.router.navigate(['../../'], {relativeTo: this.route});
  }

  ngOnDestroy(): void {
    this.store.dispatch({
      type: RESET_TRANSACTION_FORM
    });
  }

}
