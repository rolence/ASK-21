/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PaymentType} from '../../../../../core/services/teller/domain/teller-transaction.model';
import {CreateTransactionEvent} from '../../domain/create-transaction-event.model';
import {TellerTransactionCosts} from '../../../../../core/services/teller/domain/teller-transaction-costs.model';
import {LoanAgreementOutline} from '../../../../../core/services/loan/domain/agreement/loan-agreement-outline.model';
import {TdStepComponent} from '@covalent/core';
import {ProductInstanceOutline} from '../../../../../core/services/depositAccount/domain/instance/product-instance-outline.model';
import {defaultPaymentOptions} from '../../domain/payment-type.options';
import {ConfirmTransactionEvent} from '../../domain/confirm-transaction-event.model';
import {PrintLoanChequeEvent} from '../../domain/print-loan-cheque-event.model';
import {FimsValidators} from '../../../../../core/validator/validators';

@Component({
  selector: 'aten-disbursement-transaction-form',
  templateUrl: './form.component.html'
})
export class LoanDisbursementFormComponent implements OnInit, OnChanges {

  form: FormGroup;
  printChequeForm: FormGroup;
  paymentOptions = defaultPaymentOptions;

  @Input('agreements') agreements: LoanAgreementOutline[];
  @Input('transactionCosts') transactionCosts: TellerTransactionCosts;
  @Input('productInstances') productInstances: ProductInstanceOutline[];
  @Input('transactionConfirmed') transactionConfirmed: boolean;

  @Output('onCreateTransaction') onCreateTransaction = new EventEmitter<CreateTransactionEvent>();
  @Output('onConfirmTransaction') onConfirmTransaction = new EventEmitter<ConfirmTransactionEvent>();
  @Output('onCancelTransaction') onCancelTransaction = new EventEmitter<string>();
  @Output('onCancel') onCancel = new EventEmitter<void>();
  @Output('onPrintCheque') onPrintCheque = new EventEmitter<PrintLoanChequeEvent>();
  @Output('onPrintReceipt') onPrintReceipt = new EventEmitter<string>();

  @ViewChild('transactionStep') transactionStep: TdStepComponent;
  @ViewChild('confirmationStep') confirmationStep: TdStepComponent;
  @ViewChild('printChequeStep') printChequeStep: TdStepComponent;

  constructor(private formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({
      agreement: ['', Validators.required],
      amount: [{value: 0, disabled: true}, [Validators.required, FimsValidators.greaterThanValue(0)]],
      paymentType: ['CASH', [Validators.required]],
      targetAccountIdentifier: ['', [Validators.required]]
    });

    this.printChequeForm = this.formBuilder.group({
      chequeNumber: ['', Validators.required],
      payee: ['', Validators.required]
    });

    this.form.get('paymentType').valueChanges
      .startWith('CASH')
      .subscribe(paymentType => this.togglePaymentType(paymentType));

    this.form.get('agreement').valueChanges
      .startWith(null)
      .subscribe(agreement => this.toggleAgreement(agreement));
  }

  ngOnInit(): void {
    this.transactionStep.open();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.transactionCosts && this.transactionCosts) {
      this.confirmationStep.open();
    }

    if (changes.transactionConfirmed && this.transactionConfirmed && this.printChequeStep) {
      this.printChequeStep.open();
    }
  }

  private toggleAgreement(agreement: LoanAgreementOutline): void {
    if (!agreement) {
      return;
    }

    this.form.get('amount').setValue(agreement.amountToDisburse);

    const paymentTypeControl = this.form.get('paymentType');
    if (agreement.disbursementMethod) {
      paymentTypeControl.setValue(agreement.disbursementMethod);
      paymentTypeControl.disable();
    } else {
      paymentTypeControl.enable();
    }
  }

  private togglePaymentType(paymentType: PaymentType): void {
    const targetAccountControl = this.form.get('targetAccountIdentifier');
    if (paymentType === 'TRANSFER') {
      targetAccountControl.enable();
    } else {
      targetAccountControl.disable();
    }

    if (paymentType === 'CHEQUE') {
      this.printChequeForm.enable();
    } else {
      this.printChequeForm.disable();
    }
  }

  createTransaction(): void {
    const agreement: LoanAgreementOutline = this.form.get('agreement').value;

    const formData: CreateTransactionEvent = {
      productIdentifier: agreement.productShortName,
      accountIdentifier: agreement.loanAccount,
      targetAccountIdentifier: this.form.get('targetAccountIdentifier').value ? this.form.get('targetAccountIdentifier').value : undefined,
      paymentType: this.form.get('paymentType').value,
      amount: this.form.get('amount').value
    };

    this.onCreateTransaction.emit(formData);
  }

  confirmTransaction(): void {
    this.onConfirmTransaction.emit({
      chargesIncluded: false,
      tellerTransactionIdentifier: this.transactionCosts.tellerTransactionIdentifier
    });
  }

  cancelTransaction(): void {
    this.onCancelTransaction.emit(this.transactionCosts.tellerTransactionIdentifier);
  }

  printCheque(): void {
    const agreement: LoanAgreementOutline = this.form.get('agreement').value;

    const printChequeEvent: PrintLoanChequeEvent = {
      productShortName: agreement.productShortName,
      chequeNumber: this.printChequeForm.get('chequeNumber').value,
      payee: this.printChequeForm.get('payee').value,
      amount: this.transactionCosts.totalAmount
    };

    this.onPrintCheque.emit(printChequeEvent);
  }

  printReceipt(): void {
    this.onPrintReceipt.emit(this.transactionCosts.tellerTransactionIdentifier);
  }

  cancel(): void {
    this.onCancel.emit();
  }

  get createTransactionDisabled(): boolean {
    return this.form.invalid || this.transactionCreated;
  }

  get confirmTransactionDisabled(): boolean {
    return !this.transactionCreated || this.transactionConfirmed;
  }

  get printReceiptDisabled(): boolean {
    return !this.transactionConfirmed;
  }

  get transactionCreated(): boolean {
    return !!this.transactionCosts;
  }
}
