/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ChequeFormService} from '../../components/cheque-form/cheque-form.service';
import {TdStepComponent} from '@covalent/core';
import {CreateTransactionEvent} from '../../domain/create-transaction-event.model';
import {TellerTransactionCosts} from '../../../../../core/services/teller/domain/teller-transaction-costs.model';
import {LoanAgreementOutline} from '../../../../../core/services/loan/domain/agreement/loan-agreement-outline.model';
import {defaultPaymentOptions} from '../../domain/payment-type.options';
import {ProductInstanceOutline} from '../../../../../core/services/depositAccount/domain/instance/product-instance-outline.model';
import {PaymentType} from '../../../../../core/services/teller/domain/teller-transaction.model';
import {Observable} from 'rxjs/Observable';
import {ConfirmTransactionEvent} from '../../domain/confirm-transaction-event.model';
import {FimsValidators} from '../../../../../core/validator/validators';

@Component({
  selector: 'aten-repayment-transaction-form',
  templateUrl: './form.component.html'
})
export class LoanRepaymentFormComponent implements OnChanges, OnInit {

  nextPayment: string;
  lastPayment: boolean;
  form: FormGroup;
  chequeForm: FormGroup;
  paymentOptions = defaultPaymentOptions;
  micrResolutionError$: Observable<boolean>;

  @Input('agreements') agreements: LoanAgreementOutline[];
  @Input('transactionCosts') transactionCosts: TellerTransactionCosts;
  @Input('payee') payee: string;
  @Input('productInstances') productInstances: ProductInstanceOutline[];
  @Input('transactionConfirmed') transactionConfirmed: boolean;

  @Output('onCreateTransaction') onCreateTransaction = new EventEmitter<CreateTransactionEvent>();
  @Output('onConfirmTransaction') onConfirmTransaction = new EventEmitter<ConfirmTransactionEvent>();
  @Output('onCancelTransaction') onCancelTransaction = new EventEmitter<string>();
  @Output('onCancel') onCancel = new EventEmitter<void>();
  @Output('onPrintReceipt') onPrintReceipt = new EventEmitter<string>();

  @ViewChild('transactionStep') transactionStep: TdStepComponent;
  @ViewChild('confirmationStep') confirmationStep: TdStepComponent;

  constructor(private formBuilder: FormBuilder, private chequeFormService: ChequeFormService) {
    this.chequeFormService.init();
    this.chequeForm = chequeFormService.formGroup;

    this.form = this.formBuilder.group({
      agreement: ['', Validators.required],
      amount: ['', [Validators.required, FimsValidators.greaterThanValue(0)]],
      paymentType: ['CASH', [Validators.required]],
      targetAccountIdentifier: ['', [Validators.required]]
    });

    this.form.get('agreement').valueChanges
      .subscribe(agreementOutline => this.toggleAgreement(agreementOutline));

    this.form.get('paymentType').valueChanges
      .startWith('CASH')
      .subscribe(paymentType => this.togglePaymentType(paymentType));
  }

  private toggleAgreement(agreementOutline: LoanAgreementOutline): void {
    this.nextPayment = agreementOutline.nextPayment;
    this.lastPayment = agreementOutline.lastPayment;
    const amountControl = this.form.get('amount');
    if (this.lastPayment) {
      amountControl.setValue(this.nextPayment);
      amountControl.disable();
    } else {
      amountControl.setValue(null);
      amountControl.enable();
    }
  }

  private togglePaymentType(paymentType: PaymentType): void {
    const targetAccountControl = this.form.get('targetAccountIdentifier');
    if (paymentType === 'TRANSFER') {
      targetAccountControl.enable();
    } else {
      targetAccountControl.disable();
    }

    if (paymentType === 'CHEQUE') {
      this.chequeFormService.enable();
    } else {
      this.chequeFormService.disable();
    }
  }

  expandMICR(): void {
    this.micrResolutionError$ = this.chequeFormService.expandMICR();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.transactionCosts && this.transactionCosts) {
      this.confirmationStep.open();
    }

    if (changes.payee && this.payee) {
      this.chequeFormService.formGroup.get('payee').setValue(this.payee);
    }
  }

  ngOnInit(): void {
    this.transactionStep.open();
  }

  createTransaction(): void {
    const agreement: LoanAgreementOutline = this.form.get('agreement').value;

    const formData: CreateTransactionEvent = {
      productIdentifier: agreement.productShortName,
      accountIdentifier: agreement.loanAccount,
      targetAccountIdentifier: this.form.get('targetAccountIdentifier').value ? this.form.get('targetAccountIdentifier').value : undefined,
      paymentType: this.form.get('paymentType').value,
      amount: this.form.get('amount').value,
      cheque: this.chequeFormService.getCheque(this.form.get('amount').value)
    };

    this.onCreateTransaction.emit(formData);
  }

  confirmTransaction(): void {
    this.onConfirmTransaction.emit({
      chargesIncluded: false,
      tellerTransactionIdentifier: this.transactionCosts.tellerTransactionIdentifier
    });
  }

  cancelTransaction(): void {
    this.onCancelTransaction.emit(this.transactionCosts.tellerTransactionIdentifier);
  }

  printReceipt(): void {
    this.onPrintReceipt.emit(this.transactionCosts.tellerTransactionIdentifier);
  }

  cancel(): void {
    this.onCancel.emit();
  }

  get createTransactionDisabled(): boolean {
    return this.form.invalid || this.chequeForm.invalid || this.transactionCreated;
  }

  get confirmTransactionDisabled(): boolean {
    return !this.transactionCreated || this.transactionConfirmed;
  }

  get printReceiptDisabled(): boolean {
    return !this.transactionConfirmed;
  }

  get transactionCreated(): boolean {
    return !!this.transactionCosts;
  }

}
