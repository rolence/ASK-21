/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, OnDestroy} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import * as fromTeller from '../../../../store';
import {CreateTransactionEvent} from '../../domain/create-transaction-event.model';
import {LoanAgreementOutline} from '../../../../../core/services/loan/domain/agreement/loan-agreement-outline.model';
import {ConfirmTransactionEvent} from '../../domain/confirm-transaction-event.model';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductInstanceOutline} from '../../../../../core/services/depositAccount/domain/instance/product-instance-outline.model';
import {RESET_TRANSACTION_FORM} from '../../../../store/transaction.actions';
import {TellerTransactionCosts} from '../../../../../core/services/teller/domain/teller-transaction-costs.model';
import {TransactionService} from '../../services/transaction.service';
import {Store} from '@ngrx/store';

@Component({
  templateUrl: './repayment.component.html'
})
export class TellerRepayLoanAccountComponent implements OnDestroy {

  payee$: Observable<string>;
  agreements$: Observable<LoanAgreementOutline[]>;
  transactionCosts$: Observable<TellerTransactionCosts>;
  productInstances$: Observable<ProductInstanceOutline[]>;
  transactionConfirmed$: Observable<boolean>;

  constructor(private router: Router, private route: ActivatedRoute,
              private store: Store<fromTeller.State>, private transactionService: TransactionService) {
    this.payee$ = this.store.select(fromTeller.getPayee);
    this.agreements$ = this.store.select(fromTeller.getAllTellerCustomerLoanProducts)
      .map(agreements => agreements.filter(agreement => agreement.state === 'DISBURSED' || agreement.state === 'DELINQUENT'));

    this.productInstances$ = this.store.select(fromTeller.getAllActiveDepositAccounts);
    this.transactionCosts$ = this.store.select(fromTeller.getTransactionCosts);
    this.transactionConfirmed$ = this.store.select(fromTeller.getTransactionConfirmed);
  }

  createTransaction(event: CreateTransactionEvent): void {
    this.transactionService.createTransaction(event, 'PPAY');
  }

  confirmTransaction(event: ConfirmTransactionEvent): void {
    this.transactionService.confirmTransaction(event, this.route);
  }

  cancelTransaction(tellerTransactionIdentifier: string): void {
    this.transactionService.cancelTransaction(tellerTransactionIdentifier, this.route);
  }

  printReceipt(tellerTransactionIdentifier: string): void {
    this.transactionService.printReceipt(tellerTransactionIdentifier);
  }

  cancel(): void {
    this.router.navigate(['../../'], {relativeTo: this.route});
  }

  ngOnDestroy(): void {
    this.store.dispatch({
      type: RESET_TRANSACTION_FORM
    });
  }
}
