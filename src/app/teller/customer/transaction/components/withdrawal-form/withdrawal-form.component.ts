/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {TdStepComponent} from '@covalent/core';
import {FormBuilder, FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {CreateTransactionEvent} from '../../domain/create-transaction-event.model';
import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';
import {ConfirmTransactionEvent} from '../../domain/confirm-transaction-event.model';
import {PaymentType} from '../../../../../core/services/teller/domain/teller-transaction.model';
import {PrintDepositChequeEvent} from '../../domain/print-deposit-cheque-event.model';
import {ProductInstanceOutline} from '../../../../../core/services/depositAccount/domain/instance/product-instance-outline.model';
import {TellerTransactionCosts} from '../../../../../core/services/teller/domain/teller-transaction-costs.model';
import {defaultPaymentOptions} from '../../domain/payment-type.options';
import {Observable} from 'rxjs/Observable';
import {FimsValidators} from '../../../../../core/validator/validators';

@Component({
  selector: 'aten-teller-withdrawal-form',
  templateUrl: './withdrawal-form.component.html'
})
export class TellerWithdrawalFormComponent implements OnInit, OnChanges {

  form: FormGroup;
  printChequeForm: FormGroup;
  paymentOptions = defaultPaymentOptions.filter(option => option.type !== 'TRANSFER');
  currentBalance: number;
  voucherNumber: string;
  chargesIncluded = false;

  @Input('transactionCosts') transactionCosts: TellerTransactionCosts;
  @Input('productInstances') productInstances: ProductInstanceOutline[];
  @Input('transactionConfirmed') transactionConfirmed: boolean;
  @Input('cashdrawLimit') cashdrawLimit: number;

  @Output('onCreateTransaction') onCreateTransaction = new EventEmitter<CreateTransactionEvent>();
  @Output('onConfirmTransaction') onConfirmTransaction = new EventEmitter<ConfirmTransactionEvent>();
  @Output('onCancelTransaction') onCancelTransaction = new EventEmitter<string>();
  @Output('onCancel') onCancel = new EventEmitter<void>();
  @Output('onPrintCheque') onPrintCheque = new EventEmitter<PrintDepositChequeEvent>();
  @Output('onPrintReceipt') onPrintReceipt = new EventEmitter<string>();

  @ViewChild('transactionStep') transactionStep: TdStepComponent;
  @ViewChild('confirmationStep') confirmationStep: TdStepComponent;
  @ViewChild('printChequeStep') printChequeStep: TdStepComponent;

  constructor(private formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({
      productInstance: ['', Validators.required],
      amount: [0, [Validators.required, FimsValidators.greaterThanValue(0)]],
      paymentType: ['CASH', [Validators.required]]
    });

    this.printChequeForm = this.formBuilder.group({
      chequeNumber: ['', Validators.required],
      payee: ['', Validators.required]
    });

    const paymentType$ = this.form.get('paymentType').valueChanges
      .startWith('CASH');

    const productInstance$ = this.form.get('productInstance').valueChanges
      .startWith(null);

    Observable.combineLatest(
      productInstance$,
      paymentType$
    ).subscribe(([productInstance, paymentType]) => this.toggleValidators(productInstance, paymentType));
  }

  ngOnInit(): void {
    this.transactionStep.open();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.transactionCosts && this.transactionCosts) {
      this.confirmationStep.open();
    }

    if (changes.transactionConfirmed && this.transactionConfirmed && this.printChequeStep) {
      this.printChequeStep.open();
    }
  }

  private toggleValidators(productInstance: ProductInstanceOutline, paymentType: PaymentType): void {
    let validateCashdrawLimit = true;

    if (paymentType === 'CHEQUE') {
      this.printChequeForm.enable();
      validateCashdrawLimit = false;
    } else {
      this.printChequeForm.disable();
    }

    if (productInstance) {
      this.enableAmountBalanceValidators(productInstance.balance, validateCashdrawLimit);
    }
  }

  private enableAmountBalanceValidators(balance: string, validateCashdrawLimit: boolean): void {
    const amountValidators: ValidatorFn[] = [FimsValidators.minValue(0), Validators.required];
    this.currentBalance = parseFloat(balance);
    let limit = this.currentBalance;

    if (validateCashdrawLimit) {
      limit = Math.min(this.cashdrawLimit, this.currentBalance);
    }

    amountValidators.push(FimsValidators.maxValue(limit));

    const amountControl = this.form.get('amount') as FormControl;
    amountControl.setValidators(amountValidators);
    amountControl.updateValueAndValidity();
  }

  createTransaction(): void {
    const productInstance: ProductInstanceOutline = this.form.get('productInstance').value;

    const formData: CreateTransactionEvent = {
      productIdentifier: productInstance.productShortCode,
      accountIdentifier: productInstance.accountIdentifier,
      paymentType: this.form.get('paymentType').value,
      amount: this.form.get('amount').value
    };

    this.onCreateTransaction.emit(formData);
  }

  confirmTransaction(chargesIncluded: boolean, voucherNumber: string): void {
    this.onConfirmTransaction.emit({
      chargesIncluded,
      tellerTransactionIdentifier: this.transactionCosts.tellerTransactionIdentifier,
      voucherNumber
    });
  }

  cancelTransaction(): void {
    this.onCancelTransaction.emit(this.transactionCosts.tellerTransactionIdentifier);
  }

  printCheque(): void {
    const productInstance: ProductInstanceOutline = this.form.get('productInstance').value;

    const printChequeEvent: PrintDepositChequeEvent = {
      productShortName: productInstance.productShortCode,
      type: productInstance.type,
      chequeNumber: this.printChequeForm.get('chequeNumber').value,
      payee: this.printChequeForm.get('payee').value,
      amount: this.transactionCosts.totalAmount
    };

    this.onPrintCheque.emit(printChequeEvent);
  }

  printReceipt(): void {
    this.onPrintReceipt.emit(this.transactionCosts.tellerTransactionIdentifier);
  }

  cancel(): void {
    this.onCancel.emit();
  }

  get createTransactionDisabled(): boolean {
    return this.form.invalid || this.transactionCreated;
  }

  get confirmTransactionDisabled(): boolean {
    return !this.transactionCreated || this.transactionConfirmed;
  }

  get printReceiptDisabled(): boolean {
    return !this.transactionConfirmed;
  }

  get transactionCreated(): boolean {
    return !!this.transactionCosts;
  }
}
