/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Injectable} from '@angular/core';
import {toShortISOString} from '../../../../../core/services/domain/date.converter';
import {Observable} from 'rxjs/Observable';
import {ChequeService} from '../../../../../core/services/cheque/cheque.service';
import {toMICRIdentifier} from '../../../../../core/services/cheque/domain/mapper/fims-cheque.mapper';
import {Cheque} from '../../../../../core/services/teller/domain/cheque.model';
import {FimsValidators} from '../../../../../core/validator/validators';

@Injectable()
export class ChequeFormService {

  private _formGroup: FormGroup;

  constructor(private formBuilder: FormBuilder, private chequeService: ChequeService) {}

  init(): void {
    this._formGroup = this.formBuilder.group({
      chequeNumber: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(8), FimsValidators.isNumber]],
      branchSortCode: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(11)]],
      accountNumber: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(34)]],
      drawee: ['', Validators.required],
      drawer: ['', Validators.required],
      payee: [{value: '', disabled: true}, Validators.required],
      dateIssued: ['', [Validators.required]],
      openCheque: [false]
    });
  }

  getCheque(amount: number): Cheque | undefined {
    if (this._formGroup.enabled) {
      const cheque: Cheque = {
        micr: {
          chequeNumber: this._formGroup.get('chequeNumber').value,
          branchSortCode: this._formGroup.get('branchSortCode').value,
          accountNumber: this._formGroup.get('accountNumber').value
        },
        drawee: this._formGroup.get('drawee').value,
        drawer: this._formGroup.get('drawer').value,
        payee: this._formGroup.get('payee').value,
        amount,
        dateIssued: toShortISOString(this._formGroup.get('dateIssued').value),
        openCheque: this._formGroup.get('openCheque').value
      };

      return cheque;
    }
  }

  get formGroup(): FormGroup {
    return this._formGroup;
  }

  reset(): void {
    this._formGroup.reset();
  }

  expandMICR(): Observable<boolean> {
    const chequeNumber = this._formGroup.get('chequeNumber').value;
    const branchSortCode = this._formGroup.get('branchSortCode').value;
    const accountNumber = this._formGroup.get('accountNumber').value;

    const identifier = toMICRIdentifier(chequeNumber, branchSortCode, accountNumber);

    const draweeControl = this.formGroup.get('drawee');
    const drawerControl = this.formGroup.get('drawer');

    return this.chequeService.expandMicr(identifier)
      .do(resolution => {
        draweeControl.setValue(resolution.office);
        drawerControl.setValue(resolution.customer);
      })
      .map(resolution => !resolution)
      .catch(() => {
        return Observable.of(true);
      });
  }

  enable(): void {
    this._formGroup.enable();
    this._formGroup.get('payee').disable();
  }

  disable(): void {
    this._formGroup.disable();
  }
}
