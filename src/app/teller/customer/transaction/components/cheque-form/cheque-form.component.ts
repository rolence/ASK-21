/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({
  templateUrl: './cheque-form.component.html',
  selector: 'aten-teller-cheque-form'
})
export class ChequeFormComponent {

  @Input() formGroup: FormGroup;
  @Input() micrResolutionError: boolean;

  @Output() onExpandMICR = new EventEmitter<string>();

  expandMICR(): void {
    this.onExpandMICR.emit();
  }

  get chequeFormInvalid(): boolean {
    const chequeNumber = this.formGroup.get('chequeNumber');
    const branchSortCode = this.formGroup.get('branchSortCode');
    const accountNumber = this.formGroup.get('accountNumber');
    return chequeNumber.invalid || branchSortCode.invalid || accountNumber.invalid;
  }
}
