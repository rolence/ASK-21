/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {NgModule} from '@angular/core';
import {TellerRoutingModule} from './teller.routing';
import {TellerIndexComponent} from './teller.index.component';
import {TellerLoginGuard} from './teller-login.guard';
import {TellerAuthComponent} from './auth/teller-auth.component';
import {EffectsModule} from '@ngrx/effects';
import {TellerApiEffects} from './store/effects/service.effects';
import {TellerRouteEffects} from './store/effects/route.effects';
import {TellerCustomerExistsGuard} from './customer/teller-customer-exists.guard';
import {TellerCustomerDetailComponent} from './customer/customer-detail.component';
import {TellerProductsApiEffects} from './store/effects/products.service.effects';
import {TellerCustomerIndexComponent} from './customer/customer-index.component';
import {TellerNotificationEffects} from './store/effects/notification.effects';
import {TransactionCostComponent} from './customer/transaction/components/cost.component';
import {TellerTransactionService} from './services/transaction.service';
import {AvailableActionService} from './services/available-actions.service';
import {ChequeFormComponent} from './customer/transaction/components/cheque-form/cheque-form.component';
import {LoanRepaymentFormComponent} from './customer/transaction/loan/repayment/form.component';
import {LoanDisbursementFormComponent} from './customer/transaction/loan/disbursement/form.component';
import {ChequeFormService} from './customer/transaction/components/cheque-form/cheque-form.service';
import {TellerWithdrawalComponent} from './customer/transaction/withdrawal/withdrawal.component';
import {TellerDepositComponent} from './customer/transaction/deposit/deposit.component';
import {TellerTransferComponent} from './customer/transaction/transfer/transfer.component';
import {TellerTransferFormComponent} from './customer/transaction/transfer/form.component';
import {TellerCloseDepositAccountComponent} from './customer/transaction/deposit-account/close/close.component';
import {TellerOpenDepositAccountComponent} from './customer/transaction/deposit-account/open/open.component';
import {TellerRepayLoanAccountComponent} from './customer/transaction/loan/repayment/repayment.component';
import {TellerDisburseLoanAccountComponent} from './customer/transaction/loan/disbursement/disbursement.component';
import {TellerWithdrawalFormComponent} from './customer/transaction/components/withdrawal-form/withdrawal-form.component';
import {TellerDepositFormComponent} from './customer/transaction/components/deposit-form/deposit-form.component';
import {TransactionService} from './customer/transaction/services/transaction.service';
import {TellerACHComponent} from './customer/transaction/ach/ach.component';
import {TellerACHFormComponent} from './customer/transaction/ach/form.component';
import {ProductInstancesFormComponent} from './customer/transaction/components/product-instances-form/product-instances-form.component';
import {StoreModule} from '@ngrx/store';
import {reducerProvider, reducerToken} from './store';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  imports: [
    TellerRoutingModule,
    SharedModule,

    StoreModule.forFeature('teller', reducerToken),

    EffectsModule.forFeature([
      TellerApiEffects,
      TellerRouteEffects,
      TellerProductsApiEffects,
      TellerNotificationEffects
    ])
  ],
  declarations: [
    TellerIndexComponent,
    TellerAuthComponent,
    TellerCustomerIndexComponent,
    TellerCustomerDetailComponent,
    TransactionCostComponent,
    LoanRepaymentFormComponent,
    LoanDisbursementFormComponent,
    ChequeFormComponent,
    ProductInstancesFormComponent,
    TellerOpenDepositAccountComponent,
    TellerCloseDepositAccountComponent,
    TellerDepositComponent,
    TellerWithdrawalComponent,
    TellerTransferComponent,
    TellerTransferFormComponent,
    TellerRepayLoanAccountComponent,
    TellerDisburseLoanAccountComponent,
    TellerDepositFormComponent,
    TellerWithdrawalFormComponent,
    TellerACHComponent,
    TellerACHFormComponent
  ],
  providers: [
    TellerLoginGuard,
    TellerCustomerExistsGuard,
    TellerTransactionService,
    TransactionService,
    AvailableActionService,
    ChequeFormService,
    reducerProvider
  ]
})
export class TellerModule { }
