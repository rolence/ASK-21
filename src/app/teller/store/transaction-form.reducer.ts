/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import * as actions from './transaction.actions';
import {TellerTransactionCosts} from '../../core/services/teller/domain/teller-transaction-costs.model';

export interface State {
  transactionCosts: TellerTransactionCosts;
  transactionConfirmed: boolean;
  chequePrinted: boolean;
}

export const initialState: State = {
  transactionCosts: null,
  transactionConfirmed: false,
  chequePrinted: false
};

export function reducer(state = initialState, action: actions.Actions): State {
  switch (action.type) {

    case actions.CREATE_TRANSACTION_SUCCESS: {
      const transactionCosts: TellerTransactionCosts = action.payload;

      return {
        ...state,
        transactionCosts
      };
    }

    case actions.CONFIRM_TRANSACTION_SUCCESS: {
      return {
        ...state,
        transactionConfirmed: true
      };
    }

    case actions.PRINT_CHEQUE_SUCCESS: {
      return {
        ...state,
        chequePrinted: true
      };
    }

    case actions.RESET_TRANSACTION_FORM: {
      return initialState;
    }

    default: {
      return state;
    }
  }
}

export const getTransactionCosts = (state: State) => state.transactionCosts;
export const getChequePrinted = (state: State) => state.chequePrinted;
export const getTransactionConfirmed = (state: State) => state.transactionConfirmed;

