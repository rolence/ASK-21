/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import * as tellerActions from '../teller.actions';
import * as transactionActions from '../transaction.actions';
import {NotificationService, NotificationType} from '../../../core/services/notification/notification.service';

@Injectable()
export class TellerNotificationEffects {

  @Effect({ dispatch: false })
  unlockDrawerSuccess$ = this.actions$
    .ofType(tellerActions.UNLOCK_DRAWER_SUCCESS)
    .do(() => this.notificationService.send({
      type: NotificationType.MESSAGE,
      message: 'Teller drawer unlocked'
    }));

  @Effect({ dispatch: false })
  lockDrawerSuccess$ = this.actions$
    .ofType(tellerActions.LOCK_DRAWER_SUCCESS)
    .do(() => this.notificationService.send({
      type: NotificationType.MESSAGE,
      message: 'Teller drawer is now locked'
    }));

  @Effect({ dispatch: false })
  createTransactionFail = this.actions$
    .ofType(transactionActions.CREATE_TRANSACTION_FAIL)
    .map((action: transactionActions.CreateTransactionFailAction) => action.payload)
    .do(error => {
      this.notificationService.send({
        type: NotificationType.ALERT,
        message: error.message ? error.message : 'There was an issue creating the transaction'
      });
    });

  @Effect({ dispatch: false })
  confirmTransactionSuccess = this.actions$
    .ofType(transactionActions.CONFIRM_TRANSACTION_SUCCESS)
    .map((action: transactionActions.ConfirmTransactionSuccessAction) => action.payload)
    .do((payload) => {
      const action: string = payload.command === 'CONFIRM' ? 'confirmed' : 'canceled';
      this.notificationService.send({
        type: NotificationType.MESSAGE,
        message: `Transaction successfully ${action}`
      });
    });

  @Effect({ dispatch: false })
  confirmTransactionFail = this.actions$
    .ofType(transactionActions.CONFIRM_TRANSACTION_FAIL)
    .map((action: transactionActions.ConfirmTransactionFailAction) => action.payload)
    .do((error) => {
      this.notificationService.send({
        title: 'Invalid transaction',
        type: NotificationType.ALERT,
        message: error.message
      });
    });

  @Effect({ dispatch: false })
  printReceiptFail = this.actions$
    .ofType(transactionActions.PRINT_RECEIPT_FAIL)
    .map((action: transactionActions.PrintReceiptFailAction) => action.payload)
    .do(() => {
      this.notificationService.send({
        type: NotificationType.ALERT,
        message: 'There was an issue printing your receipt'
      });
    });

  @Effect({ dispatch: false })
  printChequeFail = this.actions$
    .ofType(transactionActions.PRINT_CHEQUE_FAIL)
    .map((action: transactionActions.PrintChequeFailAction) => action.payload)
    .do(() => {
      this.notificationService.send({
        type: NotificationType.ALERT,
        message: 'There was an issue printing your cheque'
      });
    });

  constructor(private actions$: Actions, private notificationService: NotificationService) {}
}
