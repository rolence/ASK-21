/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import * as fromRoot from '../../core/store';
import {ActionReducerMap, createFeatureSelector} from '@ngrx/store';
import * as fromAuthentication from './authentication.reducer';
import * as fromDepositProducts from './customer-deposit-products.reducer';
import * as fromLoanProducts from './customer-loan-products.reducer';
import * as fromTransactionForm from './transaction-form.reducer';
import {createSelector} from 'reselect';
import {
  createResourceReducer,
  getResourceAll,
  getResourceLoadedAt,
  getResourceSelected,
  ResourceState
} from '../../core/store/util/resource.reducer';

import {InjectionToken} from '@angular/core';
import {CustomerAccountOutline} from '../../shared/customer-account-list/domain/customer-account-outline.model';
import {ProductInstanceOutline} from '../../core/services/depositAccount/domain/instance/product-instance-outline.model';
import {LoanAgreementOutline} from '../../core/services/loan/domain/agreement/loan-agreement-outline.model';

export interface TellerState {
  tellerAuthentication: fromAuthentication.State;
  tellerCustomers: ResourceState;
  tellerCustomerDepositProducts: ResourceState;
  tellerCustomerLoanProducts: ResourceState;
  tellerTransactionForm: fromTransactionForm.State;
}

export interface State extends fromRoot.State {
  teller: TellerState;
}

export const reducers: ActionReducerMap<TellerState> = {
  tellerAuthentication: fromAuthentication.reducer,
  tellerCustomers: createResourceReducer('Teller Customer'),
  tellerCustomerDepositProducts: fromDepositProducts.reducer,
  tellerCustomerLoanProducts: fromLoanProducts.reducer,
  tellerTransactionForm: fromTransactionForm.reducer
};

export const reducerToken = new InjectionToken<ActionReducerMap<TellerState>>('Teller reducers');

export const reducerProvider = [
  { provide: reducerToken, useValue: reducers }
];

export const selectTellerState = createFeatureSelector<TellerState>('teller');

export const getAuthenticationState = createSelector(selectTellerState, (state: TellerState) => state.tellerAuthentication);

export const isAuthenticated = createSelector(getAuthenticationState, fromAuthentication.getAuthenticated);
export const getAuthenticationError = createSelector(getAuthenticationState, fromAuthentication.getError);
export const getAuthenticationLoading = createSelector(getAuthenticationState, fromAuthentication.getLoading);
export const getAuthenticatedTeller = createSelector(getAuthenticationState, fromAuthentication.getTeller);

export const getTellerCustomersState = createSelector(selectTellerState, (state: TellerState) => state.tellerCustomers);

export const getTellerCustomerLoadedAt = createSelector(getTellerCustomersState, getResourceLoadedAt);
export const getTellerSelectedCustomer = createSelector(getTellerCustomersState, getResourceSelected);

export const getTellerCustomerDepositProductsState =
  createSelector(selectTellerState, (state: TellerState) => state.tellerCustomerDepositProducts);
export const getAllTellerCustomerDepositProducts = createSelector(getTellerCustomerDepositProductsState, getResourceAll);
export const hasTellerCustomerDepositProducts = createSelector(getAllTellerCustomerDepositProducts, (products) => {
  return products.length > 0;
});

export const getAllActiveDepositAccounts = createSelector(getAllTellerCustomerDepositProducts, (products) => {
  return products.filter(product => product.state === 'ACTIVE');
});

export const getAllPendingDepositAccounts = createSelector(getAllTellerCustomerDepositProducts, (products) => {
  return products.filter(product => product.state === 'PENDING');
});

export const getTellerCustomerLoanProductsState =
  createSelector(selectTellerState, (state: TellerState) => state.tellerCustomerLoanProducts);
export const getAllTellerCustomerLoanProducts = createSelector(getTellerCustomerLoanProductsState, getResourceAll);
export const hasTellerCustomerLoanProducts = createSelector(getAllTellerCustomerLoanProducts, (products) => {
  return products.length > 0;
});

export const getTellerTransactionFormState = createSelector(selectTellerState, (state: TellerState) => state.tellerTransactionForm);

export const getTransactionCosts = createSelector(getTellerTransactionFormState, fromTransactionForm.getTransactionCosts);
export const getChequePrinted = createSelector(getTellerTransactionFormState, fromTransactionForm.getChequePrinted);
export const getTransactionConfirmed = createSelector(getTellerTransactionFormState, fromTransactionForm.getTransactionConfirmed);

export const getPayee = createSelector(getTellerSelectedCustomer, (customer) => `${customer.givenName} ${customer.surname}`);

export const getCreateTransactionData = createSelector(
  getAuthenticatedTeller,
  getTellerSelectedCustomer,
  fromRoot.getUsername,
  (teller, customer, username) => {
    return {
      teller,
      customer,
      username
    };
});

export const getPrintChequeData = createSelector(
  getAuthenticatedTeller,
  getTellerSelectedCustomer,
  (teller, customer) => {
    return {
      teller,
      customer
    };
  });

export const getDepositAccountOutlines =
  createSelector(getAllTellerCustomerDepositProducts, (depositAccounts: ProductInstanceOutline[]): CustomerAccountOutline[] => {
    const deposits: CustomerAccountOutline[] = depositAccounts.map(account => ({
      type: account.type,
      productShortCode: account.productShortCode,
      accountIdentifier: account.accountIdentifier,
      balance: account.balance,
      amountOnHold: account.amountOnHold,
      collateralAmount: account.collateralAmount
    }));

    return deposits.sort((outlineA, outlineB) => {
      return outlineA.accountIdentifier.localeCompare(outlineB.accountIdentifier);
    });
  });

export const getLoanAccountOutlines =
  createSelector(getAllTellerCustomerLoanProducts, (loanAgreements: LoanAgreementOutline[]): CustomerAccountOutline[] => {
    const agreements: CustomerAccountOutline[] = loanAgreements.map(account => ({
      type: account.loanType,
      productShortCode: account.productShortName,
      accountIdentifier: account.loanAccount,
      balance: account.balance,
      collateralAmount: account.collateralAmount,
      accruedInterest: account.accruedInterest
    }));

    return agreements.sort((outlineA, outlineB) => {
      return outlineA.accountIdentifier.localeCompare(outlineB.accountIdentifier);
    });
  });
