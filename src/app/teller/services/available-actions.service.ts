/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Injectable} from '@angular/core';
import {DepositAccountService} from '../../core/services/depositAccount/deposit-account.service';
import {Observable} from 'rxjs/Observable';
import {TransactionType} from '../../core/services/teller/domain/teller-transaction.model';
import {CustomerLoanService} from '../../core/services/loan/customer-loan.service';
import {LoanAgreementOutline} from '../../core/services/loan/domain/agreement/loan-agreement-outline.model';
import {FimsPermission} from '../../core/services/security/authz/fims-permission.model';

export interface Action {
  transactionType: TransactionType;
  icon: string;
  title: string;
  relativeLink: string;
  permission?: FimsPermission;
}

const depositActions: Action[] = [
  { transactionType: 'CDPT', icon: 'arrow_forward', title: 'Deposit', relativeLink: 'transaction/deposit'},
  { transactionType: 'CWDL', icon: 'arrow_back', title: 'Withdrawal', relativeLink: 'transaction/withdrawal'},
  { transactionType: 'ACCO', icon: 'create', title: 'Open', relativeLink: 'transaction/open' },
  { transactionType: 'ACCT', icon: 'swap_horiz', title: 'Account transfer', relativeLink: 'transaction/transfer'},
  { transactionType: 'ATXN', icon: 'arrow_back', title: 'ACH transfer', relativeLink: 'transaction/ach'},
  { transactionType: 'ACCC', icon: 'close', title: 'Close deposit account', relativeLink: 'transaction/close' }
];

const loanActions: Action[] = [
  {
    transactionType: 'PPAY',
    icon: 'arrow_forward',
    title: 'Payment',
    relativeLink: 'transaction/repay',
    permission: {id: 'loan_repay_agreement', accessLevel: 'CHANGE'}
  },
  {
    transactionType: 'CDIS',
    icon: 'arrow_back',
    title: 'Disburse',
    relativeLink: 'transaction/disburse',
    permission: {id: 'loan_disburse_agreement', accessLevel: 'CHANGE'}
  }
];

@Injectable()
export class AvailableActionService {

  constructor(private depositService: DepositAccountService, private customerLoanService: CustomerLoanService) {}

  getAvailableActions(customerIdentifier: string): Observable<Action[]> {
    const depositActions$ = this.getAvailableDepositActions(customerIdentifier);
    const loanActions$ = this.getAvailableLoanActions(customerIdentifier);
    return Observable.combineLatest(
      depositActions$,
      loanActions$,
      (availableDepositActions, availableLoanActions) =>
        availableDepositActions.concat(availableLoanActions)
    );
  }

  getAvailableDepositActions(customerIdentifier: string): Observable<Action[]> {
    return this.depositService.fetchPossibleTransactionsForCustomer(customerIdentifier)
      .map(types => depositActions.filter(action =>
        !!types.find(type => action.transactionType === type)
      ));
  }

  getAvailableLoanActions(customerIdentifier: string): Observable<Action[]> {
    return this.customerLoanService.fetchAllLoanAgreements(customerIdentifier)
      .map(agreements => {
        const actions: Action[] = [];
        if (agreements.length === 0) {
          return actions;
        }

        if (this.hasApprovedLoan(agreements)) {
          actions.push(loanActions.find(action => action.transactionType === 'CDIS'));
        }

        if (this.hasPayableLoan(agreements)) {
          actions.push(loanActions.find(action => action.transactionType === 'PPAY'));
        }

        return actions;
      });
  }

  private hasApprovedLoan(agreements: LoanAgreementOutline[]): boolean {
    return !!agreements.find(agreement => agreement.state === 'APPROVED');
  }

  private hasPayableLoan(agreements: LoanAgreementOutline[]): boolean {
    return !!agreements.find(agreement =>
      agreement.state === 'DISBURSED' || agreement.state === 'DELINQUENT'
    );
  }

}
