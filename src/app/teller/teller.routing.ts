/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {RouterModule, Routes} from '@angular/router';
import {TellerLoginGuard} from './teller-login.guard';
import {TellerAuthComponent} from './auth/teller-auth.component';
import {TellerIndexComponent} from './teller.index.component';
import {TellerCustomerDetailComponent} from './customer/customer-detail.component';
import {TellerCustomerExistsGuard} from './customer/teller-customer-exists.guard';
import {TellerCustomerIndexComponent} from './customer/customer-index.component';
import {TellerTransferComponent} from './customer/transaction/transfer/transfer.component';
import {TellerWithdrawalComponent} from './customer/transaction/withdrawal/withdrawal.component';
import {TellerDepositComponent} from './customer/transaction/deposit/deposit.component';
import {TellerOpenDepositAccountComponent} from './customer/transaction/deposit-account/open/open.component';
import {TellerCloseDepositAccountComponent} from './customer/transaction/deposit-account/close/close.component';
import {TellerRepayLoanAccountComponent} from './customer/transaction/loan/repayment/repayment.component';
import {TellerDisburseLoanAccountComponent} from './customer/transaction/loan/disbursement/disbursement.component';
import {TellerACHComponent} from './customer/transaction/ach/ach.component';
import {NgModule} from '@angular/core';

const TellerRoutes: Routes = [
  {
    path: '',
    canActivate: [TellerLoginGuard],
    data: {
      title: 'Search member',
      hasPermission: { id: 'teller_operations', accessLevel: 'READ' }
    },
    children: [
      {
        path: '',
        component: TellerIndexComponent
      },
      {
        path: 'customers/detail/:id',
        component: TellerCustomerIndexComponent,
        data: {
          hasPermission: { id: 'customer_customers', accessLevel: 'READ' }
        },
        canActivate: [ TellerCustomerExistsGuard ],
        children: [
          {
            path: '',
            component: TellerCustomerDetailComponent,
            data: {
              title: 'View member'
            }
          },
          {
            path: 'transaction/open',
            component: TellerOpenDepositAccountComponent,
            data: {
              title: 'Open deposit account'
            }
          },
          {
            path: 'transaction/close',
            component: TellerCloseDepositAccountComponent,
            data: {
              title: 'Close deposit account'
            }
          },
          {
            path: 'transaction/repay',
            component: TellerRepayLoanAccountComponent,
            data: {
              title: 'Loan payment'
            }
          },
          {
            path: 'transaction/disburse',
            component: TellerDisburseLoanAccountComponent,
            data: {
              title: 'Loan disbursement'
            }
          },
          {
            path: 'transaction/transfer',
            component: TellerTransferComponent,
            data: {
              title: 'Transfer'
            }
          },
          {
            path: 'transaction/withdrawal',
            component: TellerWithdrawalComponent,
            data: {
              title: 'Withdrawal'
            }
          },
          {
            path: 'transaction/deposit',
            component: TellerDepositComponent,
            data: {
              title: 'Deposit'
            }
          },
          {
            path: 'transaction/ach',
            component: TellerACHComponent,
            data: {
              title: 'ACH transfer'
            }
          },
          {
            path: 'identifications',
            loadChildren: '../customers/detail/identityCard/identity-card.module#IdentityCardModule'
          },
        ]
      }
    ]
  },
  {
    path: 'auth',
    component: TellerAuthComponent,
    data: {
      title: 'Teller login'
    }
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(TellerRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class TellerRoutingModule {}
