/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Component, EventEmitter, Input, Output} from '@angular/core';
import {LoanProductOutline} from '../../core/services/loan/domain/definition/product-outline.model';

@Component({
  selector: 'aten-loan-product-table',
  templateUrl: './loan-product-list.component.html'
})
export class LoanProductTableComponent {

  @Input() loanProducts: LoanProductOutline[];
  @Input() disabled = false;
  @Output() onSelect = new EventEmitter<LoanProductOutline>();

  constructor() {}

  rowSelect(loanProduct: LoanProductOutline): void {
    if (!this.disabled) {
      this.onSelect.emit(loanProduct);
    }
  }
}
