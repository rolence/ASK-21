/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, Input} from '@angular/core';
import {CustomerAccountOutline} from './domain/customer-account-outline.model';

@Component({
  selector: 'aten-account-outline-table',
  templateUrl: './account-outline-table.component.html'
})
export class AccountOutlineTableComponent {

  @Input() accountOutlines: CustomerAccountOutline[] = [];

  get totalBalance(): number {
    return this.accountOutlines.reduce((previous: number, current) => {
      if (current.balance) {
        return Number.parseFloat(current.balance) + previous;
      }
      return previous;
    }, 0);
  }

  get totalCollateral(): number {
    return this.accountOutlines.reduce((previous: number, current) => {
      if (current.collateralAmount) {
        return Number.parseFloat(current.collateralAmount) + previous;
      }
      return previous;
    }, 0);
  }

  get totalAmountOnHold(): number {
    return this.accountOutlines.reduce((previous: number, current) => {
      if (current.amountOnHold) {
        return Number.parseFloat(current.amountOnHold) + previous;
      }
      return previous;
    }, 0);
  }
}
