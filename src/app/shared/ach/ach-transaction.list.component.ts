/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, Input} from '@angular/core';
import {DatePipe} from '@angular/common';
import {TableData} from '../data-table/data-table.component';
import {DisplayFimsNumber} from '../number/fims-number.pipe';

@Component({
  selector: 'aten-ach-transaction-list',
  templateUrl: './ach-transaction.list.component.html',
  providers: [DisplayFimsNumber, DatePipe]
})
export class ACHTransactionListComponent {

  @Input() transactionData: TableData;

  columns: any[] = [
    {name: 'customerNumber', label: 'Member no.'},
    {name: 'customer', label: 'Member'},
    {name: 'accountNumber', label: 'Account no.'},
    {name: 'createdOn', label: 'Created on', format: (v: any) => this.datePipe.transform(v, 'short')},
    {name: 'amount', label: 'Amount', format: (v: any) => this.displayFimsNumber.transform(v) }
  ];

  constructor(private displayFimsNumber: DisplayFimsNumber, private datePipe: DatePipe) {}

}
