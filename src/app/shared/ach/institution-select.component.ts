/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {FormGroup} from '@angular/forms';
import {Component, Input} from '@angular/core';
import {FinancialInstitution} from '../../core/services/office/domain/financial-institution.model';
import {isObject} from '../../core/validator/validators';

@Component({
  selector: 'aten-institution-select',
  templateUrl: './institution-select.component.html'
})
export class InstitutionSelectComponent {

  @Input() filteredInstitutions: FinancialInstitution[];
  @Input() form: FormGroup;

  institutionDisplay(institution: any): string {
    if (isObject(institution)) {
      return institution ? institution.code : undefined;
    }
    return institution;
  }
}
