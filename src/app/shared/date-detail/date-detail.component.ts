/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, HostBinding, Input} from '@angular/core';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'aten-date-detail',
  templateUrl: './date-detail.component.html',
  providers: [DatePipe]
})
export class DateDetailComponent {

  @HostBinding('attr.layout')
  @Input() layout = 'row';
  @Input() title: string;
  @Input() textValue: string;
  @Input() dateValue: string;

  constructor(private datePipe: DatePipe) {}

  get value(): string {
    if (this.textValue && this.dateValue) {
      return `${this.textValue} - ${this.datePipe.transform(this.dateValue, 'medium')}`;
    }

    if (this.textValue) {
      return this.textValue;
    }

    if (this.dateValue) {
      return this.dateValue;
    }
    return `-`;
  }
}
