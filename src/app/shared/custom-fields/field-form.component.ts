/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormArray, FormGroup} from '@angular/forms';
import {DataTypeOption, dataTypes} from './domain/datatype-types.model';

@Component({
  selector: 'aten-custom-field-form',
  templateUrl: './field-form.component.html'
})
export class FieldFormComponent {

  dataTypeOptions: DataTypeOption[] = dataTypes;

  @Input() form: FormGroup;
  @Input() editMode: boolean;

  @Output() onAddOption = new EventEmitter<void>();
  @Output() onRemoveOption = new EventEmitter<number>();

  addOption(): void {
    this.onAddOption.emit();
  }

  removeOption(index: number): void {
    this.onRemoveOption.emit(index);
  }

  get options(): FormArray {
    return this.form.get('options') as FormArray;
  }
}
